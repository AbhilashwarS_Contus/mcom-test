/**
 * CustomButton.java
 * <p/>
 * This is the Custom button view class.
 *
 * @category Contus
 * @package contus.com.mcomm
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contussupport.ecommerce.R;


public class CustomButton extends Button {
    /**
     * The typed array.
     */
    private TypedArray typedArray;

    /**
     * Instantiates a new custom text view.
     *
     * @param context the context
     */
    public CustomButton(Context context) {
        super(context);
    }

    /**
     * Instantiates a new custom text view.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Instantiates a new custom text view.
     *
     * @param context  the context
     * @param attrs    the attrs
     * @param defStyle the def style
     */
    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    /**
     * Inits the.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    public void init(Context context, AttributeSet attrs) {
        try {
            typedArray = context.obtainStyledAttributes(attrs,
                    R.styleable.CustomWidget);
            int arrayCount = typedArray.getIndexCount();
            for (int i = 0; i < arrayCount; i++) {
                int attr = typedArray.getIndex(i);
                if (attr == R.styleable.CustomWidget_font_name) {
                    setTypeFace(attr);
                }
            }
        } catch (Exception e) {
            LogUtils.e(Constant.TAG_EXCEPTION, e.getMessage());
        }
    }

    private void setTypeFace(int attr) {
        Typeface font = Typeface.createFromAsset(getResources()
                .getAssets(), typedArray.getString(attr));
        if (font != null) {
            this.setTypeface(font);
        }
    }

}
