/**
 * NumberPicker.java
 * This is the Custom NumberPicker view class.
 *
 * @category Contus
 * @package contus.com.mcomm
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.views;

import android.content.Context;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.method.NumberKeyListener;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.model.CartItem;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contussupport.ecommerce.R;


public class NumberPicker extends LinearLayout implements View.OnClickListener,
        View.OnFocusChangeListener, View.OnLongClickListener, EditText.OnEditorActionListener {

    private static final char[] DIGIT_CHARACTERS = new char[]{'0', '1', '2',
            '3', '4', '5', '6', '7', '8', '9'};
    private static int defaultMin = 1;
    private final Handler mHandler;
    private final InputFilter mNumberInputFilter;
    protected int incrementcount;
    private CustomEditTextView mText;
    private int defaultMax = 1;
    private int value = 1;
    private String[] mDisplayedValues;
    private int mStart;
    private int mEnd;
    private int mCurrent = 1;
    private int mPosition;
    private int mPrevious;
    private OnChangedListener mListener;
    private Formatter mFormatter;
    private int maxValue = 1;
    private boolean mIncrement;
    private boolean mDecrement;
    private final Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            long mSpeed = 300;
            if (mIncrement) {
                changeCurrent(mCurrent + value);
                mHandler.postDelayed(this, mSpeed);
            } else if (mDecrement) {
                changeCurrent(mCurrent - value);
                mHandler.postDelayed(this, mSpeed);
            }
        }
    };
    private int quantity;
    private NumberPickerTextView mIncrementButton;
    private NumberPickerTextView mDecrementButton;
    private Context mContext;


    public NumberPicker(Context context) {
        this(context, null);
    }


    public NumberPicker(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NumberPicker(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        mContext = context;
        setOrientation(VERTICAL);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.number_picker, this, true);
        mHandler = new Handler();
        mNumberInputFilter = new NumberRangeKeyListener();
        mIncrementButton = (NumberPickerTextView) findViewById(R.id.increment);
        mIncrementButton.setOnClickListener(this);
        mIncrementButton.setOnLongClickListener(this);
        mIncrementButton.setNumberPicker(this);
        mDecrementButton = (NumberPickerTextView) findViewById(R.id.decrement);
        mDecrementButton.setOnClickListener(this);
        mDecrementButton.setOnLongClickListener(this);
        mDecrementButton.setNumberPicker(this);

        if (!isEnabled()) {
            setEnabled(false);
        }
        mText = (CustomEditTextView) findViewById(R.id.numberquantity);
        mText.setOnEditorActionListener(this);
        mStart = defaultMin;
        mEnd = defaultMax;

    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        validateInput(v);
        return false;
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        mIncrementButton.setEnabled(enabled);
        mDecrementButton.setEnabled(enabled);
        mText.setEnabled(enabled);
    }

    public void setOnChangeListener(OnChangedListener listener) {
        mListener = listener;
    }

    public void setCurrentQty(int value) {

        mCurrent = value;
        mEnd = getQuantity();
        maxValue = getCurrent();
        mText.setText(formatNumber(mCurrent));

    }

    public void setPosition(int value, CartItem cartItem) {
        setQuantity((int) Float.parseFloat(cartItem.getStockQty()));
        setCurrent(Integer.parseInt(cartItem.getQty()));
        setIncrementValue(1);
        mPosition = value;
    }

    public int getIncrementValue() {
        return incrementcount;
    }

    public void setIncrementValue(int value) {
        incrementcount = value;
    }

    public void onClick(View v) {

        Utils.hideSoftKeyboard(mContext);
        if (R.id.increment == v.getId()) {
            if (getCurrent() >= getQuantity()) {
                decrementValue();
            }
            if (getCurrent() != getQuantity())
                changeCurrent(mCurrent + getIncrementValue());
            mEnd = getQuantity();
            maxValue = getCurrent();
        } else if (R.id.decrement == v.getId()) {
            decrementValue();
        }
        validateInput(mText);

    }

    private void decrementValue() {
        if (getCurrent() != 0)
            mEnd = maxValue;
        else
            mEnd = 0;
        changeCurrent(mCurrent - getIncrementValue());
    }

    private String formatNumber(int value) {
        return (mFormatter != null) ? mFormatter.toString(value) : String
                .valueOf(value);
    }

    protected void changeCurrent(int currentValue) {
        int current;
        // Wrap around the values if we go past the start or end
        if (currentValue > mEnd) {
            current = mStart;
        } else if (currentValue < mStart) {
            current = mEnd;
        } else {
            current = currentValue;
        }
        mPrevious = mCurrent;
        mCurrent = current;

        notifyChange();
        updateView();
    }

    protected void notifyChange() {
        if (mListener != null) {
            mListener.onChanged(this, mPrevious, mCurrent, mPosition);
        }
    }

    protected void updateView() {
        /*
         * If we don't have displayed values then use the current number else
		 * find the correct value in the displayed values for the current
		 * number.
		 */
        if (mCurrent > 0) {
            if (mDisplayedValues == null) {
                mText.setText(formatNumber(mCurrent));
            } else {
                mText.setText(mDisplayedValues[mCurrent - mStart]);
            }
        }
        mText.setSelection(mText.getText().length());
    }

    private void validateCurrentView(CharSequence str) {
        int val = getSelectedPos(str.toString());
        if ((val >= mStart) && (val <= mEnd) && mCurrent != val) {
            mPrevious = mCurrent;
            mCurrent = val;
            notifyChange();
        }
        if (val >= getQuantity()) {
            Utils.showToast(mContext, getResources().getString(R.string.maximum_quantity) + Constant.SINGLE_SPACE + getQuantity(), Toast.LENGTH_SHORT);
        }

        updateView();
    }

    public void onFocusChange(View v, boolean hasFocus) {

		/*
         * When focus is lost check that the text field has valid values.
		 */
        if (!hasFocus) {
            validateInput(v);
        }
    }

    private void validateInput(View v) {
        String str = String.valueOf(((TextView) v).getText());

        switch (str) {
            case "":
                // Restore to the old value as we don't allow empty values
                updateView();
                break;
            case "0":
                // Restore to the old value as we don't allow empty values
                updateView();
                break;
            default:
                // Check the new value and ensure it's in range
                validateCurrentView(str);
                break;
        }
    }

    /**
     * We start the long click here but rely on the {@link NumberPickerTextView}
     * to inform us when the long click has ended.
     */
    public boolean onLongClick(View v) {

		/*
         * The text view may still have focus so clear it's focus which will
		 * trigger the on focus changed and any typed values to be pulled.
		 */
        mText.clearFocus();

        if (R.id.increment == v.getId()) {
            mIncrement = true;
            mHandler.post(mRunnable);
        } else if (R.id.decrement == v.getId()) {
            mDecrement = true;
            mHandler.post(mRunnable);
        }
        return true;
    }

    public void cancelIncrement() {
        mIncrement = false;
    }

    public void cancelDecrement() {
        mDecrement = false;
    }

    private int getSelectedPos(String strValue) {
        int value;
        try {
            value = Integer.parseInt(strValue);
            if (value >= getQuantity()) {
                value = getQuantity();
            }
            if (mDisplayedValues == null) {
                return value;
            } else {
                for (int i = 0; i < mDisplayedValues.length; i++) {

				/* Don't force the user to type in jan when ja will do */
                    if (mDisplayedValues[i].toLowerCase().startsWith(strValue.toLowerCase())) {
                        return mStart + i;
                    }
                }
            }
            return value;
        } catch (NumberFormatException e) {
            LogUtils.i(Constant.TAG_EXCEPTION, e.getMessage());
                /* Ignore as if it's not a number we don't care */
        }
        return mEnd;
    }

    /**
     * @return the current value.
     */
    public int getCurrent() {
        return mCurrent;
    }

    public void setCurrent(int current) {

        mCurrent = current;
        updateView();
    }

    public int getQuantity() {

        return quantity;
    }

    public void setQuantity(int value) {

        quantity = value;
    }



    public interface OnChangedListener {
        void onChanged(NumberPicker picker, int oldVal, int newVal, int position);
    }

    public interface Formatter {
        String toString(int value);
    }


    private class NumberRangeKeyListener extends NumberKeyListener {

        // XXX This doesn't allow for range limits when controlled by a
        // soft input method!
        public int getInputType() {
            return InputType.TYPE_CLASS_NUMBER;
        }

        @Override
        protected char[] getAcceptedChars() {
            return DIGIT_CHARACTERS;
        }

        @Override
        public CharSequence filter(@NonNull CharSequence source, int start, int end,
                                   Spanned dest, int dstart, int dend) {

            CharSequence filtered = super.filter(source, start, end, dest,
                    dstart, dend);
            if (filtered == null) {
                filtered = source.subSequence(start, end);
            }

            String result = String.valueOf(dest.subSequence(0, dstart))
                    + filtered + dest.subSequence(dend, dest.length());

            if ("".equals(result)) {
                return result;
            }
            int val = getSelectedPos(result);

			/*
             * Ensure the user can't type in a value greater than the max
			 * allowed. We have to allow less than min as the user might want to
			 * delete some numbers and then type a new number.
			 */
            if (val > mEnd) {
                return "";
            } else {
                return filtered;
            }
        }
    }

}
