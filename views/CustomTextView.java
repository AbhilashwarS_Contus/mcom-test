/**
 * CustomTextView.java
 * <p/>
 * This Class is the custom Textview class for Categories.
 *
 * @category Contus
 * @package com.contus.mcomm.views
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contussupport.ecommerce.R;


public class CustomTextView extends TextView {

    /**
     * The typed array.
     */
    private TypedArray textTypedArray;

    /**
     * Instantiates a new custom text view.
     *
     * @param context the context
     */
    public CustomTextView(Context context) {
        super(context);
    }

    /**
     * Instantiates a new custom text view.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * Instantiates a new custom text view.
     *
     * @param context  the context
     * @param attrs    the attrs
     * @param defStyle the def style
     */
    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    /**
     * Inits the.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    public void init(Context context, AttributeSet attrs) {
        try {
            textTypedArray = context.obtainStyledAttributes(attrs,
                    R.styleable.CustomWidget);
            int arrayCount = textTypedArray.getIndexCount();
            for (int i = 0; i < arrayCount; i++) {
                int attr = textTypedArray.getIndex(i);
                setTypeFace(attr);
            }
        } catch (Exception e) {
            LogUtils.e(Constant.TAG_EXCEPTION, e.getMessage());
        }
    }

    private void setTypeFace(int attr) {
        if (attr == R.styleable.CustomWidget_font_name) {
            Typeface font = Typeface.createFromAsset(getResources()
                    .getAssets(), textTypedArray.getString(attr));
            if (font != null) {
                this.setTypeface(font);
            }
        }
    }
}
