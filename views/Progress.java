/**
 * ProgressDialog.java
 * <p/>
 * Helper class that is used to generate progress.
 *
 * @category Contus
 * @package com.contus.mcomm.views
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.views;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.ViewGroup;
import android.view.Window;

import com.contus.mcomm.externallibraries.MaterialProgressBar;

/**
 * Created by user on 13-May-15.
 */
public class Progress {

    private Context mContext;

    public Progress(Context context) {
        this.mContext = context;
    }

    public Dialog showProgress() {
        Dialog pDialog = new Dialog(mContext);
        pDialog.setCancelable(false);
        pDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        pDialog.setContentView(new MaterialProgressBar(mContext), new ViewGroup.LayoutParams(100, 100));
        pDialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));
        pDialog.show();
        return pDialog;
    }

    public void dismissProgress(Dialog pDialog) {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }
}
