/**
 * CustomDialog.java
 * <p/>
 * The Class Utils is the helper class for validation, storing and retrieving preference data.
 *
 * @category Contus
 * @package com.contus.mcomm.utils
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.views;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.contus.mcomm.utils.Constant;
import com.contussupport.ecommerce.R;


public class CustomDialog extends DialogFragment {
    private int clickType;

    private OnDialogSelectedListener callback;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arg = getArguments();
        clickType = arg.getInt("key");
        try {
            callback = (OnDialogSelectedListener) getTargetFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement DialogClickListener interface");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.action_bar_dialog, container, false);
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        Button btDeltYes = (Button) view.findViewById(R.id.bt_delete_yes);
        Button btDeltNo = (Button) view.findViewById(R.id.bt_delete_no);
        TextView alertMessage = (TextView) view.findViewById(R.id.message);
        btDeltYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onYesClick(clickType);
            }
        });
        btDeltNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onNoClick(clickType);
            }
        });
        toolbar.setTitle(getResources().getString(R.string.dialog_title));
        switch (clickType) {
            case Constant.DIALOG_WISHLIST_DELETE:
            case Constant.DIALOG_ADDRESS_DELETE:
            case Constant.DIALOG_CART_DELETE:
                alertMessage.setText(getResources().getString(R.string.dialog_delete_title));
                break;
            case Constant.DIALOG_CART_UPDATE:
                alertMessage.setText(getResources().getString(R.string.dialog_update_title));
                break;
            default:
                break;
        }
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    // Container Activity must implement this interface
    public interface OnDialogSelectedListener {
        public void onYesClick(int type);

        public void onNoClick(int type);
    }
}

