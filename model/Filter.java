package com.contus.mcomm.model;

import com.contus.mcomm.utils.Constant;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 5/25/2015.
 */
public class Filter {
    @SerializedName(Constant.Filter.FILTER_CODE)
    private String filterCode;
    @SerializedName(Constant.Filter.FILTER_ATTR_LABEL)
    private String filterLabel;
    @SerializedName(Constant.Filter.FILTER_ID)
    private String filterId;
    @SerializedName(Constant.Filter.VALUES)
    private List<FilterValues> values;

    public String getFilterCode() {
        return filterCode;
    }

    public void setFilterCode(String filterCode) {
        this.filterCode = filterCode;
    }

    public List<FilterValues> getValues() {
        return values;
    }

    public void setValues(List<FilterValues> values) {
        this.values = values;
    }

    public String getFilterId() {
        return filterId;
    }

    public void setFilterId(String filterId) {
        this.filterId = filterId;
    }

    public String getFilterLabel() {
        return filterLabel;
    }

    public void setFilterLabel(String filterLabel) {
        this.filterLabel = filterLabel;
    }
}
