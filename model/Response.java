/**
 * HomePageResonpse.java
 * The Class allows access to properties using getter and setter methods.
 *
 * @category Contus
 * @package contus.com.mcomm.model
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.model;

import com.contus.mcomm.utils.Constant;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Response {
    /**
     * The result code.
     */
    @SerializedName(Constant.Common.AVAILABLE_STORES)
    public List<Store> stores;
    @SerializedName(Constant.Common.CATEGORIES)
    public List<Category> categories;
    @SerializedName(Constant.Common.COLLECTION)
    public List<Product> product;
    @SerializedName(Constant.Common.OFFER)
    public List<Product> offer;
    @SerializedName(Constant.Common.PRODUCT_TYPE)
    public String productTYpe;

}