package com.contus.mcomm.model;

import com.contus.mcomm.utils.Constant;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 5/25/2015.
 */
public class FilterValues {
    @SerializedName(Constant.Filter.FILTER_VALUE)
    private String value;
    @SerializedName(Constant.Filter.FILTER_LABEL)
    private String label;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
