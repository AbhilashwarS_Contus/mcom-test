package com.contus.mcomm.model;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 

import com.contus.mcomm.utils.Constant;
import com.google.gson.annotations.SerializedName;

/**
 * Entity mapped to table COUNTRY.
 */
public class Regions {

    private Long id;
    @SerializedName(Constant.Country.REGION_ID)
    private String regionId;
    @SerializedName(Constant.Country.NAME)
    private String name;
    @SerializedName(Constant.Country.CODE)
    private String code;

    public Regions() {
    }

    public Regions(Long id) {
        this.id = id;
    }

    public Regions(Long id, String countryId, String name, String code) {
        this.id = id;
        this.regionId = countryId;
        this.name = name;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }
}
