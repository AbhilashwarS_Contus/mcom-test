/**
 * HomePageResonpse.java
 * The Class allows access to properties using getter and setter methods.
 *
 * @category Contus
 * @package contus.com.mcomm.model
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.model;

import com.contus.mcomm.utils.Constant;
import com.google.gson.annotations.SerializedName;

public class Login extends ErrorResponse {


    @SerializedName(Constant.Profile.PROFILE_LAST_NAME)
    private String lastName;
    @SerializedName(Constant.Profile.PROFILE_CUSTOMER_ID)
    private String customerId;
    @SerializedName(Constant.Profile.CART_COUNT)
    private String cartCount;
    @SerializedName(Constant.Profile.TOKEN)
    private String token;
    @SerializedName(Constant.Profile.PROFILE_EMAIL)
    private String email;
    @SerializedName(Constant.Profile.PROFILE_FIRST_NAME)
    private String firstName;
    @SerializedName(Constant.Profile.NEWSLETTER)
    private String newsletter;
    @SerializedName(Constant.Profile.DATE_OF_BIRTH)
    private String dateOfBirth;


    public String getNewsletter() {
        return newsletter;
    }

    public void setNewsletter(String newsletter) {
        this.newsletter = newsletter;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCartCount() {
        return cartCount;
    }

    public void setCartCount(String cartCount) {
        this.cartCount = cartCount;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


}
