package com.contus.mcomm.model;

import com.contus.mcomm.utils.Constant;
import com.google.gson.annotations.SerializedName;


/**
 * Created by user on 5/20/2015.
 */
public class CartAddress {

    @SerializedName(Constant.Cart.IS_SHIPPING)
    public boolean isShipping;
    @SerializedName(Constant.Cart.IS_BILLING)
    public boolean isBilling;

    @SerializedName(Constant.Cart.BILLING)
    public Address billingAddress;
    @SerializedName(Constant.Cart.SHIPPING)
    public Address shippingAddress;

    public boolean isShipping() {
        return isShipping;
    }

    public void setIsShipping(boolean isShipping) {
        this.isShipping = isShipping;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public Address getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(Address billingAddress) {
        this.billingAddress = billingAddress;
    }

    public boolean isBilling() {
        return isBilling;
    }

    public void setIsBilling(boolean isBilling) {
        this.isBilling = isBilling;
    }
}
