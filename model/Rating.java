/**
 * HomePageResonpse.java
 * The Class allows access to properties using getter and setter methods.
 *
 * @category Contus
 * @package contus.com.mcomm.model
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.model;

import com.contus.mcomm.utils.Constant;
import com.google.gson.annotations.SerializedName;


public class Rating extends ErrorResponse {

    @SerializedName(Constant.Rating.RATING_STAR)
    private String star;
    @SerializedName(Constant.Rating.RATING_COUNT)
    private String count;

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
