/**
 * ForgotResponse.java
 * The Class allows access to properties using getter and setter methods.
 *
 * @category Contus
 * @package contus.com.mcomm.model
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.model;


import com.contus.mcomm.utils.Constant;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegionResponse extends ErrorResponse {

    @SerializedName(Constant.Common.RESULT)
    private List<Regions> regions;

    public RegionResponse() {
        // Empty constructor
    }

    public List<Regions> getRegions() {
        return regions;
    }

    public void setRegions(List<Regions> region) {
        this.regions = region;
    }
}

