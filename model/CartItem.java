package com.contus.mcomm.model;

import com.contus.mcomm.utils.Constant;
import com.google.gson.annotations.SerializedName;


/**
 * Created by user on 5/20/2015.
 */
public class CartItem {

    @SerializedName(Constant.Cart.CART_PRICE)
    private String price;
    @SerializedName(Constant.Cart.CART_STOCK_QTY)
    private String stockQty;
    @SerializedName(Constant.Cart.CART_NAME)
    private String name;
    @SerializedName(Constant.Cart.CART_ENTITY_ID)
    private String entityId;
    @SerializedName(Constant.Cart.CART_ROW_TOTAL)
    private String rowTotal;
    @SerializedName(Constant.Cart.CART_QUANTITY)
    private String qty;
    @SerializedName(Constant.Cart.CART_TYPE_ID)
    private String typeId;
    @SerializedName(Constant.Cart.CART_IMAGE_URL)
    private String imageUrl;
    @SerializedName(Constant.Cart.CART_CONFIG)
    private CartConfig config;
    @SerializedName(Constant.Cart.CART_IS_STOCK)
    private boolean isStock;
    private String updateQty;
    private boolean isUpdate = false;

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    public void setUpdate(boolean isUpdate) {
        this.isUpdate = isUpdate;
    }

    public String getStockQty() {
        return stockQty;
    }

    public void setStockQty(String stockQty) {
        this.stockQty = stockQty;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getRowTotal() {
        return rowTotal;
    }

    public void setRowTotal(String rowTotal) {
        this.rowTotal = rowTotal;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public CartConfig getConfig() {
        return config;
    }

    public void setConfig(CartConfig config) {
        this.config = config;
    }

    public boolean getIsStock() {
        return isStock;
    }

    public void setIsStock(boolean isStock) {
        this.isStock = isStock;
    }

    public String getUpdateQty() {
        return updateQty;
    }

    public void setUpdateQty(String updateQty) {
        this.updateQty = updateQty;
    }
}
