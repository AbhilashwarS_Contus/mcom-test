package com.contus.mcomm.utils;

import android.app.Activity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * GooglePlusHelper.java
 * <p/>
 * This is the helper class to extract details in google plus.
 *
 * @author Contus Team <developers@contus.in>
 * @version 1.0
 * @category Contus
 * @package com.contus.mcomm.utils
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
public class GooglePlusHelper {
    /**
     * The rc sign in.
     */
    public static final int RC_SIGN_IN = 0;

    /**
     * The tag.
     */
    public static final String TAG = "MainActivity";

    /**
     * The profile pic size.
     */
    public static final int PROFILE_PIC_SIZE = 400;

    /**
     * The m google api client.
     */
    public static GoogleApiClient mGoogleApiClient;

    /**
     * The m intent in progress.
     */
    public static boolean mIntentInProgress;

    /**
     * The m sign in clicked.
     */
    public static boolean mSignInClicked = true;

    /**
     * The m connection result.
     */
    public static ConnectionResult mConnectionResult;

    /**
     * The con.
     */
    public static Activity con;

    public GooglePlusHelper(Activity c) {
        con = c;
    }
}
