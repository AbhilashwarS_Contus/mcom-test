/**
 * LogUtils.java
 *
 * @category Contus
 * @package contus.com.mcomm.utils
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.utils;


import android.util.Log;

import com.contussupport.ecommerce.BuildConfig;

/**
 * LogUtils.java
 * <p/>
 * To get the different types of log in the application we can use this LogUtils
 * class. By default before creating the signed APK the application will be in
 * debug-able mode Once, you created the signed APK, SDK itself change to
 * debug-able false.Since,we have checked the condition BuildConfig.DEBUG and
 * showing log.
 */
public class LogUtils {
    private static final String LOG_PREFIX = "Log_";
    private static final int LOG_PREFIX_LENGTH = LOG_PREFIX.length();
    private static final int MAX_LOG_TAG_LENGTH = 23;

    // Default constructor.
    private LogUtils() {
    }

    /**
     * To Make log tag.
     *
     * @param str the str
     * @return the string
     */
    private static String makeLogTag(String str) {
        if (str.length() > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            return LOG_PREFIX
                    + str.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH
                    - 1);
        }
        return LOG_PREFIX + str;
    }

    /**
     * Don't use this when obfuscating class names!
     */
    public static String makeLogTag(Class<?> cls) {
        return makeLogTag(cls.getSimpleName());
    }

    /**
     * Log D.
     *
     * @param tag     the tag
     * @param message the message
     */
    public static void d(final String tag, String message) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, message);
        }
    }

    /**
     * Log D with throwable
     *
     * @param tag   the tag
     * @param cause the cause
     */
    public static void d(final String tag, Throwable cause) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, tag, cause);
        }
    }

    /**
     * Log V
     *
     * @param tag     the tag
     * @param message the message
     */
    public static void v(final String tag, String message) {
        if (BuildConfig.DEBUG) {
            Log.v(tag, message);
        }
    }

    /**
     * Log V with throwable
     *
     * @param tag     the tag
     * @param message the message
     * @param cause   the cause
     */
    public static void v(final String tag, String message, Throwable cause) {
        if (BuildConfig.DEBUG && Log.isLoggable(tag, Log.VERBOSE)) {
            Log.v(tag, message, cause);
        }
    }

    /**
     * Log I
     *
     * @param tag     the tag
     * @param message the message
     */
    public static void i(final String tag, String message) {
        if (BuildConfig.DEBUG) {
            Log.i(tag, message);
        }
    }

    /**
     * Log I with throwable
     *
     * @param tag   the tag
     * @param cause the cause
     */
    public static void i(final String tag, Throwable cause) {
        if (BuildConfig.DEBUG) {
            Log.i(tag, tag, cause);
        }
    }

    /**
     * Log W
     *
     * @param tag     the tag
     * @param message the message
     */
    public static void w(final String tag, String message) {
        if (BuildConfig.DEBUG) {
            Log.w(tag, message);
        }
    }

    /**
     * Log W with throwable
     *
     * @param tag   the tag
     * @param cause the cause
     */
    public static void w(final String tag, Throwable cause) {
        if (BuildConfig.DEBUG) {
            Log.w(tag, tag, cause);
        }
    }

    /**
     * Log E
     *
     * @param tag     the tag
     * @param message the message
     */
    public static void e(final String tag, String message) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, message);
        }
    }

    /**
     * Log E with throwable.
     *
     * @param tag   the tag
     * @param cause the cause
     */
    public static void e(final String tag, Throwable cause) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, tag, cause);
        }
    }

}
