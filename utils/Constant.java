/**
 * Constant.java
 * <p/>
 * This class is used to declare the constand variable.
 *
 * @category Contus
 * @package contus.com.mcomm.utils
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.utils;

public class Constant extends ConstantApi {

    public static final String DEFAULT_STORE_ID_VALUE = "1";
    public static final String WEBSITE_ID_VALUE = "1";
    public static final String DEFAULT_REVIEW_STATUS = "1";
    public static final String DEFAULT_CURRENCY_VALUE = "$";
    public static final String DEFAULT_CURRENCY_CODE = "USD";

    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";

    /*Fragment Names*/
    public static final String HOME_FRAGMENT = "HomeFragment";

    /*Sign In*/

    public static final int MINIMUM_PASSWORD_LENTH = 6;
    public static final int MAXIMUM_PASSWORD_LENTH = 15;

    public static final String DD_MM_YYYY = "dd-MM-yyyy";
    public static final String MM_DD_YYYY = "MM-dd-yyyy";
    public static final String MMM_DD_YYYY = "MMM dd, yyyy";
    /**
     * The Constant YYYY_MM_DD.
     */
    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    public static final int PROFILE_LIMIT_COUNT = 20;
    public static final int LIMIT_COUNT = 10;
    public static final int PAGE_VALUE = 1;
    public static final int REVIEW_LIMIT_COUNT = 5;
    /**
     * The Constant ZERO.
     */
    public static final String ZERO = "0";

    /**
     * The Constant SLASH.
     */
    public static final String HYPHEN = "-";

    public static final int SUCCESS_VALUE = 1;

    public static final int CATEGORY_PRODUCT = 1;
    public static final int OFFER_PRODUCT = 2;
    public static final int NEW_PRODUCT = 3;

    public static final int DEFAULT_PARENTID = 2;
    public static final String TAG_EXCEPTION = "Exception:";

    public static final String SELECTED_ID = "selected_id";
    public static final String SELECTED_NAME = "selected_name";

    public static final String IMG_URLS = "imgUrls";
    public static final String IMG_POSITION = "position";


    public static final String TYPE_CONFIGURABLE = "configurable";
    public static final String TYPE_SIMPLE = "simple";
    public static final String DIALOG_CART_PARAM = "key";
    /*Sort Options*/
    public static final String SORT_NAME = "name";
    public static final String SORT_PRICE = "price";
    public static final String SORT_RATING = "rating";
    public static final String SORT_ASC = "asc";
    public static final String SORT_DESC = "desc";

    public static final String NAVIGATION_TYPE = "navType";
    public static final String SINGLE_VIEW = "singleView";
    public static final int TYPE_IMAGES = 1;
    public static final int TYPE_CART = 2;
    public static final int TYPE_SEARCH = 8;

    public static final int TYPE_DESCRIPTION = 3, FRAGMENT_DELAY = 300, CASE_HOME = -1, CASE_MYACCOUNT = -3, CASE_AUTHENTICATION = -2;
    public static final int TYPE_MY_PROFILE = 4;
    public static final int TYPE_MY_WISHLIST = 5;
    public static final int TYPE_MY_ORDER = 6;
    public static final int TYPE_MY_ADDRESS = 7;
    public static final int TYPE_MY_NEW_ADDR = 8;
    public static final int TYPE_REMOVE_MY_NEW_ADDR = 9;
    public static final int TYPE_MY_ORDER_DETAILS = 10;
    public static final String ADD_ADDRESS = "add_address";
    public static final String UPDATE_ADDRESS = "add_update";

    //Preference key
    public static final String PREF_CONSTUMER_ID = "pre_customer_id";
    public static final String PREF_TOKEN = "pre_token";
    public static final String PREF_LOGINOBJECT = "pre_login_object";
    public static final String PREF_IS_LOGIN = "pre_is_login";
    public static final String PREF_CART_ITEM_COUNT = "cart_item_count";
    public static final String PREF_CART_QUOTE_ID = "cart_qoute_id";
    public static final String PREF_STORE_OBJECT = "pre_store_object";
    public static final String PREF_SELECTION = "selection";


    /**
     * The Constant SINGLE_SPACE.
     */
    public static final String SINGLE_SPACE = " ";
    //database
    public static final String SQL_WHERE = SINGLE_SPACE + "where" + SINGLE_SPACE;
    /**
     * The Constant EMPTY.
     */
    public static final String EMPTY = "";
    public static final String SEARCH_PAGE = "search";
    public static final String CATEGORY_PAGE = "category";
    public static final int CHECK_INTERNET = 1000;
    public static final int CHECK_INTERNET_LOGIN = 1001;
    public static final int CHECK_INTERNET_LOGIN_REDIRECT = 1002;
    public static final int CHECK_USER_LOGIN = 1003;
    public static final int CHECK_USER_LOGIN_TOAST = 1004;
    public static final int CHECK_USER_LOGIN_INTERNET = 1005;
    //onback press
    public static final String PRODUCTLIST_FRAGMENT = "ProductListFragment";
    public static final String PRODUCTDETAILS_FRAGMENT = "ProductDetailsFragment";
    public static final int DIALOG_CART_DELETE = 2000;
    public static final int DIALOG_CART_UPDATE = 2001;
    public static final int DIALOG_WISHLIST_DELETE = 2002;
    public static final int DIALOG_ADDRESS_DELETE = 2003;
    public static final String UNAUTHORIZED_VALUE = "unauthorized";
    //payment method
    public static final String CASHONDELIVERY = "cashondelivery";
    public static final String FREE_SHIPPING = "freeshipping_freeshipping";

}
