/**
 * Utils.java
 * <p/>
 * The Class Utils is the helper class for validation, storing and retrieving preference data.
 *
 * @category Contus
 * @package com.contus.mcomm.utils
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Base64;
import android.view.Gravity;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.contussupport.ecommerce.R;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;


public class Utils {

    // Default constructor.
    private Utils() {
    }

    /**
     * Used to Check the Network Connectivity.
     *
     * @param context ;
     * @return true; If network available.
     */
    public static boolean isNetConnected(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        return conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected();
    }

    /**
     * Show no Internet Connection dialog.
     *
     * @param context ; Context of an activity
     * @return null;
     */
    public static void showNetworkError(Context context) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage(context.getString(R.string.no_internet));

        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public static String bitmapToString(Bitmap bitmap) {
        ByteArrayOutputStream bitmapOutput = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, bitmapOutput);
        byte[] b = bitmapOutput.toByteArray();
        return Base64.encodeToString(b, Base64.DEFAULT);

    }

    public static int calculatePercentage(Float actualPrice, Float discountedPrice) {
        int percent = (int) (discountedPrice * 100.0f / actualPrice);
        return 100 - percent;
    }

    /**
     * Save preferences.
     *
     * @param context the context
     * @param key     the key
     * @param value   the value
     */
    public static void savePreferences(Context context, String key,
                                       String value) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.commit();
    }

    /**
     * Read preferences.
     *
     * @param context      the context
     * @param key          the key
     * @param defaultValue the default value
     * @return the string
     */
    public static String getPreferences(Context context, String key,
                                        String defaultValue) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context);
        return sp.getString(key, defaultValue);
    }

    /**
     * Clear preferences.
     *
     * @param context the context
     * @param key     the key
     * @return the string
     */
    public static void clearPreferences(Context context, String key) {
        SharedPreferences sp = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.remove(key);
        editor.commit();
    }


    /**
     * Check if the provided email address is valid.
     *
     * @param loginEmailId the login email id
     * @return true, if successful
     */
    public static boolean validateEmail(String loginEmailId) {
        return Utils.getValidEmailPattern().matcher(loginEmailId).matches();
    }

    /**
     * Returns valid email address pattern.
     *
     * @return the valid email pattern
     */
    public static Pattern getValidEmailPattern() {
        return Pattern
                .compile("^[A-Za-z0-9-_]+(\\.[A-Za-z0-9-_]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
    }

    /**
     * Returns the data binded in the view.
     *
     * @param edtView the EditText View.
     * @return String, the binded String.
     */
    public static String getString(EditText edtView) {
        return edtView.getText().toString().trim();
    }

    /**
     * Used to Show Toast.
     *
     * @param context ;
     * @param text    the text
     * @param time    the time
     * @return null.
     */
    public static void showToast(Context context, String text, int time) {
        Toast toast = Toast.makeText(context, text, time);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    /**
     * Gets the birth day date.
     *
     * @param selectedYear  the selected year
     * @param selectedMonth the selected month
     * @param selectedDay   the selected day
     * @return the birth day date
     */
    public static String getBirthDayDate(int selectedYear, int selectedMonth,
                                         int selectedDay) {
        int month = selectedMonth + 1;
        String formattedMonth = String.valueOf(month);
        String formattedDayOfMonth = String.valueOf(selectedDay);
        if (month < 10) {
            formattedMonth = Constant.ZERO + month;
        }
        if (selectedDay < 10) {
            formattedDayOfMonth = Constant.ZERO + selectedDay;
        }
        return String.valueOf(selectedYear) + Constant.HYPHEN
                + String.valueOf(formattedMonth) + Constant.HYPHEN
                + String.valueOf(formattedDayOfMonth);
    }

    /**
     * Gets the date picker dialog.
     *
     * @param activity           the activity
     * @param datePickerListener the date picker listener
     * @param selectedDay        the selected day
     * @param selectedMonth      the selected month
     * @param selectedYear       the selected year
     * @return the date picker dialog
     */
    public static void getDatePickerDialog(Activity activity,
                                           DatePickerDialog.OnDateSetListener datePickerListener,
                                           int selectedDay, int selectedMonth, int selectedYear) {

        Calendar c = Calendar.getInstance();
        int year, month, day = 0;
        if (selectedYear == 0 && selectedMonth == 0 && selectedDay == 0) {
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        } else {
            year = selectedYear;
            month = selectedMonth;
            day = selectedDay;
        }
        Date minDate = new Date();
        c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH),
                c.get(Calendar.DAY_OF_MONTH) - 1, 0, 0);
        minDate.setTime(c.getTimeInMillis());
        DatePickerDialog dpdFromDate = new DatePickerDialog(activity,
                datePickerListener, year, month, day);
        dpdFromDate.getDatePicker().setMaxDate(System.currentTimeMillis());
        dpdFromDate.show();

    }

    /**
     * Returns a string containing the tokens joined by delimiters.
     *
     * @param tokens an array objects to be joined. Strings will be formed from
     *               the objects by calling object.toString().
     */
    public static String join(CharSequence delimiter, String[] tokens) {
        StringBuilder sb = new StringBuilder();
        boolean firstTime = true;
        for (String token : tokens) {
            if (!TextUtils.isEmpty(token)) {
                if (firstTime) {
                    firstTime = false;
                } else {
                    sb.append(delimiter);
                }
                sb.append(token);
            }
        }

        return sb.toString();
    }

    /**
     * Creates a {@code String} representation of the {@code Object[]} passed.
     * The result is surrounded by brackets ({@code "[]"}), each
     * element is converted to a {@code String} via the
     * {@link String#valueOf(Object)} and separated by {@code ", "}.
     * If the array is {@code null}, then {@code "null"} is returned.
     *
     * @param array the {@code Object} array to convert.
     * @return the {@code String} representation of {@code array}.
     * @since 1.5
     */
    public static JSONArray toJsonArray(String[] array) {
        if (array == null || array.length == 0) {
            return null;
        }

        JSONArray ary = new JSONArray();
        boolean firstTime = true;
        for (int i = 0; i < array.length; i++) {
            if (!TextUtils.isEmpty(array[i])) {

                ary.put(array[i]);
            }
        }

        return ary;
    }

    public static String changeDateFormat(String parseDate, boolean time) {
        SimpleDateFormat readFormat;
        if (time)
            readFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        else
            readFormat = new SimpleDateFormat("yyyy-MM-dd");

        SimpleDateFormat writeFormat = new SimpleDateFormat("MMM dd, yyyy");

        Date date = null;
        try {
            date = readFormat.parse(parseDate);

        } catch (ParseException e) {
            LogUtils.i(Constant.TAG_EXCEPTION, e.toString());
        }
        return writeFormat.format(date);
    }

    public static boolean checkFreeShipping(String amount) {
        return (int) (Float.parseFloat(amount)) == 0 ? true : false;
    }

    /**
     * Hides the keyboard.
     */
    public static void hideSoftKeyboard(Context mContext) {
        try {
            InputMethodManager inputMethodManager = (InputMethodManager) mContext
                    .getSystemService(mContext.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(
                    ((Activity) mContext).getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {
            LogUtils.e(Constant.TAG_EXCEPTION, e.toString());
        }
    }
}
