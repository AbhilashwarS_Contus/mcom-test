/**
 * Constant.java
 * <p/>
 * This class is used to declare the constand variable.
 *
 * @category Contus
 * @package contus.com.mcomm.utils
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.utils;

public class ConstantApi {

    //public static final String BASEURL = "http://52.74.232.15/staging/rest";
   // public static final String BASEURL = "http://mcommdemo.contus.us/rest";
    public static final String BASEURL = "http://release.contus.us/mobi/MCOM1025/rest";
    public static final String DBNAME = "mcom";
    public static final String CUSTOMER_ID = "customer_id";
    public static final String FIRST_NAME = "firstname";
    public static final String LAST_NAME = "lastname";
    public static final String CURRENCT_SYMBOL = "currency_symbol";
    public static final String ENTITY_ID = "entity_id";
    public static final String NAME = "name";
    public static final String PRICE = "price";
    public static final String QUANTITY = "qty";
    public static final String IMAGE_URL = "image_url";
    public static final String IS_STOCK = "is_stock";
    public static final String PRICE_WITH_TAX = "regular_price_with_tax";
    public static final String FINAL_PRICE_WITH_TAX = "final_price_with_tax";
    public static final String TYPE_ID = "type_id";
    public static final String IS_SALEABLE = "is_saleable";
    public static final String RATINGS = "rating";
    public static final String SHIPPING_AMOUNT = "shipping_amount";
    public static final String GRAND_TOTAL = "grand_total";
    public static final String ITEM_COUNT = "item_count";
    public static final String ROW_TOTAL = "row_total";
    public static final String QUOTE_ID = "quote_id";
    public static final String LABEL = "label";
    public static final String CODE = "code";
    public static final String ORDER_ID = "order_id";
    public static final String TITLE = "title";
    public static final String VALUE = "value";
    public static final String SUMMARY_RATINGS = "summary_rating";
    public static final String COUNTRY_ID = "country_id";
    public static final String STORE_ID = "store_id";


    public class Common {

        public static final String TOTAL_COUNT = "total_count";
        public static final String RESULT = "result";
        public static final String MESSAGE = "message";
        public static final String ERROR = "error";
        public static final String SUCCESS = "success";
        public static final String PRODUCT_TYPE = "product_type";
        public static final String COLLECTION = "collection";
        public static final String OFFER = "offer";
        public static final String CATEGORIES = "categories";
        public static final String CONTENT = "content";
        public static final String AVAILABLE_STORES = "available_stores";

        private Common() {

        }

    }

    public class Stroe {

        public static final String STORE_CURRENCY_CODE = "currency_code";
        public static final String STORE_CURRENCY_SYMBOL = "currency_symbol";

        private Stroe() {

        }

        public static final String STORE_NAME = NAME;

        public static final String STORE_IDS = STORE_ID;


    }

    //Category api
    public class Category {
        public static final String CATEGORYID = "category_id";
        public static final String PARENTID = "parent_id";
        public static final String POSITION = "position";
        public static final String IS_CHILD = "is_child";

        private Category() {

        }

        public static final String CATEGORY_NAME = NAME;
        public static final String CAT_IMAGE_URL = IMAGE_URL;


    }

    //Product api
    public class Product {
        public static final String SHORT_DESCRIPTION = "short_description";
        public static final String REVIEWS_COUNT = "total_reviews_count";
        public static final String PRODUCT_URL = "url";
        public static final String PRODUCT_ENTITY_ID = ENTITY_ID;
        public static final String HAS_OPTIONS = "has_custom_options";
        public static final String IMAGES = "images";
        public static final String REVIEW = "reviews";
        public static final String PRODUCT_NAME = NAME;
        public static final String CONFIG = "config";
        public static final String ATTRIBUITE = "attr";
        public static final String IS_WISHLIST = "is_wishlist";
        public static final String PRODUCT_IMAGE_URL = IMAGE_URL;
        public static final String DESCRIPTION = "description";

        private Product() {

        }

        public static final String PRODUCT_IS_STOCK = IS_STOCK;

        public static final String PRODUCT_PRICE_WITH_TAX = PRICE_WITH_TAX;

        public static final String PRODUCT_FINAL_PRICE_WITH_TAX = FINAL_PRICE_WITH_TAX;

        public static final String PRODUCT_TYPE_ID = TYPE_ID;


        public static final String PRODUCT_RATINGS = SUMMARY_RATINGS;


        public static final String RATING = RATINGS;


        public static final String PRODUCT_IS_SALEABLE = IS_SALEABLE;


    }

    //Attribute api
    public class Attribute {
        public static final String CODE = "code";
        public static final String ATTRIBUTE_ID = "attribute_id";
        public static final String CONFIGID = "config_id";
        public static final String ATTRID = "attr_id";
        public static final String ATTR_TITLE = TITLE;
        public static final String STOCK_QTY = "stock_qty";
        public static final String IS_STOCK = "is_stock";

        private Attribute() {

        }

        public static final String ATTR_PRICE = PRICE;


        public static final String ATTR_LABEL = LABEL;
        public static final String ATTR_VALUE = VALUE;


    }

    //Review api
    public class Review {
        public static final String DETAILS = "detail";
        public static final String AUTHOR = "author";
        public static final String DATE = "date";
        public static final String REVIEWS_TITLE = TITLE;

        private Review() {

        }


        public static final String REVIEW_RATING = RATINGS;


    }

    //cart
    public class Wishlist {
        public static final String ADDED_DATE = "added_date";

        private Wishlist() {

        }

        public static final String WISHLIST_ENTITY_ID = ENTITY_ID;


        public static final String WISHLIST_NAME = NAME;
        public static final String WISHLIST_IMAGE_URL = IMAGE_URL;
        public static final String WISHLIST_REGULAR_PRICE = PRICE_WITH_TAX;
        public static final String WISHLIST_FINAL_PRICE = FINAL_PRICE_WITH_TAX;
        public static final String WISHLIST_IS_SALEABLE = IS_SALEABLE;
        public static final String WISHLIST_IS_STOCK = IS_STOCK;
        public static final String WISHLIST_TYPE_ID = TYPE_ID;
        public static final String WISHLIST_SUMMARY_RATING = RATINGS;


    }

    //cart
    public class Cart {
        public static final String ITEM_QTY = "items_qty";
        public static final String SUBTOTAL = "subtotal";
        public static final String DISCOUNT = "discount";
        public static final String CART_ITEM_COUNT = ITEM_COUNT;
        public static final String COUPON_CODE = "coupon_code";
        public static final String TAX = "tax";
        public static final String IS_BILLING = "is_billing";
        public static final String IS_SHIPPING = "is_shipping";
        public static final String CART_GRANT_TOTAL = GRAND_TOTAL;
        public static final String BILLING = "billing";
        public static final String SHIPPING = "shipping";
        public static final String CART_ADDRESS = "address";
        public static final String CART_ITEMS = "items";
        public static final String CART_STOCK_QTY = "stock_qty";
        public static final String CART_QUOTE_ID = QUOTE_ID;
        public static final String CART_CONFIG = "config";
        public static final String CART_INFO = "info_buyRequest";
        public static final String CART_ATTR_INFO = "attributes_info";
        public static final String CART_ATTR = "super_attribute";
        public static final String CART_SHIPPING_AMOUNT = SHIPPING_AMOUNT;
        public static final String OPTIONS = "options";
        public static final String OPTION_ID = "option_id";
        public static final String OPTION_TYPE = "option_type";
        public static final String OPTION_VALUE = "option_value";

        private Cart() {

        }


        public static final String CART_IS_STOCK = IS_STOCK;

        public static final String CART_TYPE_ID = TYPE_ID;

        public static final String CART_ENTITY_ID = ENTITY_ID;

        public static final String CART_NAME = NAME;


        public static final String CART_IMAGE_URL = IMAGE_URL;
        public static final String CART_PRICE = PRICE;
        public static final String CART_QUANTITY = QUANTITY;
        public static final String CART_ROW_TOTAL = ROW_TOTAL;


        public static final String CART_LABEL = LABEL;
        public static final String CART_VALUE = VALUE;


        public static final String OPTION_LABEL = LABEL;


    }

    //Login api

    public class Profile {

        public static final String NEWSLETTER = "newsletter";
        public static final String DATE_OF_BIRTH = "dob";
        public static final String TOKEN = "token";
        public static final String PROFILE_CUSTOMER_ID = CUSTOMER_ID;
        public static final String CART_COUNT = "cart_count";
        public static final String PROFILE_EMAIL = "email";

        private Profile() {

        }

        public static final String PROFILE_FIRST_NAME = FIRST_NAME;

        public static final String PROFILE_LAST_NAME = LAST_NAME;


    }

    //Address api
    public class Address {

        public static final String CREATED_AT = "created_at";
        public static final String UPDATED_AT = "updated_at";
        public static final String MIDDLE_NAME = "middlename";
        public static final String COMPANY = "company";
        public static final String CITY = "city";
        public static final String REGION = "region";
        public static final String ADDRESS_LAST_NAME = LAST_NAME;
        public static final String POSTCODE = "postcode";
        public static final String TELEPHONE = "telephone";
        public static final String FAX = "fax";
        public static final String ADDRESS_FIRST_NAME = FIRST_NAME;
        public static final String VAT_ID = "vat_id";
        public static final String REGION_ID = "region_id";
        public static final String REGION_CODE = "region_code";
        public static final String SUFFIX = "suffix";
        public static final String PREFIX = "prefix";
        public static final String ADDRESS_COUNTRY_ID = COUNTRY_ID;
        public static final String ENTITY_ID = "entity_id";
        public static final String STREET = "street";
        public static final String COUNTRY_NAME = "country_name";
        public static final String IS_DEFAULT_BILLING = "is_default_billing";
        public static final String IS_DEFAULT_SHIPPING = "is_default_shipping";

        private Address() {

        }


        public static final String ADDRESS_CUSTOMER_ID = CUSTOMER_ID;
        public static final String ADDRESS_STORE_ID = STORE_ID;


    }

    public class Order {

        public static final String ATTRIBUTE_INFO = "attributes_info";
        public static final String OPTIONS = "options";
        public static final String STATUS = "status";
        public static final String ORDER_DATE = "order_date";
        public static final String SHIP_NAME = "ship_name";
        public static final String ORDER_ROW_TOTAL = ROW_TOTAL;
        public static final String ORDER_ITEMS = "items";
        public static final String ORDER_STATUS = "status";
        public static final String SHIPPING_METHOD = "shipping_method";
        public static final String ORDER_IMAGE_URL = IMAGE_URL;
        public static final String PAYMENT_METHOD = "payment_method";
        public static final String DISCONT_AMOUNT = "discount_amount";
        public static final String TAX_AMOUNT = "tax_amount";
        public static final String ORDER_QUANTITY = QUANTITY;
        public static final String SUB_AMOUNT = "sub_amount";
        public static final String TOTAL_AMOUNT = "total_amount";

        private Order() {

        }

        public static final String ORDER_PRICE = PRICE;

        public static final String ORDER_PRODUCT_NAME = NAME;

        public static final String ORDER_IDS = ORDER_ID;


        public static final String ITEM_AMOUNT = ITEM_COUNT;


        public static final String ORDER_ITEM_COUNT = ITEM_COUNT;
        public static final String ORDER_GRAND_TOTAL = GRAND_TOTAL;
        public static final String CURRENCY = CURRENCT_SYMBOL;


        public static final String ORDER_SHIPPING_AMOUNT = SHIPPING_AMOUNT;


        public static final String ORDER_ENTITY_ID = ENTITY_ID;


    }

    public class Rating {
        public static final String RATING_STAR = "star";
        public static final String RATING_COUNT = "count";
        public static final String REVIEW_STATUS = "review_status";
        public static final String RAT_RATING = RATINGS;
        public static final String REVIEW_COUNT = "total_reviews_count";
        public static final String REVIEW = "reviews";

        private Rating() {

        }

        public static final String RATINGS_SUMMARY = SUMMARY_RATINGS;


    }

    public class Filter {


        public static final String FILTER_CODE = "attribute_code";
        public static final String FILTER_ATTR_LABEL = "attribute_label";
        public static final String FILTER_ID = "attribute_id";
        public static final String VALUES = "values";

        private Filter() {

        }

        public static final String FILTER_VALUE = VALUE;
        public static final String FILTER_LABEL = LABEL;


    }

    public class Checkout {
        public static final String SHIPPING_METHODS = "shipping_methods";
        public static final String PAYMNET_METHODS = "payment_methods";
        public static final String CC_TYPES = "cc_types";
        public static final String CARRIER_NAME = "carrierName";
        public static final String CHECKOUT_ADDRESS = "address";
        public static final String PAYMNET_CODE = CODE;
        public static final String SHIPPING_TITLE = "method_title";
        public static final String SHIPPING_DESC = "method_description";
        public static final String METHOD = "method";
        public static final String PAYMNET_TITLE = TITLE;
        public static final String SHIPPING_CARRIER_TITLE = "carrier_title";
        public static final String SHIPPING_CARRIER = "carrier";

        private Checkout() {

        }


        public static final String SHIPPING_PRICE = PRICE;


        public static final String SHIPPING_CODE = CODE;


        public static final String PLACE_ORDER_ID = ORDER_ID;


    }

    public class Country {

        public static final String NAME = "name";
        public static final String REGION_ID = "region_id";
        public static final String CODE = "code";

        private Country() {

        }

        public static final String COUNTRY_IDS = COUNTRY_ID;


    }

    //cart
    public class Request {
        // Api base url

        public static final String HOME_ACTION = "homepage";
        public static final String CATEGORY_PRODUCT_ACTION = "category_products";
        public static final String PRODUCT_DETAIL_ACTION = "productdetail";
        public static final String LOGIN_ACTION = "login";
        public static final String LOGIN_ACTION_SOCIAL = "social_login";

        public static final String ACTION = "action";
        public static final String STORE_ID = "store_id";
        public static final String WEBSITE_ID = "website_id";

        public static final String PAGE = "page";
        public static final String EMAIL = "email";
        public static final String SIGNIN_SIGNATURE = "login_signature";
        public static final String LIMIT = "limit";
        public static final String SORTBY = "sortby";
        public static final String ORDERBY = "orderby";
        public static final String CUSTOMER_ID = "customer_id";
        public static final String CATEGORY_ID = "category_id";
        public static final String PRODUCT_ID = "product_id";
        public static final String CURRENCY_CODE = "currencyCode";
        public static final String COUNTRY_VALUE = "US";
        public static final String SUPER_ATTRIBUTE = "super_attribute";
        public static final String PRODUCT_QUANTITY = QUANTITY;
        public static final String CUSTOM_OPTION = "custom_option";
        /**
         * Cart*
         */
        public static final String ADD_CART_ACTION = "add_tocart";
        public static final String GET_CART_INFO_ACTION = "cart_list";
        public static final String UPDATE_CART_ACTION = "update_cart";
        public static final String DELETE_CART_ACTION = "deletefromcart";
        /*Profile*/
        public static final String GROUP_ID = "group_id";
        public static final String FIRST_NAME = "firstname";
        public static final String LAST_NAME = "lastname";
        public static final String NEWSLETTER = "newsletter";
        public static final String DATE_OF_BIRTH = "dob";
        public static final String REGISTER_ACTION = "customer_register";
        public static final String CART_REQ_QUOTE_ID = QUOTE_ID;
        public static final String CUSTOMER_UPDATE_ACTION = "customer_update";
        public static final String GROUP_ID_VALUE = "1";
        public static final String FORGOT_ACTION = "forgot_signature";
        public static final String CHANGE_SIGNATURE_ACTION = "change_signature";
        public static final String OLD_SIGNATURE = "old_signature";
        public static final String NEW_SIGNATURE = "new_signature";
        public static final String TOKEN = "token";
        public static final String PERSONAL_ACTION = "personal_detail";
        //Address
        public static final String ADD_ADDRESS_ACTION = "address_add";
        public static final String UPDATE_ADDRESS_ACTION = "address_update";
        public static final String DELETE_ADDRESS_ACTION = "address_delete";
        public static final String ADDRESS_ACTION = "address_collection";
        public static final String ADDRESS_ID = "address_id";
        public static final String STREET = "street";
        public static final String SHIPPING_ADDRESS = "shipping_address";
        public static final String CITY = "city";
        public static final String POSTCODE = "postcode";
        public static final String TELEPHONE = "telephone";
        public static final String REGION_CODE = "region_code";
        public static final String REGION = "region";
        public static final String COUNTRY_ID = "country_id";
        public static final String IS_DEFAULT_BILLING = "is_default_billing";
        public static final String IS_DEFAULT_SHIPPING = "is_default_shipping";
        //orders
        public static final String ORDER_LIST_ACTION = "myorders_list";
        public static final String ORDER_DETAILS_ACTION = "myorder_detail";
        public static final String ORDER_ID = "order_id";
        //Wishlist
        public static final String WISHLIST_ACTION = "wishlist_list";
        public static final String ADD_WISHLIST_ACTION = "add_wishlist";
        public static final String DELETE_WISHLIST_ACTION = "delete_wishlist";
        //Static apge
        public static final String PAGE_KEY = "page_key";
        public static final String STATIC_PAGE_ACTION = "static_page";
        public static final String ABOUT_PAGE = "about-magento-demo-store";
        public static final String SERVICE_PAGE = "customer-service";
        public static final String PRIVACY_PAGE = "privacy-policy-cookie-restriction-mode";
        //review
        public static final String REVIEW_NAME = "customer_name";
        public static final String REVIEW_TITLE = "review_title";
        public static final String REVIEW_DESCRIPTION = "review_description";
        public static final String REVIEW_STATUS = "review_status";
        public static final String ADD_REVIEW_ACTION = "add_review";
        //Filter
        public static final String FILTERS_ACTION = "filters";
        //Search
        public static final String SEARCH_ACTION = "searchproducts";
        public static final String SEARCH_TERM = "search_term";
        //checkout
        public static final String ADD_ADDRESS_CART = "add_address_tocart";
        public static final String BILLING_ADDRESS_ID = "billing_address_id";
        public static final String SHIPPING_ADDRESS_ID = "shipping_address_id";
        public static final String PLACE_ORDER_ACTION = "place_order";
        public static final String SHIPPING_METHOD = "shipping_method";
        public static final String REVIEW_RATING = RATINGS;
        public static final String PAYMNET_METHOD = "payment_method";
        //coupon
        public static final String APPLY_COUPON = "apply_coupon";
        public static final String CANCEL_COUPON = "cancel_coupon";
        public static final String COUPON_CODE = "coupon_code";
        public static final String COUNTRY_ACTION = "country";
        public static final String STATE_ACTION = "state";
        public static final String COUNTRY_CODE = "countrycode";
        //Review
        public static final String REVIEW_LIST_ACTION = "reviews_list";

        private Request() {

        }


    }

}

