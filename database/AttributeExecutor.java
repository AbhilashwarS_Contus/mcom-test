/**
 * CategoryExecutor.java
 * <p/>
 * This is the Helper class that is used to insert and get the category data from database.
 *
 * @category Contus
 * @package com.contus.mcomm.database
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.database;

import android.content.Context;
import android.database.Cursor;
import android.text.TextUtils;

import com.contus.mcomm.model.Attribute;
import com.contus.mcomm.model.CustomAttribute;
import com.contus.mcomm.utils.Constant;
import com.contussupport.ecommerce.McommApplication;

import java.util.ArrayList;
import java.util.List;

public class AttributeExecutor {

    private final DaoSession daoSession;
    private final CustomAttributeDao customAttributeDao;
    private final AttributeDao attributeDao;
    private String distinctQuery;

    /**
     * This function called at the time of object creation.
     *
     * @param context
     */
    public AttributeExecutor(Context context) {
        McommApplication myApplication = (McommApplication) context.getApplicationContext();
        daoSession = myApplication.getDaoSession();
        customAttributeDao = daoSession.getCustomAttributeDao();
        attributeDao = daoSession.getAttributeDao();
    }

    /**
     * Clear the catgeory table session
     */
    public void closeSession() {
        daoSession.clear();
    }

    /**
     * Insert the catgeory data.
     *
     * @param attribute
     * @return long
     */
    public long insertData(final int productId, final List<CustomAttribute> attribute) {

        daoSession.runInTx(new Runnable() {

            @Override
            public void run() {
                deleteAll();
                int count = attribute.size();
                for (int i = 0; i < count; i++) {
                    CustomAttribute message = attribute.get(i);
                    message.setProductId(productId);
                    customAttributeDao.insertOrReplace(message);
                    List<Attribute> attrValue = message.getAttribute();
                    for (Attribute value : attrValue) {
                        value.setProductId(productId);
                        value.setConfigId(message.getConfigId());
                        attributeDao.insertOrReplace(value);
                    }
                }
                closeSession();
            }
        });
        return 1;

    }

    /**
     * To delete all data from product table
     */
    public void deleteAll() {
        customAttributeDao.deleteAll();
        attributeDao.deleteAll();
        closeSession();
    }

    /**
     * Gets the attribute collection by code.
     *
     * @param code
     */
    public List<CustomAttribute> getAttributeByCode(String code, int productId) {

        distinctQuery = "SELECT * FROM " + CustomAttributeDao.TABLENAME + Constant.SQL_WHERE +
                CustomAttributeDao.Properties.Code.columnName + " = '" + code + "' " +
                "and " + CustomAttributeDao.Properties.ProductId.columnName + " = '" + productId + "' group by " + CustomAttributeDao.Properties.Label.columnName;

        List<CustomAttribute> result = new ArrayList<>();
        Cursor cursor = daoSession.getDatabase().rawQuery(distinctQuery, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            result.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        closeSession();
        return result;
    }


    /**
     * Gets the attribute collection by code.
     *
     * @param code
     */
    public List<CustomAttribute> getAttributeById(String code, String id, int productId) {

        distinctQuery = "SELECT * FROM " + CustomAttributeDao.TABLENAME + Constant.SQL_WHERE +
                CustomAttributeDao.Properties.Code.columnName + " = '" + code + "' and "
                + AttributeDao.Properties.ConfigId.columnName + " in(" + id + ") " +
                "and " + CustomAttributeDao.Properties.ProductId.columnName + " ='" + productId + "' group by " + AttributeDao.Properties.ConfigId.columnName;

        List<CustomAttribute> result = new ArrayList<>();
        Cursor cursor = daoSession.getDatabase().rawQuery(distinctQuery, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {

            result.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        closeSession();
        return result;
    }

    public CustomAttribute cursorToObject(Cursor cursor) {
        CustomAttribute attribute = new CustomAttribute();

        attribute.setLabel(cursor.getString(cursor
                .getColumnIndex(CustomAttributeDao.Properties.Label.columnName)));
        attribute.setCode(cursor.getString(cursor
                .getColumnIndex(CustomAttributeDao.Properties.Code.columnName)));
        attribute.setId(cursor.getLong(cursor
                .getColumnIndex(CustomAttributeDao.Properties.Id.columnName)));
        attribute.setConfigId(cursor.getString(cursor
                .getColumnIndex(CustomAttributeDao.Properties.ConfigId.columnName)));
        attribute.setPrice(cursor.getString(cursor
                .getColumnIndex(CustomAttributeDao.Properties.Price.columnName)));
        attribute.setStockQty(cursor.getString(cursor
                .getColumnIndex(CustomAttributeDao.Properties.StockQty.columnName)));
        attribute.setIsStock(cursor.getString(cursor
                .getColumnIndex(CustomAttributeDao.Properties.IsStock.columnName)));
        return attribute;
    }

    /**
     * Gets the attribute collection by code.
     *
     * @param configIds
     */
    public String getValueByCode(String configIds) {

        String query = "select a0.CONFIG_ID from ";
        String table = "";
        String where = "";
        String values = "";

        String[] separated = configIds.split(",");

        for (int i = 0; i < separated.length; i++) {
            String tableName = "a" + i;
            if (table.isEmpty())
                table = "ATTRIBUTE " + tableName;
            else
                table = table + ", ATTRIBUTE " + tableName;

            if (where.isEmpty() && separated.length > 1)
                where = tableName + ".CONFIG_ID = ";
            else if (!where.isEmpty())
                where = where + tableName + ".CONFIG_ID and ";

            if (values.isEmpty())
                values = tableName + ".ATTR_ID =" + separated[i];
            else
                values = values + " and " + tableName + ".ATTR_ID =" + separated[i];

        }
        distinctQuery = query + table + " where " + where + values;
        List<String> result = new ArrayList<>();
        Cursor cursor = daoSession.getDatabase().rawQuery(distinctQuery, null);
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            String value = cursor.getString(cursor
                    .getColumnIndex(AttributeDao.Properties.ConfigId.columnName));

            result.add(value);
            cursor.moveToNext();
        }
        cursor.close();
        closeSession();
        return TextUtils.join(",", result);
    }

}
