/**
 * CategoryExecutor.java
 * <p/>
 * This is the Helper class that is used to insert and get the category data from database.
 *
 * @category Contus
 * @package com.contus.mcomm.database
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.database;

import android.content.Context;

import com.contus.mcomm.model.Product;
import com.contus.mcomm.model.Wishlistdb;
import com.contussupport.ecommerce.McommApplication;

import java.util.List;

public class WishlistdbExecutor {

    private final DaoSession daoSession;
    private final WishlistdbDao wishlistDao;


    /**
     * This function called at the time of object creation.
     *
     * @param context
     */
    public WishlistdbExecutor(Context context) {
        McommApplication myApplication = (McommApplication) context.getApplicationContext();
        daoSession = myApplication.getDaoSession();
        wishlistDao = daoSession.getWishlistdbDao();
    }

    /**
     * Clear the catgeory table session
     */
    public void closeSession() {
        daoSession.clear();
    }

    /**
     * Insert the wishlist data.
     *
     * @param products product collection
     * @return long
     */
    public long insertData(final List<Product> products) {
        deleteAll();
        daoSession.runInTx(new Runnable() {

            @Override
            public void run() {
                deleteAll();
                int count = products.size();
                for (int i = 0; i < count; i++) {
                    Product message = products.get(i);
                    Wishlistdb wishValue = new Wishlistdb();
                    wishValue.setIsWishList(message.getIsWishlist());
                    wishValue.setProductId(message.getProductId());
                    wishlistDao.insertOrReplace(wishValue);
                }
                closeSession();
            }
        });
        return 1;

    }

    /**
     * To delete all data from product table
     */
    public void deleteAll() {
        wishlistDao.deleteAll();
        closeSession();
    }

    /**
     * Update the wishlist dtabase by product id with wishlist flag.
     *
     * @param productId product Id
     * @param wishlist  Is wishlist
     */
    public void updateWishlist(String productId, final boolean wishlist) {
        final List<Wishlistdb> wishlistlist = wishlistDao.queryBuilder()
                .where(WishlistdbDao.Properties.ProductId
                        .eq(productId)).list();
        if (!wishlistlist.isEmpty()) {
            daoSession.runInTx(new Runnable() {
                @Override
                public void run() {
                    for (Wishlistdb mList : wishlistlist) {
                        mList.setIsWishList(wishlist);
                        wishlistDao.update(mList);
                    }
                    closeSession();
                }
            });

        }
    }

    /**
     * Gets the wishlist collection
     *
     * @return the Wishlistdb
     */
    public List<Wishlistdb> getWishlist() {

        List<Wishlistdb> wishlistdb = wishlistDao.queryBuilder()
                .list();
        closeSession();
        return wishlistdb;
    }


    /**
     * Gets the wishlist by product id
     *
     * @param productId selected product id
     * @return the Wishlistdb
     */
    public boolean getWishlistbyId(int productId) {
        boolean isWishlist = false;

        List<Wishlistdb> wishlistdb = wishlistDao.queryBuilder()
                .where(WishlistdbDao.Properties.ProductId
                        .eq(productId))
                .list();
        if (!wishlistdb.isEmpty()) {
            isWishlist = wishlistdb.get(0).getIsWishList();
        }
        closeSession();
        return isWishlist;
    }
}
