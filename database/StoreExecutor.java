/**
 * CategoryExecutor.java
 * <p/>
 * This is the Helper class that is used to insert and get the category data from database.
 *
 * @category Contus
 * @package com.contus.mcomm.database
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.database;

import android.content.Context;

import com.contus.mcomm.database.StoreDao.Properties;
import com.contus.mcomm.model.Store;
import com.contussupport.ecommerce.McommApplication;

import java.util.List;

public class StoreExecutor {

    private final DaoSession daoSession;
    private final StoreDao storeDao;
    private final Context con;


    /**
     * This function called at the time of object creation.
     *
     * @param context
     */
    public StoreExecutor(Context context) {
        McommApplication myApplication = (McommApplication) context.getApplicationContext();
        this.con=context;
        daoSession = myApplication.getDaoSession();
        storeDao = daoSession.getStoreDao();
    }

    /**
     * Clear the store table session
     */
    public void closeSession() {
        daoSession.clear();
    }

    /**
     * Insert the store data.
     *
     * @param stores
     * @return long
     */
    public long insertData(final List<Store> stores) {

        daoSession.runInTx(new Runnable() {

            @Override
            public void run() {
                int count = stores.size();
                for (int i = 0; i < count; i++) {
                    storeDao.insertOrReplace(stores.get(i));
                }
                closeSession();
                McommApplication.getStoreDetails(con);
            }
        });
        return 1;

    }

    /**
     * Gets the store by id.
     *
     * @param storeId
     */
    public Store geStoreById(String storeId) {

        List<Store> stores = storeDao.queryBuilder()
                .where(Properties.StoreId
                        .like(storeId)).list();
        if (!stores.isEmpty()) {
            return stores.get(0);
        }
        closeSession();
        return null;
    }


}
