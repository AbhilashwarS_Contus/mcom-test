/**
 * ProductExecutor.java
 * <p/>
 * This is the Helper class that is used to insert and get the product data from database.
 *
 * @category Contus
 * @package com.contus.mcomm.database
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.database;

import android.content.Context;

import com.contus.mcomm.database.ProductDao.Properties;
import com.contus.mcomm.model.Product;
import com.contussupport.ecommerce.McommApplication;

import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;

public class ProductExecutor {

    private final DaoSession daoSession;
    private final ProductDao productDao;


    /**
     * This function called at the time of object creation.
     *
     * @param context
     */
    public ProductExecutor(Context context) {
        McommApplication myApplication = (McommApplication) context.getApplicationContext();
        daoSession = myApplication.getDaoSession();
        productDao = daoSession.getProductDao();

    }

    /**
     * Clear the product table session
     */
    private void closeSession() {
        daoSession.clear();
    }

    /**
     * Insert the product data.
     *
     * @param typeId
     * @param product product collection
     * @return long
     */
    public long insertData(final List<Product> product, final int typeId) {
        daoSession.runInTx(new Runnable() {

            @Override
            public void run() {
                int count = product.size();

                for (int i = 0; i < count; i++) {
                    Product message = product.get(i);
                    message.setProductType(typeId);
                    productDao.insertOrReplace(message);
                }
                closeSession();
            }
        });
        return 1;

    }

    /**
     * To delete all data from product table
     */
    public void deleteAll() {
        productDao.deleteAll();
        closeSession();
    }

    /**
     * Gets the product by type id.
     *
     * @param typeId
     * @return the product
     */
    public List<Product> getProductById(int typeId) {

        List<Product> product = productDao.queryBuilder()
                .where(Properties.ProductType
                        .eq(typeId))
                .orderAsc(Properties.ProductId).list();
        closeSession();
        return product;
    }

    public void deleteProductByType(int type) {

        QueryBuilder qb = productDao.queryBuilder();
        qb.where(Properties.ProductType.eq(type));
        List messageReceived = qb.list();
        productDao.deleteInTx(messageReceived);
        closeSession();
    }


    /**
     * Gets the product by type id.
     *
     * @return the product
     */
    public boolean isCollection() {
        boolean value;
        List<Product> productCollection = productDao.queryBuilder()
                .orderAsc(Properties.ProductId).list();
        if (productCollection.isEmpty()) {
            value = false;
        } else {
            value = true;
        }
        closeSession();
        return value;
    }
}
