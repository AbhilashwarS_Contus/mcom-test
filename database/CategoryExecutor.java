/**
 * CategoryExecutor.java
 * <p/>
 * This is the Helper class that is used to insert and get the category data from database.
 *
 * @category Contus
 * @package com.contus.mcomm.database
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.database;

import android.content.Context;

import com.contus.mcomm.database.CategoryDao.Properties;
import com.contus.mcomm.model.Category;
import com.contussupport.ecommerce.McommApplication;

import java.util.List;

public class CategoryExecutor {

    private final DaoSession daoSession;
    private final CategoryDao catDao;


    /**
     * This function called at the time of object creation.
     *
     * @param context
     */
    public CategoryExecutor(Context context) {
        McommApplication myApplication = (McommApplication) context.getApplicationContext();
        daoSession = myApplication.getDaoSession();
        catDao = daoSession.getCategoryDao();
    }

    /**
     * Clear the catgeory table session
     */
    public void closeSession() {
        daoSession.clear();
    }

    /**
     * Insert the catgeory data.
     *
     * @param categories
     * @return long
     */
    public long insertData(final List<Category> categories) {

        daoSession.runInTx(new Runnable() {

            @Override
            public void run() {
                int count = categories.size();
                for (int i = 0; i < count; i++) {
                    catDao.insertOrReplace(categories.get(i));
                }
                closeSession();
            }
        });
        return 1;

    }

    /**
     * Gets the category by id.
     *
     * @param parentId
     */
    public List<Category> getCategoryById(int parentId) {

        List<Category> category = catDao.queryBuilder()
                .where(Properties.ParentId
                        .eq(parentId))
                .orderAsc(Properties.Position).list();
        closeSession();
        return category;
    }


}
