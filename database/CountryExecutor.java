/**
 * CategoryExecutor.java
 * <p/>
 * This is the Helper class that is used to insert and get the category data from database.
 *
 * @category Contus
 * @package com.contus.mcomm.database
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.database;

import android.content.Context;

import com.contus.mcomm.model.Country;
import com.contussupport.ecommerce.McommApplication;

import java.util.List;

public class CountryExecutor {

    private final DaoSession daoSession;
    private final CountryDao countryDao;


    /**
     * This function called at the time of object creation.
     *
     * @param context
     */
    public CountryExecutor(Context context) {
        McommApplication myApplication = (McommApplication) context.getApplicationContext();
        daoSession = myApplication.getDaoSession();
        countryDao = daoSession.getCountryDao();
    }

    /**
     * Clear the country table session
     */
    public void closeSession() {
        daoSession.clear();
    }

    /**
     * Insert the country data.
     *
     * @param country
     * @return long
     */
    public long insertData(final List<Country> country) {

        daoSession.runInTx(new Runnable() {

            @Override
            public void run() {
                int count = country.size();
                for (int i = 0; i < count; i++) {
                    countryDao.insertOrReplace(country.get(i));
                }
                closeSession();
            }
        });
        return 1;

    }

    /**
     * Gets the category by id.
     */
    public List<Country> getCountry() {

        List<Country> country = countryDao.queryBuilder()
                .orderAsc().list();
        closeSession();
        return country;
    }


}
