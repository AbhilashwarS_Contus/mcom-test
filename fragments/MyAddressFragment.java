/**
 * MyAddressFragment.java
 * <p/>
 * This is a view class of Home Page.
 *
 * @category Contus
 * @package contus.com.mcomm
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.adapters.MyAccountAdapter;
import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.model.Address;
import com.contus.mcomm.model.AddressResponse;
import com.contus.mcomm.model.DeleteAddress;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.CustomDialog;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MyAddressFragment extends Fragment implements View.OnClickListener, CustomDialog.OnDialogSelectedListener {
    private static final String TAG = LogUtils.makeLogTag(MyAddressFragment.class);
    boolean isDelete = false;
    private Context mContext;
    private View rootView;
    private ListView myAddrListView;
    private Dialog pDialog;
    private Progress progress;
    private McommApplication application;
    private Button btDelNewAddr;
    private MyAccountAdapter accountAdapter;
    private List<Address> userResult;
    private int posAddr;
    private Address addressCollection;
    private TextView emptyView;
    private LinearLayout header;
    private CustomDialog mDialog;
    private OnAddressListener mListener;

    public static MyAddressFragment newInstance() {
        return new MyAddressFragment();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_address, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = this.getActivity();
        progress = new Progress(mContext);
        initViews();
        application = new McommApplication();
    }

    /**
     * Request for get the addresses in a list
     */

    private void serverRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            Map<String, String> addressParams = new HashMap<>();
            addressParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            addressParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            addressParams.put(Constant.Request.ACTION, Constant.Request.ADDRESS_ACTION);
            addressParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            addressParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            new RestClient(mContext).getInstance().get().getAddress(addressParams, new RestCallback<AddressResponse>());
        }

    }

    /**
     * Request for delete the address in a list
     */
    private void addressDeleteRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            String addressId = addressCollection.getAddressId();
            new RestClient(mContext).getInstance().get().deleteAddress(
                    McommApplication.getStoreId(mContext),
                    Constant.WEBSITE_ID_VALUE,
                    application.getCustomerId(mContext),
                    application.getToken(mContext),
                    addressId,
                    Constant.Request.DELETE_ADDRESS_ACTION,
                    new RestCallback<DeleteAddress>());
        }
    }

    /**
     * initialize all views
     */
    private void initViews() {
        rootView = getView();
        myAddrListView = (ListView) rootView.findViewById(R.id.listview);
        emptyView = (TextView) rootView.findViewById(R.id.emptyView);
        btDelNewAddr = (Button) rootView.findViewById(R.id.btn_del_new_addr);
        header = (LinearLayout) rootView.findViewById(R.id.header_myaddr);
        header.setVisibility(View.GONE);
        btDelNewAddr.setOnClickListener(this);
        serverRequest();

    }


    /**
     * method to perform on click event
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_del_new_addr:
                if (mListener != null) {
                    mListener.onAddressInteraction(addressCollection, Constant.ADD_ADDRESS);
                }
                break;
            case R.id.ic_delete:
                getAddr(v);
                delAddrDialog();
                break;
            case R.id.ic_edit:
                getAddr(v);
                editAddrDetails();
                break;
            default:
                break;
        }
    }

    /**
     * Get the postion of the view.
     *
     * @param view the view
     * @return void
     */
    public void getAddr(View view) {
        posAddr = Integer.parseInt(view.getTag().toString());
        addressCollection = userResult.get(posAddr);
    }


    @Subscribe
    public void dataReceived(String errorMessage) {
        LogUtils.i(TAG, errorMessage);
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        if (errorMessage.equalsIgnoreCase(Constant.UNAUTHORIZED_VALUE)) {
            McommApplication.redirectAuthentication(mContext);
        } else {
            Utils.showToast(mContext, errorMessage, Toast.LENGTH_SHORT);
        }
    }

    @Subscribe
    public void dataReceived(DeleteAddress result) {
        BusProvider.getInstance().unregister(this);
        dismissProgress();
        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {
            Utils.showToast(mContext,
                    result.getMessage(), Toast.LENGTH_SHORT);
            if (McommApplication.isSuccess(result.getSuccess()))
                setValueAdapter();
        } else {
            isDelete = false;
            LogUtils.i(TAG, result.getMessage());
        }
    }


    @Subscribe
    public void dataReceived(AddressResponse result) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {
            if (McommApplication.isSuccess(result.getSuccess()))
                userResult = result.address;
            setValueAdapter();
        }
        LogUtils.i(TAG, result.getMessage());

    }

    /**
     * method to set the value to the adapter
     */
    private void setValueAdapter() {

        if (isDelete) {
            setListView();
            userResult.remove(posAddr);
            accountAdapter.notifyDataSetChanged();
        } else if (!userResult.isEmpty()) {
            setListView();
            boolean selection = application.getSelection(mContext);
            accountAdapter = new MyAccountAdapter(mContext, userResult, MyAccountAdapter.LIST_TYPE.ADDRESS);
            accountAdapter.setSelection(selection);
            myAddrListView.setAdapter(accountAdapter);
            if (selection) {
                myAddrListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {

                        getAddrPayment(userResult.get(position));
                    }
                });

            }
            accountAdapter.setOnClickAccountListener(this);
        }

        if (userResult.isEmpty()) {
            setEmptyView();
        }
    }

    /**
     * method to show list view when the list has value
     */
    private void setListView() {
        myAddrListView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
    }

    /**
     * method to show empty view when the list is empty
     */
    private void setEmptyView() {
        myAddrListView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        emptyView.setText(getResources().getString(R.string.no_results_addr));
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnAddressListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnAddressListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * method to edit the address detail
     */
    private void editAddrDetails() {

        if (mListener != null) {
            mListener.onAddressInteraction(addressCollection, Constant.UPDATE_ADDRESS);

        }

    }

    /**
     * method to dismiss the progress
     */
    private void dismissProgress() {
        progress.dismissProgress(pDialog);
    }

    /**
     * method to show dialog to delete the address
     */
    private void delAddrDialog() {
        Bundle args = new Bundle();
        args.putInt(Constant.DIALOG_CART_PARAM, Constant.DIALOG_ADDRESS_DELETE);
        mDialog = new CustomDialog();
        mDialog.setArguments(args);
        mDialog.setTargetFragment(this, 0);
        mDialog.show(getFragmentManager(), "dialog");
    }

    @Override
    public void onYesClick(int type) {
        if (type == Constant.DIALOG_ADDRESS_DELETE) {
            isDelete = true;
            addressDeleteRequest();
            mDialog.dismiss();
        }
    }

    private void getAddrPayment(Address addr) {
        if (mListener != null) {
            mListener.onAddressPaymentInteraction(addr);
        }
    }

    @Override
    public void onNoClick(int type) {
        mDialog.dismiss();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnAddressListener {
        void onAddressInteraction(Address address, String type);

        void onAddressPaymentInteraction(Address addr);
    }
}



