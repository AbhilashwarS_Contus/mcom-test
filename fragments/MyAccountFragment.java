package com.contus.mcomm.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.contus.mcomm.model.Login;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contussupport.ecommerce.Home;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.NavigationActivity;
import com.contussupport.ecommerce.R;


/**
 * Created by contus on 6/5/15.
 */
public class MyAccountFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = LogUtils.makeLogTag(MyAccountFragment.class);
    private LinearLayout viewMyProfile, viewWishList, viewAddress, viewLogOut, viewOrders;
    private TextView tvUserName;
    private View rootView;
    private Context mContext;
    private McommApplication application;

    private OnMyAccountInteractionListener mListener;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_account, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        application = new McommApplication();

    }


    private void initViews() {
        mContext = getActivity();
        rootView = getView();
        tvUserName = (TextView) rootView.findViewById(R.id.txt_user_name);
        viewMyProfile = (LinearLayout) rootView.findViewById(R.id.view_myprofile);
        viewAddress = (LinearLayout) rootView.findViewById(R.id.view_myaddress);
        viewWishList = (LinearLayout) rootView.findViewById(R.id.view_mywishlist);
        viewOrders = (LinearLayout) rootView.findViewById(R.id.view_myorders);
        viewLogOut = (LinearLayout) rootView.findViewById(R.id.view_logout);
        tvUserName.setText(application.getLoginDetails(mContext).getFirstName());
        viewMyProfile.setOnClickListener(this);
        viewOrders.setOnClickListener(this);
        viewWishList.setOnClickListener(this);
        viewAddress.setOnClickListener(this);
        viewLogOut.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        try {

            switch (v.getId()) {
                case R.id.view_myaddress:
                    Intent myAddrFrag = new Intent(mContext, NavigationActivity.class);
                    myAddrFrag.putExtra(Constant.NAVIGATION_TYPE, Constant.TYPE_MY_ADDRESS);
                    startActivity(myAddrFrag);
                    break;
                case R.id.view_myorders:
                    Intent myOrderFrag = new Intent(mContext, NavigationActivity.class);
                    myOrderFrag.putExtra(Constant.NAVIGATION_TYPE, Constant.TYPE_MY_ORDER);
                    startActivity(myOrderFrag);
                    break;
                case R.id.view_myprofile:
                    onMyAccount(Constant.TYPE_MY_PROFILE);
                    break;
                case R.id.view_mywishlist:
                    onMyAccount(Constant.TYPE_MY_WISHLIST);
                    break;
                case R.id.view_logout:
                    ((Home) getActivity()).updateDrawer();
                    McommApplication.userLogout(getActivity());
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            LogUtils.i(TAG, e.getMessage());
        }
    }

    public void onMyAccount(int type) {
        if (mListener != null) {
            mListener.onMyAccountFragmentInteraction(type);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnMyAccountInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnMyAccountInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void updateMyAccount() {
        Login userResult = McommApplication.getLoginDetails(mContext);
        tvUserName.setText(userResult.getFirstName());
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnMyAccountInteractionListener {
        public void onMyAccountFragmentInteraction(int type);
    }
}