/**
 * LoginFragment.java
 * <p/>
 * This is the fragment view for login.
 *
 * @category Contus
 * @package com.contus.mcomm.fragments
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.model.ForgotResponse;
import com.contus.mcomm.model.Login;
import com.contus.mcomm.model.LoginResponse;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.GooglePlusHelper;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.SpaceAdjust;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.CustomButton;
import com.contus.mcomm.views.CustomTextView;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.Authentication;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.squareup.otto.Subscribe;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class LoginFragment extends Fragment implements View.OnClickListener, ConnectionCallbacks, OnConnectionFailedListener {
    private static final String TAG = LogUtils.makeLogTag(LoginFragment.class);
    private LoginButton facebookLogin;
    private UiLifecycleHelper uiHelper;
    private Button googlePlusLogin, btnSignIn;
    private Authentication authentication;
    private String lastName, dateOfBirth, strEmailId, strPassword, firstName;
    private CustomTextView dontHaveAccountRegister, forgotPwd;
    private EditText edtEmailAddress, edtPassword;
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state,
                         Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };
    private Dialog pDialog;
    private Progress progress;
    private Context mContext;
    private EditText passwordEmail;
    private Button btSubmit, btCancel;
    private String emailText, strLoginAction = Constant.Request.LOGIN_ACTION;
    private Dialog dialog;
    private boolean serverRequest = false;

    /**
     * Returns a new instance of this fragment.
     */
    public static LoginFragment newInstance() {
        return new LoginFragment();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = getActivity();
        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);
        progress = new Progress(mContext);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        initLoginElements(view);
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        super.onResume();

        try {

            // For scenarios where the main activity is launched and user
            // session is not null, the session state change notification
            // may not be triggered. Trigger it if it's open/closed.
            Session session = Session.getActiveSession();
            if (session != null && (session.isOpened() || session.isClosed())) {
                onSessionStateChange(session, session.getState(), null);
            }

            uiHelper.onResume();
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }
    }

    private void onSessionStateChange(Session session, SessionState state,
                                      Exception exception) {

        if (state.isOpened()) {
            strLoginAction = Constant.Request.LOGIN_ACTION_SOCIAL;
            showLoggedInUserInfo(session);

        } else if (state.isClosed()) {
            // Logged out
        }

    }


    private void initLoginElements(View view) {
        facebookLogin = (LoginButton) view.findViewById(R.id.facebook_login);
        Typeface font = Typeface.createFromAsset(getResources()
                .getAssets(), getString(R.string.font_regular));
        facebookLogin.setTypeface(font);
        googlePlusLogin = (CustomButton) view.findViewById(R.id.googleplus_login);
        dontHaveAccountRegister = (CustomTextView) view.findViewById(R.id.dontHaveAccount);
        forgotPwd = (CustomTextView) view.findViewById(R.id.forgotPassword);
        forgotPwd.setOnClickListener(this);
        edtEmailAddress = (EditText) view.findViewById(R.id.emailAddresses);
        edtPassword = (EditText) view.findViewById(R.id.passwords);
        GooglePlusHelper.con = getActivity();
        GooglePlusHelper.mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
        GooglePlusHelper.mGoogleApiClient.connect();
        authentication = new Authentication();
        String text = getString(R.string.dont_have_an_account);
        SpannableString spannableString = new SpannableString(text);
        spannableString.setSpan(new ForegroundColorSpan(getResources()
                .getColor(R.color.blue)), 23, 32, 0);
        ClickableSpan clickableSpan = new SpaceAdjust(text) {
            @Override
            public void onClick(View textView) {
                ((Authentication) getActivity()).setCurrentItem(1);
            }
        };
        spannableString.setSpan(clickableSpan, 23, 32,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        dontHaveAccountRegister.setText(spannableString);
        dontHaveAccountRegister.setMovementMethod(LinkMovementMethod
                .getInstance());
        btnSignIn = (Button) view.findViewById(R.id.signInButton);
        btnSignIn.setOnClickListener(this);
        facebookLogin.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        facebookLogin.setBackgroundResource(R.drawable.btn_facebook);
        facebookLogin.setFragment(this);
        facebookLogin.setReadPermissions(Arrays.asList("email", "user_birthday"));
        googlePlusLogin.setOnClickListener(this);
    }

    public void onStop() {
        super.onStop();
        if (GooglePlusHelper.mGoogleApiClient.isConnected()) {
            GooglePlusHelper.mGoogleApiClient.disconnect();
        }
    }

    public void showLoggedInUserInfo(final Session session) {

        // Make an API call to get user data and define a
        // new callback to handle the response.
        Request request = Request.newMeRequest(session,
                new Request.GraphUserCallback() {

                    @Override
                    public void onCompleted(GraphUser user, Response response) {
                        // If the response is successful
                        try {
                            if (session == Session.getActiveSession()) if (user != null) {
                                firstName = user.getFirstName();
                                lastName = user.getLastName();
                                dateOfBirth = user.getBirthday();
                                strEmailId = user.asMap().get("email").toString();
                                Session.getActiveSession()
                                        .closeAndClearTokenInformation();
                                if (!serverRequest)
                                    serverRequest();
                            }
                        } catch (Exception e) {
                            Log.e(Constant.TAG_EXCEPTION, e.toString());
                        }

                    }

                });

        request.executeAsync();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.googleplus_login:
                strLoginAction = Constant.Request.LOGIN_ACTION_SOCIAL;
                signInWithGplus();
                break;
            case R.id.signInButton:
                strLoginAction = Constant.Request.LOGIN_ACTION;
                userLogin();
                break;
            case R.id.forgotPassword:
                dialogForgotPwd();
                break;
            case R.id.bt_submit:
                if (validation()) {
                    forgotRequest();
                }
                break;
            case R.id.bt_cancel:
                dialog.dismiss();
                break;
            default:
                break;
        }
    }

    private void dialogForgotPwd() {

        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_forgot_pwd);
        dialog.setCancelable(true);
        passwordEmail = (EditText) dialog
                .findViewById(R.id.email_addr);
        btSubmit = (Button) dialog.findViewById(R.id.bt_submit);
        btCancel = (Button) dialog.findViewById(R.id.bt_cancel);
        btSubmit.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        dialog.show();

    }


    private boolean validation() {
        boolean success = false;
        emailText = passwordEmail.getText().toString();
        if (TextUtils.isEmpty(emailText)) {
            passwordEmail.setError(getString(R.string.error_emailSignIn));
            passwordEmail.requestFocus();
        } else if (!validateEmail(emailText)) {
            passwordEmail.setError(getString(R.string.error_emailIncorrect));
            passwordEmail.requestFocus();
        } else {
            success = true;
        }
        return success;
    }

    /**
     * method to validate the email id
     */
    private boolean validateEmail(String emails) {
        if (Utils.validateEmail(emails)) {
            return true;
        } else {
            passwordEmail.setError(getString(R.string.error_emailIncorrect));
            passwordEmail.requestFocus();
            return false;
        }
    }

    /**
     * Sign-in the user
     */
    private void userLogin() {
        strEmailId = Utils.getString(edtEmailAddress);
        strPassword = Utils.getString(edtPassword);
        if (validate()) {
            serverRequest();
        }
    }

    /**
     * Sign-in into google
     */
    private void signInWithGplus() {
        if (!GooglePlusHelper.mGoogleApiClient.isConnecting()) {
            GooglePlusHelper.mSignInClicked = true;
            authentication.resolveSignInError();
        }
    }


    @Override
    public void onConnected(Bundle bundle) {
        GooglePlusHelper.mSignInClicked = false;
        // Get user's information
        getProfileInformation();

        // Update the UI after signin
        updateUI(true);

    }

    @Override
    public void onConnectionSuspended(int i) {
        GooglePlusHelper.mGoogleApiClient.connect();
        updateUI(false);
    }

    private void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(GooglePlusHelper.mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(GooglePlusHelper.mGoogleApiClient);
                firstName = currentPerson.getName().getGivenName();
                lastName = currentPerson.getName().getFamilyName();

                dateOfBirth = currentPerson.getBirthday();

                strEmailId = Plus.AccountApi
                        .getAccountName(GooglePlusHelper.mGoogleApiClient);

                Plus.AccountApi.clearDefaultAccount(GooglePlusHelper.mGoogleApiClient);
                GooglePlusHelper.mGoogleApiClient.disconnect();
                if (strLoginAction.equalsIgnoreCase(Constant.Request.LOGIN_ACTION_SOCIAL))
                    serverRequest();
            } else {
                Toast.makeText(getActivity(), "Personal Information not available", Toast.LENGTH_SHORT)
                        .show();
            }
        } catch (Exception e) {
            Log.e(Constant.TAG_EXCEPTION, e.toString());
        }
    }


    /**
     * Updating the UI, showing/hiding buttons and profile layout
     */
    private void updateUI(boolean isSignedIn) {
        if (isSignedIn) {
            googlePlusLogin.setVisibility(View.VISIBLE);
        } else {
            googlePlusLogin.setVisibility(View.VISIBLE);

        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (!connectionResult.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(),
                    getActivity(), 0).show();
            return;
        }

        if (!GooglePlusHelper.mIntentInProgress) {
            // Store the ConnectionResult for later usage
            GooglePlusHelper.mConnectionResult = connectionResult;
        }

    }

    private boolean validate() {
        boolean success = false;
        if (TextUtils.isEmpty(strEmailId)) {
            edtEmailAddress.setError(getString(R.string.emailSignIn_error));
            edtEmailAddress.requestFocus();
        } else if (!Utils.validateEmail(strEmailId)) {
            edtEmailAddress.setError(getString(R.string.email_incorrect));
            edtEmailAddress.requestFocus();
        } else if (TextUtils.isEmpty(strPassword)) {
            edtPassword.setError(getString(R.string.passwordsignin_error));
            edtPassword.requestFocus();
        } else if (!TextUtils.isEmpty(strPassword)) {

            if (strPassword.length() < Constant.MINIMUM_PASSWORD_LENTH || strPassword.length() > Constant.MAXIMUM_PASSWORD_LENTH) {
                edtPassword.setError(getString(R.string.alert_passwordMandatory));
            } else {
                success = true;
            }
        }
        return success;
    }

    private void dismissProgress() {
        progress.dismissProgress(pDialog);
    }


    /**
     * Request for product list with specified category id
     */
    private void serverRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            serverRequest = true;
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            Map<String, String> loginParams = new HashMap<>();

            loginParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            loginParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            loginParams.put(Constant.Request.ACTION, strLoginAction);
            loginParams.put(Constant.Request.EMAIL, strEmailId);
            if (strLoginAction.equalsIgnoreCase(Constant.Request.LOGIN_ACTION_SOCIAL)) {
                loginParams.put(Constant.Request.GROUP_ID, Constant.Request.GROUP_ID_VALUE);
                loginParams.put(Constant.Request.FIRST_NAME, firstName);
                loginParams.put(Constant.Request.LAST_NAME, lastName);
                loginParams.put(Constant.Request.NEWSLETTER, "1");
                if (!TextUtils.isEmpty(dateOfBirth))
                    loginParams.put(Constant.Request.DATE_OF_BIRTH, dateOfBirth);
            } else {
                loginParams.put(Constant.Request.SIGNIN_SIGNATURE, strPassword);
            }
            new RestClient(mContext).getInstance().get().getProfile(loginParams, new RestCallback<LoginResponse>());
        }
    }

    @Subscribe
    public void dataReceived(String errorMessage) {
        serverRequest = false;
        strLoginAction = Constant.Request.LOGIN_ACTION;
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        Utils.showToast(mContext, errorMessage, Toast.LENGTH_SHORT);
        LogUtils.i(TAG, errorMessage);

    }

    @Subscribe
    public void dataReceived(LoginResponse result) {
        dismissProgress();
        serverRequest = false;
        strLoginAction = Constant.Request.LOGIN_ACTION;
        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {

            if (McommApplication.isSuccess(result.getSuccess())) {
                Login userResult = result.login;
                McommApplication.userLogin(getActivity(), userResult);
                Utils.showToast(mContext, "Welcome " + userResult.getFirstName(), Toast.LENGTH_SHORT);
                getActivity().finish();
            } else {
                Utils.showToast(mContext, result.getMessage(), Toast.LENGTH_SHORT);
            }

        } else {
            LogUtils.i(TAG, result.getMessage());
        }
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void dataReceived(ForgotResponse result) {
        dismissProgress();
        dialog.dismiss();
        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {
            LogUtils.i(TAG, result.getMessage());
        } else {
            LogUtils.i(TAG, result.getMessage());
        }
        BusProvider.getInstance().unregister(this);
    }


    private void forgotRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            Map<String, String> forgotParams = new HashMap<>();
            forgotParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            forgotParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            forgotParams.put(Constant.Request.ACTION, Constant.Request.FORGOT_ACTION);
            forgotParams.put(Constant.Request.EMAIL, emailText);
            new RestClient(mContext).getInstance().get().forgotPassword(forgotParams, new RestCallback<ForgotResponse>());

        }

    }
}