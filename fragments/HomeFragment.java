/**
 * FragmentHome.java
 * <p/>
 * This is a view class of Home Page.
 *
 * @category Contus
 * @package contus.com.mcomm
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.contus.mcomm.adapters.CategoryAdapter;
import com.contus.mcomm.adapters.ProductListAdapter;
import com.contus.mcomm.adapters.ViewpagerAdapter;
import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.database.CategoryExecutor;
import com.contus.mcomm.database.ProductExecutor;
import com.contus.mcomm.externallibraries.CirclePageIndicator;
import com.contus.mcomm.externallibraries.HorizontalListView;
import com.contus.mcomm.model.Category;
import com.contus.mcomm.model.Product;
import com.contus.mcomm.model.Response;
import com.contus.mcomm.service.DbParser;
import com.contus.mcomm.service.OnParsingCompletionListener;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.CustomGridView;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class HomeFragment extends Fragment implements AdapterView.OnItemClickListener, OnParsingCompletionListener {
    
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String ARG_REQUEST = "request";
    protected Timer swipeTimer;
    private ViewPager offersPager;
    private CirclePageIndicator circleIndicator;
    private int currentPage = 0;
    ViewPager.OnPageChangeListener pagechangelistener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int arg0) {
            currentPage = arg0;
            offersAdapter.notifyDataSetChanged();
            circleIndicator.setCurrentItem(arg0);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            //onPageScrolled
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
            //onPageScrollStateChanged
        }
    };
    private int numPages = 3;
    private ViewpagerAdapter offersAdapter;
    private CustomGridView categoryTypes;
    private CategoryAdapter categoryAdapter;
    private HorizontalListView newArrivalsList;
    private ProductListAdapter newarrivalsAdapter;
    private List<Category> categoryList;
    private List<Product> productList;
    private Activity mContext;
    private OnHomeInteractionListener mListener;
    private boolean serverRequest;

    public HomeFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static HomeFragment newInstance(int sectionNumber, boolean request) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        args.putBoolean(ARG_REQUEST, request);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.serverRequest = getArguments().getBoolean(ARG_REQUEST);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        offersPager = (ViewPager) rootView.findViewById(R.id.offer_pager);
        circleIndicator = (CirclePageIndicator) rootView.findViewById(R.id.circle_pager);
        newArrivalsList = (HorizontalListView) rootView.findViewById(R.id.new_arrivals);
        categoryTypes = (CustomGridView) rootView.findViewById(R.id.category_view);
        categoryTypes.setExpanded(true);
        this.mContext = getActivity();
        serverRequest();
        return rootView;
    }


    @Subscribe
    public void dataReceived(String error) {
        Toast.makeText(mContext, error, Toast.LENGTH_LONG).show();
        BusProvider.getInstance().unregister(this);
    }

    @Subscribe
    public void dataReceived(Response result) {

        DbParser parser;
        try {
            parser = new DbParser(mContext, result, DbParser.PARSE_TYPE.HOME);
            parser.setOnParsingCompletionListener(this);
            parser.start();

        } catch (Exception e) {
            LogUtils.i(Constant.TAG_EXCEPTION, e.getMessage());
        }

        BusProvider.getInstance().unregister(this);
    }

    @Override
    public void onParsingCompletion() {
        setBanner();
        setCategory();
        setNewArrival();
    }

    @Override
    public void onParsingCompletionError(String error) {
        LogUtils.i(Constant.TAG_EXCEPTION, String.valueOf(error));
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnHomeInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    private void serverRequest() {
        setBanner();
        setCategory();
        setNewArrival();
        timerTask();
        if (Utils.isNetConnected(mContext) && serverRequest) {
            BusProvider.getInstance().register(this);
            new RestClient(mContext)
                    .getInstance()
                    .get()
                    .getHomePage(McommApplication.getStoreId(mContext), Constant.WEBSITE_ID_VALUE, Constant.Request.HOME_ACTION,
                            new RestCallback<Response>());
        }
    }

    /**
     * Set the banner data and display the specified position in the view.
     */

    private void setBanner() {
        productList = new ProductExecutor(mContext).getProductById(Constant.OFFER_PRODUCT);
        offersAdapter = new ViewpagerAdapter(mContext, productList, mListener, R.id.offer_pager);
        offersPager.setAdapter(offersAdapter);
        circleIndicator.setViewPager(offersPager);
    }

    /**
     * Set the category data and display the specified position in the view.
     */

    private void setCategory() {
        categoryList = new CategoryExecutor(mContext).getCategoryById(Constant.DEFAULT_PARENTID);
        categoryAdapter = new CategoryAdapter(mContext, categoryList, CategoryAdapter.LIST_TYPE.MAIN_CATEGORY);
        categoryTypes.setAdapter(categoryAdapter);
        categoryTypes.setOnItemClickListener(this);

    }

    /**
     * Set the New Arrival data and display the specified position in the view.
     */

    private void setNewArrival() {
        productList = new ProductExecutor(mContext).getProductById(Constant.NEW_PRODUCT);
        newarrivalsAdapter = new ProductListAdapter(mContext, productList, ProductListAdapter.LIST_TYPE.NEW_PRODUCT);
        newArrivalsList.setAdapter(newarrivalsAdapter);
        newArrivalsList.setOnItemClickListener(this);
    }

    private void timerTask() {
        swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    if (mContext != null) {
                        mContext.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setCurrentItem();
                            }
                        });
                    }
                } catch (Exception e) {
                    LogUtils.i(Constant.TAG_EXCEPTION, e.getMessage());
                }
            }
        }, 500, 3000);


        if (offersPager != null) {
            offersPager.setOnPageChangeListener(pagechangelistener);
        }
    }

    private void setCurrentItem() {
        if (currentPage == numPages) {
            currentPage = 0;
        }
        if (currentPage >= offersAdapter.getCount()) {
            currentPage = 0;
        }
        offersPager.setCurrentItem(currentPage++);
    }

    @Override
    public void onDestroy() {
        if (swipeTimer != null)
            swipeTimer.purge();
        super.onDestroy();
    }

    public void onItemSelected(int id, Object selectedCate) {
        if (mListener != null && McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            mListener.onHomeInteraction(id, selectedCate);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int itemId = parent.getId();
        switch (itemId) {
            case R.id.category_view:
                onItemSelected(itemId, categoryList.get(position));
                break;
            case R.id.new_arrivals:
                onItemSelected(itemId, productList.get(position));
                break;
            default:
                break;
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnHomeInteractionListener {
        public void onHomeInteraction(int id, Object selectedCate);

    }
}
