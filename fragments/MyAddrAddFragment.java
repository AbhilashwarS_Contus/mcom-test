/**
 * MyAddrAddNewFragment.java
 * <p/>
 * This is a view class of Home Page.
 *
 * @category Contus
 * @package contus.com.mcomm
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.contus.mcomm.adapters.MyAccountAdapter;
import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.database.CountryExecutor;
import com.contus.mcomm.model.Address;
import com.contus.mcomm.model.AddressResponse;
import com.contus.mcomm.model.Country;
import com.contus.mcomm.model.CountryResponse;
import com.contus.mcomm.model.RegionResponse;
import com.contus.mcomm.model.Regions;
import com.contus.mcomm.service.BackgroundTaskCallback;
import com.contus.mcomm.service.OnTaskCompletionListener;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MyAddrAddFragment extends Fragment implements View.OnClickListener, OnTaskCompletionListener {
    private static final String TAG = LogUtils.makeLogTag(MyAddrAddFragment.class);
    Context mContext;
    private Dialog pDialog, countryDialog, regionDialog;
    private Progress progress;
    private McommApplication application;
    private EditText etFirstName, etLastName, etStreetAddr, etCity, etCountry,
            etPostCode, etPhone, etRegionSp, etRegionEdit;
    private Button btSaveAddr;
    private String gtFname, gtLname, gtStreet, gtCountry, gtRegionEt, gtCity, gtPcode, gtPhone, gtCountryId,
            gtRegionCode = null, gtRegionValue;
    private String stFname, stLname, stStreet, stCountry, stCountryId, stRegion = null, stCity, stPcode, stPhone, addrId;
    private String[] stArrStreet;
    private String addrAction;
    private List<Country> coutryList;
    private List<Regions> regions;
    private CountryExecutor countryExecutor;
    private MyAccountAdapter countryAdapter, regionAdapter;
    private String gtRegionSp;
    private String stRegionCode;
    private Address mAddressCollection;
    private ListView lvCountry, lvRegion;
    private OnAddAddressListener mListener;
    private BackgroundTaskCallback backgroundTaskCallback;
    private boolean success;

    public static MyAddrAddFragment newInstance(Address collection, String type) {
        MyAddrAddFragment fragment = new MyAddrAddFragment();
        Bundle args = new Bundle();
        args.putString(Constant.Request.ADDRESS_ACTION, type);
        fragment.setArguments(args);
        fragment.setAddressInfo(collection);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = getActivity();
        progress = new Progress(mContext);
        application = new McommApplication();
        if (getArguments() != null)
            addrAction = (getArguments().getString(Constant.Request.ADDRESS_ACTION));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_addr_addnew, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initLoginElements(view);
        application = new McommApplication();
    }

    public void setAddressInfo(Address collection) {
        this.mAddressCollection = collection;
    }

    /**
     * method to initialise all view objects
     */
    private void initLoginElements(View rootView) {

        etFirstName = (EditText) rootView.findViewById(R.id.fname);
        etLastName = (EditText) rootView.findViewById(R.id.lname);
        etStreetAddr = (EditText) rootView.findViewById(R.id.address);
        etCity = (EditText) rootView.findViewById(R.id.city);
        etCountry = (EditText) rootView.findViewById(R.id.country);
        etRegionSp = (EditText) rootView.findViewById(R.id.region_spinner);
        etRegionEdit = (EditText) rootView.findViewById(R.id.region_et);
        etPostCode = (EditText) rootView.findViewById(R.id.zipcode);
        etPhone = (EditText) rootView.findViewById(R.id.contactnum);
        btSaveAddr = (Button) rootView.findViewById(R.id.btn_save_and_cont);
        btSaveAddr.setOnClickListener(this);
        etCountry.setOnClickListener(this);
        etRegionSp.setOnClickListener(this);
        checkUpdateOrAdd();
    }

    /**
     * method to check whether to update or add the address
     */
    private void checkUpdateOrAdd() {
        /**
         * if update condition is true then retrieves all arguments
         */
        if ((Constant.UPDATE_ADDRESS).equals(addrAction)) {
            addrId = mAddressCollection.getAddressId();
            stFname = mAddressCollection.getFirstName();
            stLname = mAddressCollection.getLastName();
            stArrStreet = mAddressCollection.getStreet();
            for (String val : stArrStreet) {
                stStreet = val;
            }
            stCountry = mAddressCollection.getCountryName();
            stCountryId = mAddressCollection.getCountryId();
            stRegion = mAddressCollection.getRegion();
            stRegionCode = mAddressCollection.getRegionCode();
            stCity = mAddressCollection.getCity();
            stPcode = mAddressCollection.getPostcode();
            stPhone = mAddressCollection.getTelephone();

            setViewObjects();
        }
        /**
         * else if add condition is true then asigns the addrId as null
         */
        else if ((Constant.ADD_ADDRESS).equals(addrAction)) {
            addrId = null;
            stRegion = null;
        }
    }

    /**
     * method to set all the arguments to the edit text,
     * it works in the edit function alone
     */
    private void setViewObjects() {
        etFirstName.setText(stFname);
        etLastName.setText(stLname);
        etStreetAddr.setText(stStreet);
        etCity.setText(stCity);
        etCountry.setText(stCountry);
        etPostCode.setText(stPcode);
        etPhone.setText(stPhone);

        if (TextUtils.isEmpty(mAddressCollection.getRegionId())) {
            etRegionEdit.setText(stRegion);
        } else {
            stateRequest(stCountryId);
            etRegionSp.setText(stRegion);
        }
        gtCountryId = stCountryId;
        gtRegionCode = stRegionCode;

    }

    /**
     * method to perform the click event
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.btn_save_and_cont:
                getViewObjects();
                break;
            case R.id.country:
                stRegion = "";
                showCountryDialog();
                break;
            case R.id.region_spinner:
                etRegionSp.setError(null);
                showDialogRegion();
                break;
            default:
                break;
        }

    }

    /**
     * method to show the dialog which lists all the countries, retrieved from server
     */
    private void showCountryDialog() {
        countryDialog = new Dialog(mContext);
        countryDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        countryDialog.setContentView(R.layout.listview);
        lvCountry = (ListView) countryDialog.findViewById(R.id.listview);
        countryExecutor = new CountryExecutor(mContext);
        coutryList = countryExecutor.getCountry();
        /**
         * check whether the countrylist is empty, if empty then retrieves
         * the country list by requesting from server
         */
        if (coutryList.isEmpty()) {
            countryRequest();
        } else {
            setCountryAdapter(coutryList);
        }
    }

    /**
     * method to show the dialog which lists all the regions, retrieved from server using the country id
     */
    private void showDialogRegion() {
        regionDialog = new Dialog(mContext);
        regionDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        regionDialog.setContentView(R.layout.listview);
        lvRegion = (ListView) regionDialog.findViewById(R.id.listview);
        lvRegion.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        regionAdapter = new MyAccountAdapter(mContext, regions, MyAccountAdapter.LIST_TYPE.REGION);
        lvRegion.setAdapter(regionAdapter);
        lvRegion.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {
                gtRegionCode = regions.get(pos).getCode();
                etRegionSp.setText(regions.get(pos).getName());
                regionAdapter.notifyDataSetChanged();
                regionDialog.dismiss();
            }
        });
        regionDialog.show();
    }

    /**
     * method to get the values from view objects
     */
    private void getViewObjects() {
        gtFname = etFirstName.getText().toString();
        gtLname = etLastName.getText().toString();
        gtStreet = etStreetAddr.getText().toString();
        gtCity = etCity.getText().toString();
        gtCountry = etCountry.getText().toString();
        gtPcode = etPostCode.getText().toString();
        gtPhone = etPhone.getText().toString();
        gtRegionEt = etRegionEdit.getText().toString();
        gtRegionSp = etRegionSp.getText().toString();

        if (TextUtils.isEmpty(gtRegionCode)) {
            gtRegionValue = gtRegionEt;
        } else {
            gtRegionValue = gtRegionCode;
        }

        if (validate() && addrId != null) {
            updateAddressRequest();
        } else if (validate()) {
            addAddressRequest();
        }
    }

    /**
     * method to perform validation for all edit texts
     */
    private boolean validate() {
        success = false;
        if (TextUtils.isEmpty(gtFname)) {
            etFirstName.setError(getString(R.string.firstName_error));
            etFirstName.requestFocus();
        } else if (TextUtils.isEmpty(gtLname)) {
            etLastName.setError(getString(R.string.lastName_error));
            etLastName.requestFocus();
        } else if (TextUtils.isEmpty(gtStreet)) {
            etStreetAddr.setError(getString(R.string.err_addr));
            etStreetAddr.requestFocus();
        } else if (TextUtils.isEmpty(gtCity)) {
            etCity.setError(getString(R.string.err_city));
            etCity.requestFocus();
        } else if (TextUtils.isEmpty(gtCountry)) {
            etCountry.setError(getString(R.string.err_country));
            etCountry.requestFocus();
        }
        return addressValidtaion();
    }

    private boolean addressValidtaion() {
        if (etRegionEdit.getVisibility() == View.VISIBLE && TextUtils.isEmpty(gtRegionEt)) {
            etRegionEdit.setError(getString(R.string.err_region));
            etRegionEdit.requestFocus();
        } else if (etRegionSp.getVisibility() == View.VISIBLE && TextUtils.isEmpty(gtRegionSp)) {
            etRegionSp.setError(getString(R.string.err_region));
            etRegionSp.requestFocus();
            etRegionSp.setFocusableInTouchMode(true);
        } else if (TextUtils.isEmpty(gtPcode)) {
            etPostCode.setError(getString(R.string.err_zip_code));
            etPostCode.requestFocus();
        } else if (TextUtils.isEmpty(gtPhone)) {
            etPhone.setError(getString(R.string.err_contact));
            etPhone.requestFocus();
        } else {
            success = true;
        }
        return success;
    }

    private void setCountryAdapter(List<Country> countryCollection) {

        lvCountry.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
        countryAdapter = new MyAccountAdapter(mContext, countryCollection, MyAccountAdapter.LIST_TYPE.COUNTRY);
        lvCountry.setAdapter(countryAdapter);
        lvCountry.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos, long id) {

                gtCountryId = coutryList.get(pos).getCountryId();
                etCountry.setText(coutryList.get(pos).getName());
                etRegionSp.setText("");
                countryAdapter.notifyDataSetChanged();
                /**
                 * check whether the selected country has id,
                 * if it has country id then request the region from server
                 */
                stateRequest(gtCountryId);
                countryDialog.dismiss();
            }
        });

        countryDialog.show();
    }

    /**
     * Request for update the address in a list from server
     */
    private void updateAddressRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            Map<String, String> updateAddressParams = new HashMap<>();
            updateAddressParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            updateAddressParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            updateAddressParams.put(Constant.Request.ACTION, Constant.Request.UPDATE_ADDRESS_ACTION);
            updateAddressParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            updateAddressParams.put(Constant.Request.TOKEN, application.getToken(mContext));

            updateAddressParams.put(Constant.Request.ADDRESS_ID, addrId);
            updateAddressParams.put(Constant.Request.FIRST_NAME, gtFname);
            updateAddressParams.put(Constant.Request.LAST_NAME, gtLname);
            updateAddressParams.put(Constant.Request.STREET, gtStreet);

            updateAddressParams.put(Constant.Request.CITY, gtCity);
            updateAddressParams.put(Constant.Request.POSTCODE, gtPcode);
            updateAddressParams.put(Constant.Request.TELEPHONE, gtPhone);

            updateAddressParams.put(Constant.Request.REGION, gtRegionValue);
            updateAddressParams.put(Constant.Request.COUNTRY_ID, gtCountryId);
            updateAddressParams.put(Constant.Request.IS_DEFAULT_BILLING, "1");
            updateAddressParams.put(Constant.Request.IS_DEFAULT_SHIPPING, "1");

            new RestClient(mContext).getInstance().get().manageAddress(updateAddressParams, new RestCallback<AddressResponse>());
        }
    }

    /**
     * Request for add the address in a list to server
     */
    private void addAddressRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            Map<String, String> addAddressParams = new HashMap<>();

            addAddressParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            addAddressParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            addAddressParams.put(Constant.Request.ACTION, Constant.Request.ADD_ADDRESS_ACTION);
            addAddressParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            addAddressParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            addAddressParams.put(Constant.Request.FIRST_NAME, gtFname);
            addAddressParams.put(Constant.Request.LAST_NAME, gtLname);
            addAddressParams.put(Constant.Request.STREET, gtStreet);
            addAddressParams.put(Constant.Request.CITY, gtCity);
            addAddressParams.put(Constant.Request.POSTCODE, gtPcode);
            addAddressParams.put(Constant.Request.TELEPHONE, gtPhone);
            addAddressParams.put(Constant.Request.REGION, gtRegionValue);
            addAddressParams.put(Constant.Request.COUNTRY_ID, gtCountryId);
            addAddressParams.put(Constant.Request.IS_DEFAULT_BILLING, "1");
            addAddressParams.put(Constant.Request.IS_DEFAULT_SHIPPING, "1");

            new RestClient(mContext).getInstance().get().manageAddress(addAddressParams, new RestCallback<AddressResponse>());
        }
    }

    @Subscribe
    public void dataReceived(AddressResponse result) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {
            Toast.makeText(mContext, result.getMessage(), Toast.LENGTH_LONG).show();
            if (McommApplication.isSuccess(result.getSuccess())) {
                mListener.onAddAddressInteraction();
            }
        } else {
            LogUtils.i(TAG, result.getMessage());
        }
    }

    /**
     * Request to get  countrylist from server
     */
    private void countryRequest() {

        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            new RestClient(mContext).getInstance().get().getCountryList(Constant.Request.COUNTRY_ACTION,
                    new RestCallback<CountryResponse>());

        }
    }

    @Subscribe
    public void dataReceived(CountryResponse response) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        if (McommApplication.isSuccess(response.getSuccess())) {
            backgroundTaskCallback = new BackgroundTaskCallback(mContext, response, BackgroundTaskCallback.TASK_TYPE.COUNTRY);
            backgroundTaskCallback.setOnTaskCompletionListener(this);
            backgroundTaskCallback.start();
        } else {
            LogUtils.i(TAG, response.getMessage());
        }
    }

    /**
     * Request to get  region list from server using the country code
     */
    private void stateRequest(String code) {

        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            new RestClient(mContext).getInstance().get().getStateList(Constant.Request.STATE_ACTION, code,
                    new RestCallback<RegionResponse>());
        }
    }

    @Subscribe
    public void dataReceived(RegionResponse response) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        if (McommApplication.isSuccess(response.getSuccess())) {
            regions = response.getRegions();
            disableRegion();
        } else {
            enableRegion();
        }

    }

    @Subscribe
    public void dataReceived(String errorMessage) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        enableRegion();
        LogUtils.i(TAG, errorMessage);
        if (errorMessage.equalsIgnoreCase(Constant.UNAUTHORIZED_VALUE)) {
            McommApplication.redirectAuthentication(mContext);
        } else {
            Utils.showToast(mContext, errorMessage, Toast.LENGTH_SHORT);
        }
    }


    /**
     * method to enable the edit text for region
     * and disable the spinner for region
     */
    private void enableRegion() {
        etRegionEdit.setVisibility(View.VISIBLE);
        etRegionSp.setVisibility(View.GONE);
        if (!stRegion.isEmpty())
            etRegionEdit.setText(stRegion);
        else
            etRegionEdit.setText("");
    }

    /**
     * method to disable the edit text for region
     * and enable the spinner for region
     */
    private void disableRegion() {
        etRegionSp.setVisibility(View.VISIBLE);
        etRegionEdit.setVisibility(View.GONE);
        if (!stRegion.isEmpty())
            etRegionSp.setText(stRegion);
        else
            etRegionSp.setText("");
    }


    /**
     * method to dismiss the progress bar
     */
    private void dismissProgress() {
        progress.dismissProgress(pDialog);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnAddAddressListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnAddAddressListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onTaskCompletion() {
        coutryList = new CountryExecutor(mContext).getCountry();
        setCountryAdapter(coutryList);
    }

    @Override
    public void onTaskCompletionError(String error) {
        LogUtils.e(Constant.TAG_EXCEPTION, error);
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnAddAddressListener {
        void onAddAddressInteraction();
    }
}