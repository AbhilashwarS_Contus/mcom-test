package com.contus.mcomm.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.contus.mcomm.utils.LogUtils;
import com.contussupport.ecommerce.Home;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement
 * <p/>
 * create an instance of this fragment.
 */

public class PaymentSuccessFragment extends Fragment implements View.OnClickListener {

    protected static final String TAG = LogUtils.makeLogTag(CartFragment.class);
    private Button continueShopping, goToHome;
    private Context mContext;
    private String orderID;

    public static PaymentSuccessFragment newInstance(String orderId) {

        PaymentSuccessFragment fragment = new PaymentSuccessFragment();
        fragment.setInitialCollection(orderId);
        return fragment;
    }

    private void setInitialCollection(String item) {
        this.orderID = item;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_payment_success, null);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.mContext = getActivity();
        initViews();
    }

    private void initViews() {

        View rootView = getView();
        if (rootView != null) {
            continueShopping = (Button) rootView.findViewById(R.id.view_order);
            goToHome = (Button) rootView.findViewById(R.id.continue_shopping);
        }
        continueShopping.setOnClickListener(this);
        goToHome.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.view_order:
                McommApplication.redirectMyOrder(mContext, orderID);
                break;
            case R.id.continue_shopping:
                Intent intent = new Intent(getActivity(), Home.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

}
