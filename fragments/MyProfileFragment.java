/**
 * FragmentHome.java
 * <p/>
 * This is a view class of Home Page.
 *
 * @category Contus
 * @package contus.com.mcomm
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.model.ChangeResponse;
import com.contus.mcomm.model.Login;
import com.contus.mcomm.model.LoginResponse;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.Home;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.google.gson.Gson;
import com.squareup.otto.Subscribe;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class MyProfileFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = LogUtils.makeLogTag(MyProfileFragment.class);
    Button btEdit, btSave;
    Context mContext;
    private EditText etFirstName, etLastName, etEmail, etDob;
    private Dialog pDialog;
    private Progress progress;
    private int birthDayValue, birthMonthValue, birthYearValue;
    private final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            birthDayValue = selectedDay;
            birthMonthValue = selectedMonth;
            birthYearValue = selectedYear;
            String birthDayDateFormat = Utils.getBirthDayDate(
                    selectedYear, selectedMonth, selectedDay);
            String dob = Utils.changeDateFormat(birthDayDateFormat, false);
            etDob.setText(dob);

        }

    };
    private String dateText, fNameText, lNameText, emailText;
    private McommApplication application;
    private Button btChngPwd;
    private Dialog dialog;
    private EditText oldPassword;
    private EditText newPassword;
    private Button submitPassword;
    private ImageView closeButton;
    private String oldStrPassword, newStrPassword;
    private String strDateOfBirth;
    private String dateInput;
    private boolean isEdit = false;

    public static MyProfileFragment newInstance() {
        return new MyProfileFragment();

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = getActivity();
        progress = new Progress(mContext);
        application = new McommApplication();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_profile, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
        application = new McommApplication();
        serverRequest();

    }

    /**
     * method to intialize the view
     */
    private void initViews() {
        View rootView = getView();
        etFirstName = (EditText) rootView.findViewById(R.id.edit_first_name);
        etLastName = (EditText) rootView.findViewById(R.id.edit_last_name);
        etEmail = (EditText) rootView.findViewById(R.id.edit_emailid);
        etDob = (EditText) rootView.findViewById(R.id.edit_dob);
        btEdit = (Button) rootView.findViewById(R.id.btEdit);
        btSave = (Button) rootView.findViewById(R.id.btSave);
        btChngPwd = (Button) rootView.findViewById(R.id.btChngPwd);

        etDob.setInputType(InputType.TYPE_NULL);
        etDob.setFocusable(false);
        etDob.setKeyListener(null);
        etDob.setClickable(true);

        disableEditText();
        btEdit.setOnClickListener(this);
        btSave.setOnClickListener(this);
        btChngPwd.setOnClickListener(this);
        etDob.setOnClickListener(this);

    }

    /**
     * method to perform the click event
     */
    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.btEdit:
                    enableEditText();
                    break;
                case R.id.btSave:
                    if (validate()) {
                        userEditRequest();
                        isEdit = true;
                    }
                    break;
                case R.id.btChngPwd:
                    passwordDialog();
                    break;
                case R.id.edit_dob:
                    Utils.getDatePickerDialog(getActivity(), datePickerListener,
                            birthDayValue, birthMonthValue, birthYearValue);
                    break;
                case R.id.close_button:
                    dialog.dismiss();
                    break;
                case R.id.submit_password:
                    validatePwd();
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            LogUtils.i(TAG, e.getMessage());
        }
    }


    /**
     * method to set the values for the view objects
     */
    public void setViewObjects(Login details) {

        Gson gson = new Gson();
        String loginDetails = gson.toJson(details);
        Utils.savePreferences(getActivity(), Constant.PREF_LOGINOBJECT, loginDetails);

        etFirstName.setText(details.getFirstName());
        etLastName.setText(details.getLastName());
        etEmail.setText(details.getEmail());
        strDateOfBirth = details.getDateOfBirth();
        String dob = Utils.changeDateFormat(strDateOfBirth, false);
        etDob.setText(dob);

        String[] firstValue = strDateOfBirth.split(Constant.SINGLE_SPACE);
        String dateValue = firstValue[0];
        try {
            if (dateValue != null) {
                SimpleDateFormat inputFormat = new SimpleDateFormat(
                        Constant.YYYY_MM_DD, Locale.ENGLISH);
                Date date = inputFormat.parse(dateValue);
                SimpleDateFormat outFormat = new SimpleDateFormat(
                        Constant.DD_MM_YYYY, Locale.ENGLISH);
                dateInput = outFormat.format(date);
                String[] split = dateInput.split(Constant.HYPHEN);
                String dateFormat = split[0];
                String month = split[1];
                String year = split[2];
                birthDayValue = Integer.parseInt(dateFormat);
                birthMonthValue = Integer.parseInt(month) - 1;
                birthYearValue = Integer.parseInt(year);
            }
        } catch (ParseException e) {
            LogUtils.i(TAG, e.getMessage());
        }
    }

    /**
     * method to enable all the edit text
     */


    public void enableEditText() {
        btEdit.setVisibility(View.GONE);
        btSave.setVisibility(View.VISIBLE);
        etFirstName.setEnabled(true);
        etLastName.setEnabled(true);
        etDob.setEnabled(true);
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(etFirstName, InputMethodManager.SHOW_IMPLICIT);

    }

    /**
     * method to disable all the edit text
     */
    public void disableEditText() {
        btEdit.setVisibility(View.VISIBLE);
        btSave.setVisibility(View.GONE);
        etFirstName.setEnabled(false);
        etLastName.setEnabled(false);
        etDob.setEnabled(false);
    }

    /**
     * method to validate the text values from all text view objects
     */
    private boolean validate() {
        boolean success = false;

        dateText = etDob.getText().toString();
        fNameText = etFirstName.getText().toString();
        lNameText = etLastName.getText().toString();
        emailText = etEmail.getText().toString();
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat(
                    Constant.MMM_DD_YYYY, Locale.ENGLISH);
            Date date = inputFormat.parse(dateText);
            SimpleDateFormat outFormat = new SimpleDateFormat(
                    Constant.MM_DD_YYYY, Locale.ENGLISH);
            dateInput = outFormat.format(date);
            dateText = dateInput;
        } catch (ParseException e) {
            Log.e(Constant.TAG_EXCEPTION, e.getMessage());
        }

        if (TextUtils.isEmpty(fNameText)) {
            etFirstName
                    .setError(getString(R.string.firstName_error));
            etFirstName.requestFocus();
        } else if (TextUtils.isEmpty(lNameText)) {
            etLastName.setError(getString(R.string.lastName_error));
            etLastName.requestFocus();
        } else if (TextUtils.isEmpty(emailText)) {
            etEmail.setError(getString(R.string.error_emailSignIn));
            etEmail.requestFocus();
        } else if (!validateEmail(emailText)) {
            etEmail.setError(getString(R.string.error_emailIncorrect));
            etEmail.requestFocus();
        } else if (TextUtils.isEmpty(dateText)) {
            etDob.setError(getString(R.string.error_dob));
            etDob.requestFocus();
        } else {
            success = true;
        }
        return success;

    }

    /**
     * method to validate the email id
     */
    private boolean validateEmail(String emails) {
        if (Utils.validateEmail(emails)) {
            return true;
        } else {
            etEmail.setError(getString(R.string.error_emailIncorrect));
            etEmail.requestFocus();
            return false;
        }
    }

    /**
     * method to dismiss the progress
     */
    private void dismissProgress() {
        progress.dismissProgress(pDialog);
    }

    /**
     * method to show the password dialog to enter the old and new password
     */
    private void passwordDialog() {
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_change_pwd);
        dialog.setCancelable(true);
        oldPassword = (EditText) dialog.findViewById(R.id.old_password);
        newPassword = (EditText) dialog.findViewById(R.id.new_password);
        submitPassword = (Button) dialog.findViewById(R.id.submit_password);
        closeButton = (ImageView) dialog.findViewById(R.id.close_button);
        closeButton.setOnClickListener(this);
        submitPassword.setOnClickListener(this);
        dialog.show();
    }

    /**
     * method to validate the password
     */
    private void validatePwd() {
        oldStrPassword = oldPassword.getText().toString();
        newStrPassword = newPassword.getText().toString();
        if (TextUtils.isEmpty(oldStrPassword)) {
            oldPassword
                    .setError(getString(R.string.passwordsignin_error));
            oldPassword.requestFocus();
        } else if (TextUtils.isEmpty(newStrPassword)) {
            newPassword
                    .setError(getString(R.string.passwordsignin_error));
            newPassword.requestFocus();
        } else if (!TextUtils.isEmpty(oldStrPassword)) {
            validatePwdLen();
        } else if (!TextUtils.isEmpty(newStrPassword)) {
            validatePwdLen();
        }
    }

    /**
     * method to validate the password length;
     */
    private void validatePwdLen() {
        if (oldStrPassword.length() < Constant.MINIMUM_PASSWORD_LENTH) {
            oldPassword
                    .setError(getString(R.string.alert_passwordMandatory));
        } else if (newStrPassword.length() < Constant.MINIMUM_PASSWORD_LENTH) {
            newPassword
                    .setError(getString(R.string.alert_passwordMandatory));
        } else if (oldStrPassword
                .equalsIgnoreCase(newStrPassword)) {
            Utils.showToast(mContext, getString(R.string.error_pwd_same), Toast.LENGTH_LONG);
        } else {
            changeSignatureRequest();
        }

    }

    /**
     * Password validation by requesting the data from the server.
     */

    private void changeSignatureRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            Map<String, String> changeParams = new HashMap<>();
            changeParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            changeParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            changeParams.put(Constant.Request.ACTION, Constant.Request.CHANGE_SIGNATURE_ACTION);
            changeParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            changeParams.put(Constant.Request.OLD_SIGNATURE, oldStrPassword);
            changeParams.put(Constant.Request.NEW_SIGNATURE, newStrPassword);
            changeParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            new RestClient(mContext).getInstance().get().changePassword(changeParams, new RestCallback<ChangeResponse>());
        }
    }

    @Subscribe
    public void dataReceived(ChangeResponse result) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        if (dialog != null && dialog.isShowing())
            dialog.dismiss();

        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {
            ((Home) getActivity()).updateDrawer();
            Utils.showToast(mContext,
                    result.getMessage(), Toast.LENGTH_SHORT);
        } else {
            Utils.showToast(mContext, result.getMessage(),
                    Toast.LENGTH_SHORT);
        }

    }


    /**
     * Request for edit the profile details in a list
     */
    private void userEditRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            Map<String, String> registerParams = new HashMap<>();
            registerParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            registerParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            registerParams.put(Constant.Request.ACTION, Constant.Request.CUSTOMER_UPDATE_ACTION);
            registerParams.put(Constant.Request.EMAIL, emailText);
            registerParams.put(Constant.Request.FIRST_NAME, fNameText);
            registerParams.put(Constant.Request.LAST_NAME, lNameText);
            registerParams.put(Constant.Request.NEWSLETTER, "1");
            registerParams.put(Constant.Request.DATE_OF_BIRTH, dateText);
            registerParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            registerParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            new RestClient(mContext).getInstance().get().getProfile(registerParams, new RestCallback<LoginResponse>());
        }
    }

    /**
     * Request for profile details to list
     */
    private void serverRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            Map<String, String> personalParams = new HashMap<>();
            personalParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            personalParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            personalParams.put(Constant.Request.ACTION, Constant.Request.PERSONAL_ACTION);
            personalParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            personalParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            new RestClient(mContext).getInstance().get().getProfile(personalParams, new RestCallback<LoginResponse>());
        }
    }

    @Subscribe
    public void dataReceived(String errorMessage) {
        dismissProgress();
        LogUtils.i(TAG, errorMessage);
        BusProvider.getInstance().unregister(this);
        if (errorMessage.equalsIgnoreCase(Constant.UNAUTHORIZED_VALUE)) {
            McommApplication.redirectAuthentication(mContext);
        }
    }

    @Subscribe
    public void dataReceived(LoginResponse result) {
        dismissProgress();
        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {
            Login userResult = result.login;
            if (isEdit) {
                Utils.showToast(mContext, result.getMessage(), Toast.LENGTH_SHORT);
                isEdit = false;
            }
            setViewObjects(userResult);
            disableEditText();
        } else {
            LogUtils.i(TAG, result.getMessage());
        }
        BusProvider.getInstance().unregister(this);
    }

}