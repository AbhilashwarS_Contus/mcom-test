/**
 * MyWishlistFragment.java
 * <p/>
 * This is a view class of My wish list Page .
 *
 * @category Contus
 * @package contus.com.mcomm
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.adapters.MyAccountAdapter;
import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.model.Product;
import com.contus.mcomm.model.Wishlist;
import com.contus.mcomm.model.WishlistResponse;
import com.contus.mcomm.service.DbParser;
import com.contus.mcomm.service.OnParsingCompletionListener;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.CustomDialog;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MyWishlistFragment extends Fragment implements View.OnClickListener, OnParsingCompletionListener, CustomDialog.OnDialogSelectedListener {
    private static final String TAG = LogUtils.makeLogTag(MyWishlistFragment.class);
    private Context mContext;
    private ListView myWishListView;
    private McommApplication application;
    private List<Wishlist> wishlistItem;
    private Dialog pDialog;
    private Progress progress;
    private MyAccountAdapter accountAdapter;
    private boolean isDelete = false;
    private TextView emptyView;
    private int selectedPosition;
    private OnWishListInteractionListener mListener;
    private CustomDialog mydialog;

    public static MyWishlistFragment newInstance() {
        return new MyWishlistFragment();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_wishlist, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = this.getActivity();
        progress = new Progress(mContext);
        initViews();
        application = new McommApplication();
        onItemSelected(true);
        serverRequest();

    }

    /**
     * method to initialize all the view objects
     */
    private void initViews() {
        View rootView = getView();
        myWishListView = (ListView) rootView.findViewById(R.id.listview);
        emptyView = (TextView) rootView.findViewById(R.id.emptyView);
    }


    /**
     * method to show list view when the list has value
     */
    private void setListView() {
        myWishListView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
    }

    /**
     * method to show empty view when the list is empty
     */
    private void setEmptyView() {
        myWishListView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        emptyView.setText(getResources().getString(R.string.no_results_wishlist));
    }

    /**
     * mehtod to set the values to the adapter
     */
    private void setWishlistAdapter() {
        if (isDelete) {
            isDelete = false;
            wishlistItem.remove(selectedPosition);
            accountAdapter.notifyDataSetChanged();
        } else if (!wishlistItem.isEmpty()) {
            setListView();
            accountAdapter = new MyAccountAdapter(mContext, wishlistItem, MyAccountAdapter.LIST_TYPE.WISHLIST);
            myWishListView.setAdapter(accountAdapter);
            accountAdapter.setOnClickAccountListener(this);
            myWishListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
                        onItemSelected("ProductList", wishlistItem.get(position));
                    }
                }
            });
        }
        /**
         * check whether the wish list item is empty or not
         */
        if (wishlistItem.isEmpty()) {
            setEmptyView();
        }
    }

    /**
     * method to get the porduct list interaction
     */
    public void onItemSelected(String type, Wishlist productItem) {
        Product item = new Product(productItem);
        if (mListener != null) {
            mListener.onProductsListInteraction(type, item);
        }
    }

    /**
     * method to get the wishlist list interaction
     */
    public void onItemSelected(boolean flag) {
        if (mListener != null) {
            mListener.onAddWishlistInteraction(flag);
        }
    }

    /**
     * method to perform click event when yes button is pressed in the dialog
     */
    @Override
    public void onYesClick(int type) {
        if (type == Constant.DIALOG_WISHLIST_DELETE) {
            isDelete = true;
            deleteWishlistRequest(selectedPosition);
            mydialog.dismiss();
        }
    }

    /**
     * method to perform click event when no button is pressed in the dialog
     */
    @Override
    public void onNoClick(int type) {
        mydialog.dismiss();
    }

    /**
     * method to dismiss the progress
     */
    private void dismissProgress() {

        progress.dismissProgress(pDialog);
    }

    /**
     * mehtod to perform the click event
     */
    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.img_wish_delete) {
            isDelete = true;
            if (McommApplication.isNetandLogin(mContext, Constant.CHECK_USER_LOGIN)) {
                selectedPosition = Integer.parseInt(v.getTag().toString());
                delWishlistDialog();
            }

        }
    }

    /**
     * Request for product list with specified category id
     *
     * @param position
     */
    private void deleteWishlistRequest(int position) {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            new RestClient(mContext).getInstance().get().deleteWishlist(McommApplication.getStoreId(mContext), Constant.WEBSITE_ID_VALUE,
                    application.getCustomerId(mContext), application.getToken(mContext),
                    wishlistItem.get(position).getEntityId().toString(),
                    Constant.Request.DELETE_WISHLIST_ACTION, new RestCallback<WishlistResponse>());
        }
    }

    /**
     * method to show dialog to delete the address
     */
    private void delWishlistDialog() {

        Bundle args = new Bundle();
        args.putInt(Constant.DIALOG_CART_PARAM, Constant.DIALOG_WISHLIST_DELETE);
        mydialog = new CustomDialog();
        mydialog.setArguments(args);
        mydialog.setTargetFragment(this, 0);
        mydialog.show(getFragmentManager(), "dialog");

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnWishListInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Request for the wishlist details to list from the server
     */
    private void serverRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            Map<String, String> listParams = new HashMap<>();
            listParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            listParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            listParams.put(Constant.Request.ACTION, Constant.Request.WISHLIST_ACTION);
            listParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            listParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            new RestClient(mContext).getInstance().get().getWishlist(listParams, new RestCallback<WishlistResponse>());
        }
    }

    /**
     * method to get the error response from the server
     *
     * @param errorMessage
     */
    @Subscribe
    public void dataReceived(String errorMessage) {
        BusProvider.getInstance().unregister(this);
        dismissProgress();
        if (errorMessage.equalsIgnoreCase(Constant.UNAUTHORIZED_VALUE)) {
            McommApplication.redirectAuthentication(mContext);
        } else {
            Utils.showToast(mContext, errorMessage, Toast.LENGTH_SHORT);
        }
    }

    /**
     * method to get the wishlist response from the server
     *
     * @param result
     */
    @Subscribe
    public void dataReceived(WishlistResponse result) {
        BusProvider.getInstance().unregister(this);

        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {
            if (!isDelete) {
                dismissProgress();
                wishlistItem = result.wishlist;
                setWishlistAdapter();
            } else if (isDelete) {
                updateDatabase(selectedPosition);
                Utils.showToast(mContext, result.getMessage(), Toast.LENGTH_SHORT);
            } else {
                setWishlistAdapter();
            }

        } else {
            LogUtils.i(TAG, result.getMessage());
        }
    }

    /**
     * method to update the database by getting the wishlist item position
     *
     * @param position
     */
    private void updateDatabase(int position) {
        Wishlist item = wishlistItem.get(position);
        DbParser parser;
        try {
            parser = new DbParser(mContext, item.getEntityId(), DbParser.PARSE_TYPE.WISHLIST_UPDATE);
            parser.setOnParsingCompletionListener(this);
            parser.start();
        } catch (Exception e) {
            dismissProgress();
            LogUtils.i(TAG, e.getMessage());
        }
    }

    /**
     * overridden method to perform parsing
     */
    @Override
    public void onParsingCompletion() {
        dismissProgress();
        onItemSelected(true);
        setWishlistAdapter();
    }

    /**
     * overridden method to perform parsing completion error
     */
    @Override
    public void onParsingCompletionError(String error) {
        dismissProgress();
        LogUtils.i(Constant.TAG_EXCEPTION, error);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnWishListInteractionListener {
        public void onProductsListInteraction(String type, Product productItem);

        public void onAddWishlistInteraction(boolean flag);
    }
}




