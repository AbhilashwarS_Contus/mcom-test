package com.contus.mcomm.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.model.Address;
import com.contus.mcomm.model.CartAddress;
import com.contus.mcomm.model.CartAttribute;
import com.contus.mcomm.model.CartItem;
import com.contus.mcomm.model.CartList;
import com.contus.mcomm.model.Checkout;
import com.contus.mcomm.model.CheckoutResponse;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by contus on 4/5/15.
 */
public class CheckoutFragment extends Fragment implements View.OnClickListener {

    protected static final String TAG = LogUtils.makeLogTag(CheckoutFragment.class);
    private View rootView;
    private LinearLayout viewCheckoutItems;
    private TextView title, price, quantity, quantityAmount, userMail;
    private TextView deliveryName, deliveryStreet, deliveryCity, deliveryCountry, deliveryZipcode;
    private TextView deliveryPhone, itemCount, subTotal, grandTotal, shippingCharge, txBillingId, txDeliveryId;
    private Button proceedToPayment, selectAddress;
    private ImageView ivSummaryImage, addressEditImage, etBillAddress;
    private LayoutInflater inflater;
    private List<CartItem> cartItems;
    private Picasso mPicasso;
    private McommApplication application;
    private Dialog pDialog;
    private Context mContext;
    private String shippingId, billingId, currency;
    private Address shippingAdress;
    private CartList cartList;
    private OnCheckoutInteractionListener mListener;
    private String totalamount;
    private String firstName, postalCode, street, city, country, phone;
    private LinearLayout liConfigLayout;
    private CheckBox chShipAddress;
    private Button selectShipAddress;
    private RelativeLayout shipAddressLayout;
    private boolean sameBilling = true;
    private TextView txBillName, txBillStreet, txBillCity, txBillCountry, txBillPostCode, txDeliveryLine, txBillPhone;
    private Checkout checkout;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param item Id of the product.
     * @return A new instance of fragment ProductDetailFragment.
     */
    public static CheckoutFragment newInstance(CartList item) {

        CheckoutFragment fragment = new CheckoutFragment();
        fragment.setInitialCollection(item);
        return fragment;
    }

    private void setInitialCollection(CartList item) {
        this.cartList = item;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_checkout, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        currency = McommApplication.getCurrencySymbol(mContext) + Constant.SINGLE_SPACE;
        initViews();

    }

    private void initViews() {
        try {
            this.application = new McommApplication();
            rootView = getView();
            inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            cartItems = cartList.getItems();
            shippingAdress = cartList.getAddress().getShippingAddress();
            if (rootView != null) {
                viewCheckoutItems = (LinearLayout) rootView.findViewById(R.id.summary_view);
                proceedToPayment = (Button) rootView.findViewById(R.id.proceed_payment_btn);
                selectAddress = (Button) rootView.findViewById(R.id.select_address);
                deliveryName = (TextView) rootView.findViewById(R.id.delivery_name);
                deliveryStreet = (TextView) rootView.findViewById(R.id.delivery_street);
                deliveryCity = (TextView) rootView.findViewById(R.id.delivery_city);
                deliveryCountry = (TextView) rootView.findViewById(R.id.delivery_country);
                deliveryZipcode = (TextView) rootView.findViewById(R.id.delivery_zipcode);
                deliveryPhone = (TextView) rootView.findViewById(R.id.delivery_phone);
                addressEditImage = (ImageView) rootView.findViewById(R.id.address_edit);
                etBillAddress = (ImageView) rootView.findViewById(R.id.bill_edit);
                itemCount = (TextView) rootView.findViewById(R.id.item_count);
                subTotal = (TextView) rootView.findViewById(R.id.payment_amount);
                grandTotal = (TextView) rootView.findViewById(R.id.total_amount);
                shippingCharge = (TextView) rootView.findViewById(R.id.delivery_charge);
                userMail = (TextView) rootView.findViewById(R.id.mail_id);
                selectShipAddress = (Button) rootView.findViewById(R.id.select_ship_address);
                shipAddressLayout = (RelativeLayout) rootView.findViewById(R.id.ship_address_layout);
                chShipAddress = (CheckBox) rootView.findViewById(R.id.ship_address);
                txDeliveryId = (TextView) rootView.findViewById(R.id.delivery_id);
                txBillingId = (TextView) rootView.findViewById(R.id.billing_id);
                chShipAddress.setOnClickListener(this);
                initShippView();
                chShipAddress.setChecked(true);
                proceedToPayment.setOnClickListener(this);
                addressEditImage.setOnClickListener(this);
                selectAddress.setOnClickListener(this);
                selectShipAddress.setOnClickListener(this);
                etBillAddress.setOnClickListener(this);
                viewCheckoutItems.removeAllViews();
                setTotalValues();
            }
            if (shippingAdress != null) {
                setAddressItem(shippingAdress);
            } else {
                rootView.findViewById(R.id.delivery_address).setVisibility(View.GONE);
                selectAddress.setVisibility(View.VISIBLE);
            }
            this.mContext = getActivity();
            mPicasso = Picasso.with(getActivity());

            addCartItems();
        } catch (Exception e) {
            LogUtils.i(TAG, e.getMessage());
        }
    }

    private void setTotalValues() {
        totalamount = cartList.getGrandTotal();
        userMail.setText(McommApplication.getLoginDetails(getActivity()).getEmail());
        itemCount.setText(cartList.getItemCount());
        subTotal.setText(currency + cartList.getSubtotal());
        String shippingAmount = cartList.getShippingAmount();
        if (Utils.checkFreeShipping(shippingAmount))
            shippingCharge.setText(getResources().getString(R.string.free_shipping));
        else
            shippingCharge.setText(shippingAmount);
        grandTotal.setText(currency + totalamount);
    }

    public void setAddressItem(Address selectedAddress) {
        firstName = selectedAddress.getFirstName() + Constant.SINGLE_SPACE + selectedAddress.getLastName();
        postalCode = selectedAddress.getPostcode();
        street = getMultipleStreet(selectedAddress.getStreet());
        city = selectedAddress.getCity();
        country = selectedAddress.getCountryName();
        phone = selectedAddress.getTelephone();

        if (sameBilling) {
            rootView.findViewById(R.id.delivery_address).setVisibility(View.VISIBLE);
            selectAddress.setVisibility(View.GONE);
            txDeliveryId.setText(selectedAddress.getAddressId());
            setAddress();
        } else {
            txBillingId.setText(selectedAddress.getAddressId());
            shipAddressLayout.setVisibility(View.VISIBLE);
            selectShipAddress.setVisibility(View.GONE);
            rootView.findViewById(R.id.delivery_address_line).setVisibility(View.VISIBLE);
            txBillName.setText(firstName);
            txBillPostCode.setText("Postal Code: " + postalCode + ".");
            txBillStreet.setText(street + ",");
            txBillCity.setText(city + ",");
            txBillCountry.setText(country + ".");
            txBillPhone.setText("Phone: " + phone + ".");

        }
    }

    public void setAddress(Address selectedAddress) {
        CartAddress address = cartList.getAddress();
        if (sameBilling) {
            selectedAddress.setIsDefaultShipping(true);
            address.setShippingAddress(selectedAddress);
        } else {
            selectedAddress.setIsDefaultBilling(true);
            address.setBillingAddress(selectedAddress);
        }
        setAddressItem(selectedAddress);
    }

    private void setAddress() {

        if (TextUtils.isEmpty(firstName.trim()))
            deliveryName.setVisibility(View.GONE);
        else
            deliveryName.setText(firstName);

        if (TextUtils.isEmpty(postalCode.trim()))
            deliveryZipcode.setVisibility(View.GONE);
        else
            deliveryZipcode.setText("Postal Code: " + postalCode + ".");

        if (TextUtils.isEmpty(street.trim()))
            deliveryStreet.setVisibility(View.GONE);
        else
            deliveryStreet.setText(street + ",");

        if (TextUtils.isEmpty(city.trim()))
            deliveryCity.setVisibility(View.GONE);
        else
            deliveryCity.setText(city + ",");

        if (TextUtils.isEmpty(country.trim()))
            deliveryCountry.setVisibility(View.GONE);
        else
            deliveryCountry.setText(country + ".");

        if (TextUtils.isEmpty(phone.trim()))
            deliveryPhone.setVisibility(View.GONE);
        else
            deliveryPhone.setText("Phone: " + phone + ".");


    }

    private String getMultipleStreet(String[] streets) {
        String streetAddress = "";
        for (int i = 0; i < streets.length; i++) {
            if ("".equals(streetAddress))
                streetAddress += streets[i];
            else
                streetAddress = streetAddress + ", " + streets[i];
        }
        return streetAddress;
    }

    private void addAddressCart() {
        Progress progress = new Progress(mContext);
        pDialog = progress.showProgress();
        pDialog.show();
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {

            BusProvider.getInstance().register(this);
            Map<String, String> addAddressParams = new HashMap<>();

            addAddressParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            addAddressParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            addAddressParams.put(Constant.Request.ACTION, Constant.Request.ADD_ADDRESS_CART);
            addAddressParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            addAddressParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));

            addAddressParams.put(Constant.Request.CART_REQ_QUOTE_ID, application.getQuoteId(mContext));
            addAddressParams.put(Constant.Request.BILLING_ADDRESS_ID, billingId);
            addAddressParams.put(Constant.Request.SHIPPING_ADDRESS_ID, shippingId);
            new RestClient(mContext).getInstance().get().addAddressCart(addAddressParams, new RestCallback<CheckoutResponse>());
        }
    }

    @Subscribe
    public void dataReceived(String errorMessage) {
        BusProvider.getInstance().unregister(this);
        pDialog.cancel();
        if (errorMessage.equalsIgnoreCase(Constant.UNAUTHORIZED_VALUE)) {
            McommApplication.redirectAuthentication(mContext);
        } else {
            Utils.showToast(mContext, errorMessage, Toast.LENGTH_SHORT);
        }
    }

    @Subscribe
    public void dataReceived(CheckoutResponse response) {
        BusProvider.getInstance().unregister(this);
        pDialog.cancel();
        if (McommApplication.checkResponse(response.getError(), response.getSuccess())) {
            if (McommApplication.isSuccess(response.getSuccess())) {
                checkout = response.getResult();
                onItemSelected(0);
            } else {
                Utils.showToast(mContext, response.getMessage(), Toast.LENGTH_SHORT);
            }
        } else {
            LogUtils.i(TAG, response.getMessage());
        }
    }

    private void addCartItems() {
        try {
            for (int i = 0; i < cartItems.size(); i++) {
                CartItem cartItem;
                View view = inflater.inflate(R.layout.row_order_summary_item, null);
                title = (TextView) view.findViewById(R.id.ordered_product_title);
                price = (TextView) view.findViewById(R.id.ordered_product_price);
                quantity = (TextView) view.findViewById(R.id.quantity);
                quantityAmount = (TextView) view.findViewById(R.id.quantity_amount);
                ivSummaryImage = (ImageView) view.findViewById(R.id.order_summary_image);
                liConfigLayout = (LinearLayout) view.findViewById(R.id.config_option);
                cartItem = cartItems.get(i);
                title.setText(cartItem.getName());
                price.setText(currency + cartItem.getPrice());
                quantity.setText("Quantity " + cartItem.getQty() + " = ");
                quantityAmount.setText(currency + cartItem.getRowTotal());
                viewCheckoutItems.addView(view);
                String imgUrl = cartItem.getImageUrl();
                if (imgUrl != null && !imgUrl.isEmpty()) {
                    mPicasso.load(imgUrl).placeholder(R.drawable.place_holder).
                            into(ivSummaryImage);
                }

                addConfigView(cartItem);

            }
        } catch (Exception e) {
            LogUtils.i(TAG, e.getMessage());
        }
    }

    private void addConfigView(CartItem cartItem) {

        if (cartItem.getTypeId().equalsIgnoreCase(Constant.TYPE_CONFIGURABLE)) {
            List<CartAttribute> attibute = cartItem.getConfig().getAttribute();
            liConfigLayout.removeAllViews();
            for (int j = 0; j < attibute.size(); j++) {
                View cartView = inflater.inflate(R.layout.row_cart_config, null);
                TextView attrLabel = (TextView) cartView.findViewById(R.id.config_label);
                TextView attrValue = (TextView) cartView.findViewById(R.id.config_value);
                attrLabel.setText(attibute.get(j).getLabel() + ":");
                attrValue.setText(attibute.get(j).getValue());
                liConfigLayout.addView(cartView);
            }
            liConfigLayout.setVisibility(View.VISIBLE);
        } else {
            liConfigLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.proceed_payment_btn:
                proceedCheckout();
                break;
            case R.id.select_address:
            case R.id.address_edit:
                //Do the Edit Action here to change the address
                onItemSelected(1);
                sameBilling = true;
                break;
            case R.id.select_ship_address:
            case R.id.bill_edit:
                onItemSelected(1);
                sameBilling = false;
                break;
            case R.id.ship_address:
                if (chShipAddress.isChecked()) {
                    setAddressVisibility(View.GONE);
                    sameBilling = true;
                } else {
                    setBillingView();
                }
                break;
            default:
                break;
        }
    }

    private void setBillingView() {

        sameBilling = false;
        Address billingAddress = cartList.getAddress().getBillingAddress();
        if (billingAddress != null) {
            setAddressItem(billingAddress);
            setAddressVisibility(View.VISIBLE);
        } else {
            selectShipAddress.setVisibility(View.VISIBLE);
            setAddressVisibility(View.GONE);
        }
    }

    private void setAddressVisibility(int visible) {
        shipAddressLayout.setVisibility(visible);
        txDeliveryLine.setVisibility(visible);
    }

    private void proceedCheckout() {
        if (chShipAddress.isChecked()) {
            billingId = txDeliveryId.getText().toString();
            shippingId = txDeliveryId.getText().toString();
        } else {
            billingId = txBillingId.getText().toString();
            shippingId = txDeliveryId.getText().toString();
        }
        if (shippingId.isEmpty() && billingId.isEmpty())
            Utils.showToast(mContext, getString(R.string.no_address), Toast.LENGTH_LONG);
        else
            addAddressCart();

    }

    private void initShippView() {
        txBillName = (TextView) rootView.findViewById(R.id.bill_name);
        txBillStreet = (TextView) rootView.findViewById(R.id.bill_street);
        txBillCity = (TextView) rootView.findViewById(R.id.bill_city);
        txBillCountry = (TextView) rootView.findViewById(R.id.bill_country);
        txBillPostCode = (TextView) rootView.findViewById(R.id.bill_zipcode);
        txBillPhone = (TextView) rootView.findViewById(R.id.bill_phone);
        txDeliveryLine = (TextView) rootView.findViewById(R.id.delivery_address_line);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnCheckoutInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onItemSelected(int which) {
        if (mListener != null) {
            if (which == 0)
                mListener.onCheckoutInteraction(totalamount, checkout);
            else
                mListener.onCheckoutAddress(cartList);
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnCheckoutInteractionListener {
        public void onCheckoutInteraction(String total, Checkout collection);

        public void onCheckoutAddress(CartList cartList);
    }


}
