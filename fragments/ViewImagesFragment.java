package com.contus.mcomm.fragments;
/**
 * ViewImagesFragment.java
 * <p/>
 * This is the fragment view for displaying the products images and zoom.
 *
 * @category Contus
 * @package com.contus.mcomm.fragments
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.contus.mcomm.adapters.ProductImageAdapter;
import com.contus.mcomm.externallibraries.HorizontalListView;
import com.contus.mcomm.externallibraries.TouchImageView;
import com.contussupport.ecommerce.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement
 * to handle interaction events.
 * Use the {@link com.contus.mcomm.fragments.ViewImagesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViewImagesFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String IMG_URLS = "imgUrls";
    private static final String IMG_POSITION = "position";
    ProductImageAdapter imageAdapter;
    private List<String> imgUrls = new ArrayList<>();
    private int position = 0;
    private HorizontalListView imagesList;
    /**
     * The view pager.
     */
    private ViewPager viewPager;
    private Picasso mPicasso;
    /**
     * The viewpageradapter.
     */
    private ViewPagerAdapter viewPagerAdapter;

    public ViewImagesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param images   Parameter 1.
     * @param position Parameter 2.
     * @return A new instance of fragment ViewImagesFragment.
     */
    public static ViewImagesFragment newInstance(List<String> images, int position) {
        ViewImagesFragment fragment = new ViewImagesFragment();
        Bundle args = new Bundle();
        args.putStringArrayList(IMG_URLS, (ArrayList<String>) images);
        args.putInt(IMG_POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            imgUrls = getArguments().getStringArrayList(IMG_URLS);
            position = getArguments().getInt(IMG_POSITION);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mPicasso = Picasso.with(getActivity());
        View rootView = inflater.inflate(R.layout.fragment_view_images, container, false);
        viewPager = (ViewPager) rootView.findViewById(R.id.pager);
        viewPagerAdapter = new ViewPagerAdapter(getActivity(), imgUrls);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setCurrentItem(position);
        imagesList = (HorizontalListView) rootView.findViewById(R.id.product_images);
        imagesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ViewImagesFragment.this.position = position;
                imageAdapter.setPosition(ViewImagesFragment.this.position);
                imageAdapter.notifyDataSetChanged();
                viewPager.setCurrentItem(ViewImagesFragment.this.position);
            }
        });

        if (!imgUrls.isEmpty()) {
            imageAdapter = new ProductImageAdapter(getActivity(), imgUrls, position, ProductImageAdapter.LIST_TYPE.IMAGES_LIST);
            imagesList.setAdapter(imageAdapter);
        }
        viewPager.setOnPageChangeListener(new OnPageChangeListener());

        return rootView;
    }

    /**
     * View pager class adapter to display images.
     */
    class ViewPagerAdapter extends PagerAdapter {

        /**
         * The activity.
         */
        private Activity activity;

        /**
         * The img array.
         */
        private List<String> imgArray;

        /**
         * The views.
         */
        private SparseArray<TouchImageView> views = new SparseArray<>();

        /**
         * Instantiates a new view pager adapter.
         *
         * @param act     the act
         * @param imgArra the img arra
         */
        public ViewPagerAdapter(Activity act, List<String> imgArra) {
            imgArray = imgArra;
            activity = act;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.support.v4.view.PagerAdapter#getCount()
         */
        @Override
        public int getCount() {
            return imgArray.size();
        }

        /*
         * (non-Javadoc)
         *
         * @see
         * android.support.v4.view.PagerAdapter#isViewFromObject(android.view
         * .View, java.lang.Object)
         */
        @Override
        public boolean isViewFromObject(View arg0, Object arg1) {
            return arg0 == arg1;
        }

        /*
         * (non-Javadoc)
         *
         * @see
         * android.support.v4.view.PagerAdapter#instantiateItem(android.view
         * .View, int)
         */
        public Object instantiateItem(View containers, int position) {
            LayoutInflater layoutinflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutinflater.inflate(R.layout.image_viewpager_row,
                    null);
            TouchImageView mImageView = (TouchImageView) view
                    .findViewById(R.id.imageViews);
            String imageUrl = imgArray.get(position);
            mPicasso.load(imageUrl).placeholder(R.drawable.place_holder).
                    into(mImageView);
            ((ViewPager) containers).addView(view, 0);
            views.put(position, mImageView);
            return view;
        }

        /*
         * (non-Javadoc)
         *
         * @see
         * android.support.v4.view.PagerAdapter#destroyItem(android.view.View,
         * int, java.lang.Object)
         */
        @Override
        public void destroyItem(View collection, int position, Object o) {
            View view = (View) o;
            ((ViewPager) collection).removeView(view);
            views.remove(position);
            view = null;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.support.v4.view.PagerAdapter#saveState()
         */
        @Override
        public Parcelable saveState() {
            return null;
        }


        /*
         * (non-Javadoc)
         *
         * @see
         * android.support.v4.view.PagerAdapter#getItemPosition(java.lang.Object
         * )
         */
        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }


    private class OnPageChangeListener implements ViewPager.OnPageChangeListener {
        @Override
        public void onPageSelected(int position) {
            viewPagerAdapter.notifyDataSetChanged();
            imageAdapter.setPosition(position);
            imageAdapter.notifyDataSetChanged();
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
            // Un used Code .. Implemented Method
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
            // Un used Code .. Implemented Method
        }
    }
}
