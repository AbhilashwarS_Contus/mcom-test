/**
 * RegisterFragment.java
 * <p/>
 * This is the fragment view for Register.
 *
 * @category Contus
 * @package com.contus.mcomm.fragments
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.database.CountryExecutor;
import com.contus.mcomm.model.Country;
import com.contus.mcomm.model.Login;
import com.contus.mcomm.model.LoginResponse;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.SpaceAdjust;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.Authentication;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class RegisterFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = LogUtils.makeLogTag(RegisterFragment.class);
    private TextView alreadyHaveanAccount;
    /**
     * The date input.
     */
    private String firstNameText, lastNameText, emailText, passwordText,
            dateOfBirthText, dateInput;
    private Context mContext;
    /**
     * The date of birth.
     */
    private EditText firstName, lastName, email, password, dateOfBirth;
    /**
     * The register button.
     */
    private Button registerButton;
    /**
     * The birth year value.
     */
    private int birthDayValue, birthMonthValue, birthYearValue;
    private Dialog pDialog;
    private Progress progress;
    private CountryExecutor countryExecutor;
    private List<Country> coutryList;
    private String birthDayDateFormat;
    /**
     * method to perform the date pick listener
     */
    private final DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {
            birthDayValue = selectedDay;
            birthMonthValue = selectedMonth;
            birthYearValue = selectedYear;
            try {
                birthDayDateFormat = Utils.getBirthDayDate(
                        selectedYear, selectedMonth, selectedDay);
                dateOfBirth.setText(Utils.changeDateFormat(birthDayDateFormat, false));
                dateOfBirth.setError(null);
            } catch (Exception e) {
                LogUtils.e(Constant.TAG_EXCEPTION, e.getMessage());
            }

        }
    };

    /**
     * Returns a new instance of this fragment.
     */
    public static RegisterFragment newInstance() {
        return new RegisterFragment();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        initLoginElements(view);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = getActivity();
        progress = new Progress(mContext);

    }

    /**
     * method to initialise all view objects
     */
    private void initLoginElements(View view) {
        alreadyHaveanAccount = (TextView) view.findViewById(R.id.alreadyHaveanAccount);
        firstName = (EditText) view.findViewById(R.id.firstname);
        lastName = (EditText) view.findViewById(R.id.lastname);
        email = (EditText) view.findViewById(R.id.email);
        password = (EditText) view.findViewById(R.id.password);
        dateOfBirth = (EditText) view.findViewById(R.id.dateOfBirth);
        dateOfBirth.setInputType(InputType.TYPE_NULL);
        dateOfBirth.setFocusable(false);
        alreadyHaveanAccount = (TextView) view
                .findViewById(R.id.alreadyHaveanAccount);
        dateOfBirth.setKeyListener(null);
        dateOfBirth.setClickable(true);


        registerButton = (Button) view.findViewById(R.id.registerButton);
        registerButton.setOnClickListener(this);
        dateOfBirth.setOnClickListener(this);

        String alreadyHaveAccount = getString(R.string.already_have_an_account);
        SpannableString spannableString = new SpannableString(alreadyHaveAccount);
        spannableString.setSpan(new ForegroundColorSpan(getResources()
                .getColor(R.color.blue)), 25, 33, 0);
        ClickableSpan clickableSpan = new SpaceAdjust(alreadyHaveAccount) {
            @Override
            public void onClick(View textView) {
                ((Authentication) getActivity()).setCurrentItem(0);
            }
        };
        spannableString.setSpan(clickableSpan, 25, 33,
                Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        alreadyHaveanAccount.setText(spannableString);
        alreadyHaveanAccount.setMovementMethod(LinkMovementMethod
                .getInstance());

    }


    /**
     * Request for product list with specified category id
     */
    private void serverRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            Map<String, String> registerParams = new HashMap<>();
            registerParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            registerParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            registerParams.put(Constant.Request.ACTION, Constant.Request.REGISTER_ACTION);
            registerParams.put(Constant.Request.GROUP_ID, Constant.Request.GROUP_ID_VALUE);
            registerParams.put(Constant.Request.EMAIL, emailText);
            registerParams.put(Constant.Request.SIGNIN_SIGNATURE, passwordText);
            registerParams.put(Constant.Request.FIRST_NAME, firstNameText);
            registerParams.put(Constant.Request.LAST_NAME, lastNameText);
            registerParams.put(Constant.Request.NEWSLETTER, "1");
            registerParams.put(Constant.Request.DATE_OF_BIRTH, dateOfBirthText);
            new RestClient(mContext).getInstance().get().getProfile(registerParams, new RestCallback<LoginResponse>());
        }
    }

    /**
     * method to get the error response from the server
     *
     * @param errorMessage
     */
    @Subscribe
    public void dataReceived(String errorMessage) {
        dismissProgress();
        Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();
        LogUtils.i(TAG, errorMessage);
        BusProvider.getInstance().unregister(this);
    }

    /**
     * method to get the login response from the server
     *
     * @param result
     */
    @Subscribe
    public void dataReceived(LoginResponse result) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {
            if (McommApplication.isSuccess(result.getSuccess())) {
                Login userResult = result.login;
                Utils.showToast(mContext, "Welcome " + userResult.getFirstName(), Toast.LENGTH_SHORT);
                McommApplication.userLogin(getActivity(), userResult);
                getActivity().finish();
            }
            Utils.showToast(mContext, result.getMessage(), Toast.LENGTH_SHORT);
        } else {
            LogUtils.i(TAG, result.getMessage());
        }

    }

    /**
     * method to perform the onclick event
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.registerButton:
                               /*check whether the edit texts are validated
                if true then go to server request*/
                if (validate()) {
                    serverRequest();
                }
                break;
            case R.id.dateOfBirth:
                Utils.getDatePickerDialog(getActivity(), datePickerListener,
                        birthDayValue, birthMonthValue, birthYearValue);
                break;

            default:
                break;
        }
    }

    /**
     * method to perform the validation
     */
    private boolean validate() {
        boolean success = false;
        firstNameText = firstName.getText().toString();
        lastNameText = lastName.getText().toString();
        emailText = email.getText().toString();
        passwordText = password.getText().toString();
        dateInput = dateOfBirth.getText().toString();

        try {
            dateOfBirthText = dateInput;
            SimpleDateFormat inputFormat = new SimpleDateFormat(
                    Constant.YYYY_MM_DD);
            Date date = inputFormat.parse(dateInput);
            SimpleDateFormat outFormat = new SimpleDateFormat(
                    Constant.MM_DD_YYYY);
            dateOfBirthText = outFormat.format(date);
        } catch (Exception e) {
            LogUtils.e(Constant.TAG_EXCEPTION, e.getMessage());
        }


        if (TextUtils.isEmpty(firstNameText.trim())) {
            firstName.setError(getString(R.string.firstName_error));
            firstName.requestFocus();
        } else if (TextUtils.isEmpty(lastNameText.trim())) {
            lastName.setError(getString(R.string.lastName_error));
            lastName.requestFocus();
        } else if (TextUtils.isEmpty(emailText.trim())) {
            email.setError(getString(R.string.EmailSignIn_error));
            email.requestFocus();
        } else if (!Utils.validateEmail(emailText)) {
            email.setError(getString(R.string.Email_incorrect));
            email.requestFocus();
        } else if (TextUtils.isEmpty(passwordText.trim())) {
            password.setError(getString(R.string.PasswordSignIn_error));
            password.requestFocus();
        } else if (password.length() < Constant.MINIMUM_PASSWORD_LENTH || password.length() > Constant.MAXIMUM_PASSWORD_LENTH) {
            password.setError(getString(R.string.alert_passwordMandatory));
        } else if (TextUtils.isEmpty(dateInput.trim())) {
            dateOfBirth.setError(getString(R.string.DOB_empty_error));
            dateOfBirth.requestFocus();
        } else {
            success = true;
        }
        return success;
    }

    /**
     * method to dismiss the progress
     */
    private void dismissProgress() {
        progress.dismissProgress(pDialog);
    }
}
