/**
 * ReviewListFragment.java
 * <p/>
 * This is the fragment view for Listing the products review and rating.
 *
 * @category Contus
 * @package com.contus.mcomm.fragments
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.model.ProductDetails;
import com.contus.mcomm.model.Rating;
import com.contus.mcomm.model.ReviewDetails;
import com.contus.mcomm.model.ReviewListResponse;
import com.contus.mcomm.model.ReviewResponse;
import com.contus.mcomm.model.Reviews;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * <p/>
 * Use the {@link com.contus.mcomm.fragments.ReviewListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReviewListFragment extends Fragment implements View.OnClickListener, SwipyRefreshLayout.OnRefreshListener, SlidingUpPanelLayout.PanelSlideListener {

    private static final String TAG = LogUtils.makeLogTag(ReviewListFragment.class);
    private Context mContext;
    private ProductDetails mProductDetails;
    private TextView tvRatingCount, tvProductName, tvFinalPrice, tvCurrency, tvOfferPrice,
            tvOriginalPrice, tvReviewCount, tvReviewTitle, tvReviewBy, tvReviewDesc, tvProductReviewTitle;
    private LinearLayout linReviewView, linRatingView;
    private View rootView, reviewView, ratingView;
    private LayoutInflater mInflater;
    private RatingBar ratPreview, ratWrite;
    private List<Reviews> reviewList;
    private String totalReview, title, description,currency;
    private EditText edtTitle, edtComment;
    private SlidingUpPanelLayout mLayout;
    private Button btnWriteReview, btnPostReview;
    private Dialog pDialog;
    private Progress progress;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private int page = 1, totalCount = 0;

    public ReviewListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProductListFragment.
     */
    public static ReviewListFragment newInstance(ProductDetails productDetails) {
        ReviewListFragment fragment = new ReviewListFragment();
        fragment.setProductinfo(productDetails);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.mContext = getActivity();
        rootView = inflater.inflate(R.layout.fragment_review, container, false);
        mInflater = LayoutInflater.from(mContext);
        currency=McommApplication.getCurrencySymbol(mContext)+Constant.SINGLE_SPACE;
        init();
        return rootView;
    }

    private void reviewRequest() {
        BusProvider.getInstance().register(this);
        String productId = String.valueOf(mProductDetails.getProductId());
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            new RestClient(mContext).getInstance().get().getReview(McommApplication.getStoreId(mContext),
                    Constant.WEBSITE_ID_VALUE, Constant.Request.REVIEW_LIST_ACTION, productId, page, Constant.REVIEW_LIMIT_COUNT,
                    new RestCallback<ReviewListResponse>());
        }
    }


    @Subscribe
    public void dataReceived(ReviewListResponse response) {
        mSwipyRefreshLayout.setRefreshing(false);
        BusProvider.getInstance().unregister(this);
        if (McommApplication.checkResponse(response.getError(), response.getSuccess())) {
            if (McommApplication.isSuccess(response.getSuccess())) {
                List<Reviews> reviews = response.result.getReviews();
                setReviewData(reviews);
            }
        } else {
            LogUtils.i(TAG, response.getMessage());
        }
    }

    private void setReviewData(List<Reviews> reviewsList) {
        int reviewCount = reviewsList.size();
        if (!reviewsList.isEmpty()) {
            for (int i = 0; i < reviewCount; i++) {
                addReview(reviewsList.get(i), false);
            }
        }
    }


    private void setProductinfo(ProductDetails productDetails) {
        this.mProductDetails = productDetails;
    }

    /**
     * Method that is used to generate views.
     */
    private void init() {
        mSwipyRefreshLayout = (SwipyRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        tvReviewCount = (TextView) rootView.findViewById(R.id.reviewCount);
        tvProductName = (TextView) rootView.findViewById(R.id.productTitle);
        tvProductReviewTitle = (TextView) rootView.findViewById(R.id.productReviewTitle);
        tvOfferPrice = (TextView) rootView.findViewById(R.id.offerPrice);
        tvCurrency = (TextView) rootView.findViewById(R.id.currency);
        tvOriginalPrice = (TextView) rootView.findViewById(R.id.originalPrice);
        tvFinalPrice = (TextView) rootView.findViewById(R.id.finalPrice);
        tvRatingCount = (TextView) rootView.findViewById(R.id.ratingCount);
        linRatingView = (LinearLayout) rootView.findViewById(R.id.ratingListView);
        linReviewView = (LinearLayout) rootView.findViewById(R.id.reviewListView);
        mLayout = (SlidingUpPanelLayout) rootView.findViewById(R.id.sliding_layout);
        btnWriteReview = (Button) rootView.findViewById(R.id.reviewButton);
        btnWriteReview.setOnClickListener(this);
        btnPostReview = (Button) rootView.findViewById(R.id.write_review);
        btnPostReview.setOnClickListener(this);
        edtTitle = (EditText) rootView.findViewById(R.id.title);
        edtComment = (EditText) rootView.findViewById(R.id.comment);
        ratWrite = (RatingBar) rootView.findViewById(R.id.userRating);
        mLayout.setPanelSlideListener(this);

        setData();
    }

    /**
     * Method that is used to set the value to corresponding views.
     */
    private void setData() {
        totalReview = mProductDetails.getReviewsCount();
        totalCount = Integer.parseInt(totalReview);
        tvProductName.setText(mProductDetails.getName());
        tvProductReviewTitle.setText(mProductDetails.getName());
        tvOriginalPrice.setText(currency+mProductDetails.getRegularPrice());
        tvFinalPrice.setText(currency+mProductDetails.getFinalPrice());
       /// tvCurrency.setText(McommApplication.getCurrencySymbol(mContext));
        tvReviewCount.setText("(" + totalReview + ")");
        tvRatingCount.setText(totalReview);
        setDiscountPrice(mProductDetails.getRegularPrice(), mProductDetails.getFinalPrice());
        setReview();

        if (Integer.parseInt(mProductDetails.getReviewsCount()) == 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                }
            }, 10);

        } else {
            setRating(mProductDetails.getRatings(), mProductDetails.getReviewsCount());
        }

    }

    /**
     * Sets the rating value of the view.
     *
     * @param ratings     rating object
     * @param reviewCount total review count
     */
    private void setRating(List<Rating> ratings, String reviewCount) {

        linRatingView.removeAllViews();
        for (int i = 4; i >= 0; i--) {
            ratingView = mInflater.inflate(R.layout.rating_progress,
                    null);
            TextView tvRatingUserCount = (TextView) ratingView.findViewById(R.id.ratingCount);
            TextView tvRatingStars = (TextView) ratingView.findViewById(R.id.ratingText);
            tvRatingStars.setText(ratings.get(i).getStar() + Constant.SINGLE_SPACE + getString(R.string.stars));
            tvRatingUserCount.setText(ratings.get(i).getCount());
            SeekBar seekBar = (SeekBar) ratingView.findViewById(R.id.ratingBar);
            seekBar.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true;
                }
            });

            seekBar.setMax(Integer.parseInt(reviewCount));
            seekBar.setProgress(Integer.parseInt(ratings.get(i).getCount()));
            linRatingView.addView(ratingView);

        }
    }

    /**
     * Sets the review value of the view.
     */
    private void setReview() {
        reviewList = mProductDetails.getReviews();
        Reviews reviewItem;
        int reviewListCount = reviewList.size();
        if (!reviewList.isEmpty()) {
            for (int i = 0; i < reviewListCount; i++) {
                reviewItem = reviewList.get(i);
                addReview(reviewItem, false);
            }
        }
    }

    private void addReview(Reviews reviewItem, boolean newReview) {
        reviewView = mInflater.inflate(R.layout.row_review_item, null);
        tvReviewTitle = (TextView) reviewView.findViewById(R.id.reviewTitle);
        tvReviewBy = (TextView) reviewView.findViewById(R.id.reviewBy);
        tvReviewDesc = (TextView) reviewView.findViewById(R.id.reviewDescription);
        ratPreview = (RatingBar) reviewView.findViewById(R.id.reviewRating);
        tvReviewBy.setText(reviewItem.getDate()+ Constant.SINGLE_SPACE + getString(R.string.by) + Constant.SINGLE_SPACE  + reviewItem.getAuthor());
        tvReviewTitle.setText(reviewItem.getTitle());
        tvReviewDesc.setText(reviewItem.getDetail());
        ratPreview.setRating(Float.parseFloat(reviewItem.getRatingCount()));
        if (newReview)
            linReviewView.addView(reviewView, 0);
        else
            linReviewView.addView(reviewView);
    }


    private void writeReviewRequest() {
        McommApplication application = new McommApplication();
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_USER_LOGIN_TOAST)) {
            BusProvider.getInstance().register(this);
            progress = new Progress(mContext);
            pDialog = progress.showProgress();

            String name = McommApplication.getUserName(mContext);

            String productId = String.valueOf(mProductDetails.getProductId());

            BusProvider.getInstance().register(this);
            Map<String, String> addCartParams = new HashMap<>();

            addCartParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            addCartParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            addCartParams.put(Constant.Request.ACTION, Constant.Request.ADD_REVIEW_ACTION);
            addCartParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            addCartParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));

            addCartParams.put(Constant.Request.PRODUCT_ID, productId);
            addCartParams.put(Constant.Request.REVIEW_NAME, name);
            addCartParams.put(Constant.Request.REVIEW_TITLE, title);
            addCartParams.put(Constant.Request.REVIEW_DESCRIPTION, description);
            addCartParams.put(Constant.Request.REVIEW_RATING, String.valueOf(ratWrite.getRating()));
            addCartParams.put(Constant.Request.REVIEW_STATUS, Constant.DEFAULT_REVIEW_STATUS);
            new RestClient(mContext).getInstance().get().addReview(addCartParams, new RestCallback<ReviewResponse>());
        }

    }

    @Subscribe
    public void dataReceived(ReviewResponse response) {
        BusProvider.getInstance().unregister(this);
        dismissProgress();
        if (McommApplication.checkResponse(response.getError(), response.getSuccess())) {
            Utils.showToast(mContext, response.getMessage(), Toast.LENGTH_LONG);
            if (McommApplication.isSuccess(response.getSuccess())) {
                mLayout.setEnabled(true);
                mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

                if (Integer.parseInt(response.result.getReviewStatus()) == 1) {
                    Reviews reviewItem = new Reviews();
                    reviewItem.setTitle(title);
                    reviewItem.setAuthor(McommApplication.getUserName(mContext));
                    reviewItem.setRatingCount(String.valueOf(ratWrite.getRating()));
                    reviewItem.setDetail(description);
                    List<Reviews> review = mProductDetails.getReviews();
                    review.add(0, reviewItem);
                    mProductDetails.setReviews(review);
                    addReview(reviewItem, true);
                    setUpdateReview(response.result);
                }
            }
        } else {
            LogUtils.i(TAG, response.getMessage());
        }
    }

    private void setUpdateReview(ReviewDetails result) {
        String reviewCount = result.getReviewsCount();
        mProductDetails.setReviewsCount(reviewCount);
        tvReviewCount.setText("(" + reviewCount + ")");
        tvRatingCount.setText(reviewCount);
        setRating(result.getRatings(), reviewCount);
    }


    @Subscribe
    public void dataReceived(String errorMessage) {
        mSwipyRefreshLayout.setRefreshing(false);
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        if (errorMessage.equalsIgnoreCase(Constant.UNAUTHORIZED_VALUE)) {
            McommApplication.redirectAuthentication(mContext);
        } else {
            Utils.showToast(mContext, errorMessage, Toast.LENGTH_SHORT);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.reviewButton:
                if (mLayout != null  && McommApplication.isNetandLogin(mContext,Constant.CHECK_INTERNET_LOGIN_REDIRECT)) {
                    if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.COLLAPSED)
                        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                    else if (mLayout.getPanelState() == SlidingUpPanelLayout.PanelState.EXPANDED)
                        mLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
                break;
            case R.id.write_review:
                validateReview();
                break;
            default:
                break;
        }
    }

    private void validateReview() {
        edtTitle.setError(null);
        edtComment.setError(null);
        title = edtTitle.getText().toString().trim();
        description = edtComment.getText().toString().trim();
        if (ratWrite.getRating() > 0) {
            if (!TextUtils.isEmpty(title)) {
                if (!TextUtils.isEmpty(description)) {
                    writeReviewRequest();
                } else {
                    edtComment.setError(getString(R.string.comment_error));
                }
            } else {
                edtTitle.setError(getString(R.string.title_error));
            }
        } else {
            Utils.showToast(mContext, getString(R.string.rating_error), Toast.LENGTH_LONG);
        }
    }

    /**
     * Set the product discount price.
     *
     * @param actualPrice     product price
     * @param discountedPrice product discount price;
     */
    protected void setDiscountPrice(String actualPrice, String discountedPrice) {
        try {
            int percentage = Utils.calculatePercentage(Float.parseFloat(actualPrice), Float.parseFloat(discountedPrice));
            if (percentage > 0) {
                tvOfferPrice.setVisibility(View.VISIBLE);
                tvOfferPrice.setText(String.valueOf(percentage) + getString(R.string.percentage_off));
                tvOriginalPrice.setPaintFlags(tvOriginalPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                tvOfferPrice.setVisibility(View.GONE);
                tvOriginalPrice.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            LogUtils.i(TAG, e.getMessage());
        }
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private void dismissProgress() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection swipyRefreshLayoutDirection) {
        if (totalCount <= page * Constant.LIMIT_COUNT) {
            Utils.showToast(mContext, getString(R.string.no_reviews), Toast.LENGTH_LONG);
            mSwipyRefreshLayout.setRefreshing(false);
            mSwipyRefreshLayout.setEnabled(false);
        } else {
            page = page + 1;
            reviewRequest();
        }
    }

    @Override
    public void onPanelSlide(View view, float v) {
        Log.i(TAG, "onPanelSlide, offset " + view);
    }

    @Override
    public void onPanelCollapsed(View view) {
        if (btnWriteReview.getAnimation() != null) {
            btnWriteReview.getAnimation().cancel();
            btnWriteReview.setAnimation(null);
            btnWriteReview.setVisibility(View.VISIBLE);
            btnPostReview.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onPanelExpanded(View view) {
        if (Integer.parseInt(mProductDetails.getReviewsCount()) == 0) {
            mLayout.setEnabled(false);
        }
        if (btnWriteReview.getAnimation() == null) {
            TranslateAnimation animation = new TranslateAnimation(0, btnPostReview.getX() - btnWriteReview.getX(), 0, btnPostReview.getY() + dpToPx(56) - btnWriteReview.getY());
            animation.setRepeatMode(0);
            animation.setDuration(1000);
            animation.setFillAfter(true);
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    //onAnimationStart
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    btnPostReview.setVisibility(View.VISIBLE);
                    btnWriteReview.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                    // onAnimationRepeat
                }
            });
            btnWriteReview.startAnimation(animation);
        }
    }

    @Override
    public void onPanelAnchored(View view) {
        Log.i(TAG, "onPanelAnchored");
    }

    @Override
    public void onPanelHidden(View view) {
        Log.i(TAG, "onPanelHidden");
    }
}
