/**
 * CategoryListFragment.java
 * <p/>
 * This is the fragment view for Category List.
 *
 * @category Contus
 * @package com.contus.mcomm.fragments
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.adapters.CategoryAdapter;
import com.contus.mcomm.adapters.ProductListAdapter;
import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.database.CategoryExecutor;
import com.contus.mcomm.externallibraries.HorizontalListView;
import com.contus.mcomm.model.Category;
import com.contus.mcomm.model.Product;
import com.contus.mcomm.model.ProductResponse;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CategoryListFragment extends Fragment implements AdapterView.OnItemClickListener, View.OnClickListener {
    private static final String TAG = LogUtils.makeLogTag(CategoryListFragment.class);
    List<Product> productList;
    private ListView categoryListView;
    private TextView tvCategoryName;
    private OnCategoryListInteractionListeners mListener;
    private Context mContext;
    private List<Category> categoryList;
    private CategoryAdapter categoryAdapter;
    private int selectedCatId;
    private String selectedCatName;
    private HorizontalListView productView;
    private RelativeLayout productLayout;
    private TextView moreProduct;
    private int totalCount;
    private View categoryFooterView;

    public CategoryListFragment() {
    }

    public static CategoryListFragment newInstance(Bundle args) {
        CategoryListFragment fragment = new CategoryListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            selectedCatId = Integer.parseInt(bundle.getString(Constant.SELECTED_ID));
            selectedCatName = bundle.getString(Constant.SELECTED_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.mContext = getActivity();
        View view = inflater.inflate(R.layout.list_fragment_category, container, false);
        categoryListView = (ListView) view.findViewById(R.id.listview);
        tvCategoryName = (TextView) view.findViewById(R.id.categoryName);

        categoryListView.setBackgroundColor(Color.WHITE);
        tvCategoryName.setText(selectedCatName);

        categoryFooterView = inflater.inflate(R.layout.list_category_product, null);
        productView = (HorizontalListView) categoryFooterView.findViewById(R.id.product_view);
        productLayout = (RelativeLayout) categoryFooterView.findViewById(R.id.product_layout);
        moreProduct = (TextView) categoryFooterView.findViewById(R.id.more_product);
        moreProduct.setOnClickListener(this);
        categoryListView.addFooterView(categoryFooterView);
        serverRequest();
        setAdapter();
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (OnCategoryListInteractionListeners) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCategoryListInteractionListeners");
        }
    }

    /**
     * Method to set the category data to the view.
     */
    private void setAdapter() {
        categoryList = new CategoryExecutor(mContext).getCategoryById(selectedCatId);
        categoryAdapter = new CategoryAdapter(mContext, categoryList, CategoryAdapter.LIST_TYPE.SUB_CATEGORY);
        categoryListView.setAdapter(categoryAdapter);
        categoryListView.setOnItemClickListener(this);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.listview && McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            Category categoryItem = categoryList.get(position);
            if (mListener != null)
                mListener.onCategoryInteractions(categoryItem);
        }
    }

    /**
     * Method to remove the category footer view when the parent category product is empty.
     */
    private void removeFooterView() {
        productLayout.setVisibility(View.GONE);
        categoryListView.removeFooterView(categoryFooterView);
    }

    /**
     * Method to set the category product data to the view.
     */
    private void setProductAdapter() {
        if (totalCount > 0) {
            if (totalCount > Constant.LIMIT_COUNT) {
                moreProduct.setVisibility(View.VISIBLE);
            } else {
                moreProduct.setVisibility(View.GONE);
            }
            productLayout.setVisibility(View.VISIBLE);
            ProductListAdapter productListAdapter = new ProductListAdapter(getActivity(), productList, ProductListAdapter.LIST_TYPE.NEW_PRODUCT);
            productView.setAdapter(productListAdapter);
            productView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
                        mListener.onProductsListInteraction("ProductList", null, productList.get(position));
                    }
                }
            });
        } else {
            removeFooterView();
        }
    }

    /**
     * Method to perform onclick event
     */
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.more_product && mListener != null) {
            mListener.onProductInteractions(selectedCatId);
        }
    }

    /**
     * Request for product list with specified category id
     */
    private void serverRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            BusProvider.getInstance().register(this);
            Map<String, String> productListParams = new HashMap<>();
            productListParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            productListParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            productListParams.put(Constant.Request.ACTION, Constant.Request.CATEGORY_PRODUCT_ACTION);
            productListParams.put(Constant.Request.CUSTOMER_ID, McommApplication.getCustomerId(mContext));
            productListParams.put(Constant.Request.CATEGORY_ID, String.valueOf(selectedCatId));
            productListParams.put(Constant.Request.PAGE, String.valueOf(Constant.PAGE_VALUE));
            productListParams.put(Constant.Request.LIMIT, String.valueOf(Constant.LIMIT_COUNT));

            new RestClient(mContext).getInstance().get().getProductList(productListParams, new RestCallback<ProductResponse>());
        }
    }

    /**
     * method to get the error response from the server
     *
     * @param errorMessage
     */
    @Subscribe
    public void dataReceived(String errorMessage) {
        BusProvider.getInstance().unregister(this);
        if (errorMessage.equalsIgnoreCase(Constant.UNAUTHORIZED_VALUE)) {
            McommApplication.redirectAuthentication(mContext);
        } else {
            Utils.showToast(mContext, errorMessage, Toast.LENGTH_SHORT);
        }
        removeFooterView();
    }

    /**
     * method to get the product response from the server
     *
     * @param result
     */
    @Subscribe
    public void dataReceived(ProductResponse result) {
        BusProvider.getInstance().unregister(this);
        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {
            totalCount = Integer.parseInt(result.totalCount);
            if (McommApplication.isSuccess(result.getSuccess())) {
                productList = result.products;
            }
            setProductAdapter();
        } else {
            LogUtils.i(TAG, result.getMessage());
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface OnCategoryListInteractionListeners {
        public void onCategoryInteractions(Category category);

        public void onProductInteractions(int categoryId);

        public void onProductsListInteraction(String type, String id, Product productItem);
    }
}
