package com.contus.mcomm.fragments;
/**
 * ProductDetailFragment.java
 * <p/>
 * This is the fragment view for Product detail.
 *
 * @category Contus
 * @package com.contus.mcomm.fragments
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.adapters.ProductImageAdapter;
import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.database.AttributeExecutor;
import com.contus.mcomm.database.WishlistdbExecutor;
import com.contus.mcomm.externallibraries.HorizontalListView;
import com.contus.mcomm.externallibraries.PagerSlidingTabStrip;
import com.contus.mcomm.model.CartResponse;
import com.contus.mcomm.model.Product;
import com.contus.mcomm.model.ProductDetailResponse;
import com.contus.mcomm.model.ProductDetails;
import com.contus.mcomm.model.WishlistResponse;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.NavigationActivity;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.Currency;


public class ProductDetailsFragment extends ProductDetailsOnClickHandlers {


    public ProductDetailsFragment() {

    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param item Id of the product.
     * @return A new instance of fragment ProductDetailFragment.
     */
    public static ProductDetailsFragment newInstance(Product item) {

        ProductDetailsFragment fragment = new ProductDetailsFragment();
        fragment.setInitialCollection(item);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setWidth();
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_product_detail, container, false);
        this.mContext = getActivity();
        this.application = new McommApplication();
        mPicasso = Picasso.with(mContext);
        arrtibuteExecutor = new AttributeExecutor(mContext);
        currency = application.getCurrencySymbol(mContext)+Constant.SINGLE_SPACE;
        serverRequest();
        init();
        return rootView;

    }

    /**
     * Method that is used to set the intial collection.
     */
    private void setInitialCollection(Product item) {
        this.productItem = item;
    }

    /**
     * Method that is used to generate views.
     *
     * @return null;
     */
    private void init() {
        tvCartCount = (TextView) rootView.findViewById(R.id.cartCount);
        tvProductName = (TextView) rootView.findViewById(R.id.productName);
        tvOfferPrice = (TextView) rootView.findViewById(R.id.offer_price);
        tvActualPrice = (TextView) rootView.findViewById(R.id.actual_price);
        tvProductDesc = (TextView) rootView.findViewById(R.id.product_description);
        tvRatings = (TextView) rootView.findViewById(R.id.ratings);
        tvReview = (TextView) rootView.findViewById(R.id.view_review);
        ratProductReview = (RatingBar) rootView.findViewById(R.id.reviewallrating);
        tvOffer = (TextView) rootView.findViewById(R.id.offer);
        ivProductImage = (ImageView) rootView.findViewById(R.id.product_main_image);
        tvStock = (TextView) rootView.findViewById(R.id.stock_text);
        tvReadMore = (TextView) rootView.findViewById(R.id.read_more);
        ivShare = (ImageView) rootView.findViewById(R.id.product_share_icon);
        relDescription = (RelativeLayout) rootView.findViewById(R.id.product_description_layout);
        tvDelete = (TextView) rootView.findViewById(R.id.delete_options);
        ivWishList = (ImageView) rootView.findViewById(R.id.product_wishlist);
        btnBuyNow = (Button) rootView.findViewById(R.id.buy_now_button);
        btnBuyNow.setOnClickListener(this);
        cartHolder = (LinearLayout) rootView.findViewById(R.id.cart_holder);
        cartHolder.setOnClickListener(this);
        buyNowHolder = (LinearLayout) rootView.findViewById(R.id.buynow_holder);
        buyNowHolder.setOnClickListener(this);

        imagesList = (HorizontalListView) rootView.findViewById(R.id.product_images);
        addToCart = (Button) rootView.findViewById(R.id.add_cart);
        tvProductName.setText(productItem.getName());
        tvOfferPrice.setText(currency+productItem.getSpecialPrice());
        setDiscountPrice(productItem.getPrice(), productItem.getSpecialPrice());
        String imgUrl = productItem.getImageName();
        setMainImage(imgUrl);

        productId = productItem.getProductId();

    }


    /**
     * Method that is used to set listener for generated views.
     *
     * @return null;
     */
    private void setListener() {
        tvReview.setOnClickListener(this);
        ivProductImage.setOnClickListener(this);
        tvReadMore.setOnClickListener(this);
        relDescription.setOnClickListener(this);
        imagesList.setOnItemClickListener(this);
        addToCart.setOnClickListener(this);
        ivShare.setOnClickListener(this);
        tvDelete.setOnClickListener(this);
        ivWishList.setOnClickListener(this);
    }

    /**
     * method to get the error response from the server
     *
     * @param errorMessage
     */
    @Subscribe
    public void dataReceived(String errorMessage) {
        dismissProgress();
        LogUtils.i(TAG, errorMessage);
        BusProvider.getInstance().unregister(this);
        if (errorMessage.equalsIgnoreCase(Constant.UNAUTHORIZED_VALUE)) {
            McommApplication.redirectAuthentication(mContext);
        } else {
            Utils.showToast(mContext, errorMessage, Toast.LENGTH_SHORT);
        }
    }

    /**
     * method to get the product detail response from the server
     *
     * @param response
     */
    @Subscribe
    public void dataReceived(ProductDetailResponse response) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        if (McommApplication.checkResponse(response.getError(), response.getSuccess())) {

            if (McommApplication.isSuccess(response.getSuccess())) {
                ProductDetails result = response.product;
                productCollection = result;
                setProductDeatils(result);
            } else if (interation == 0) {
                serverRequest();
                interation++;
            }
        } else {
            LogUtils.i(TAG, response.getMessage());
        }
    }

    /**
     * method to set the product details to the view objects
     */
    private void setProductDeatils(ProductDetails result) {

        tvProductName.setText(result.getName());
        tvOfferPrice.setText(currency+result.getFinalPrice());
        tvActualPrice.setText(currency+result.getRegularPrice());
        tvRatings.setText("(" + result.getReviewsCount() + ")");
        ratProductReview.setRating(Float.parseFloat(result.getRating()));
        setDiscountPrice(result.getRegularPrice(), result.getFinalPrice());
        String imgUrl = result.getImageName();
        setMainImage(imgUrl);
        productImages = result.getImages();
        productImages.add(imgUrl);
        //check whether the product image size is less than 2
        if (productImages.size() < 2) {
            imagesList.setVisibility(View.GONE);
        } else {
            imagesList.setVisibility(View.VISIBLE);
            imageAdapter = new ProductImageAdapter(getActivity(), productImages, imgPosition, ProductImageAdapter.LIST_TYPE.IMAGES_LIST);
            imagesList.setAdapter(imageAdapter);
        }
        if (result.getProductType().equalsIgnoreCase(Constant.TYPE_CONFIGURABLE)) {
            rootView.findViewById(R.id.config_holder).setVisibility(View.VISIBLE);
            setCustomOption(result.getProductId(), result.getOptions());
        } else {
            rootView.findViewById(R.id.config_holder).setVisibility(View.GONE);
        }
        setWishlist(result.getIsWishlist());
        if ("0".equals(result.getReviewsCount())) {
            tvReview.setText(getResources().getString(R.string.first_review));
        } else {
            tvReview.setText(getResources().getString(R.string.see_reviews));
        }
        setDescription(result.getDescription(), result.getShortDescription());
        setStock(productCollection.getIsSaleable());
        setListener();

    }

    /**
     * method to set the wishlist
     */
    private void setWishlist(Boolean isWishlist) {
        //check whether the isWishlist is true or not
        if (isWishlist)
            ivWishList.setImageResource(R.drawable.ic_wishlist_active);
        else
            ivWishList.setImageResource(R.drawable.ic_wishlist_inactive);
    }

    /**
     * method to get the cart response from the server
     *
     * @param response
     */
    @Subscribe
    public void dataReceived(CartResponse response) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);

        if (McommApplication.checkResponse(response.getError(), response.getSuccess())) {
            Utils.showToast(mContext, response.getMessage(), Toast.LENGTH_LONG);
            if (McommApplication.isSuccess(response.getSuccess())) {
                String itemCount = response.getItemCount();
                Utils.savePreferences(getActivity(), Constant.PREF_CART_ITEM_COUNT, itemCount);
                if (mListener != null)
                    mListener.onCartUpdateInteraction();
            }
        } else {
            LogUtils.i(TAG, response.getMessage());
        }
        if (isBuyNow) {
            Intent mIntent = new Intent(mContext, NavigationActivity.class);
            mIntent.putExtra(Constant.NAVIGATION_TYPE, Constant.TYPE_CART);
            startActivity(mIntent);
        }
    }

    /**
     * method to get the wishlist response from the server
     *
     * @param response
     */
    @Subscribe
    public void dataReceived(WishlistResponse response) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        if (McommApplication.checkResponse(response.getError(), response.getSuccess())) {
            Utils.showToast(mContext, response.getMessage(), Toast.LENGTH_SHORT);
            if (productCollection.getIsWishlist()) {
                productCollection.setIsWishlist(false);
                ivWishList.setImageResource(R.drawable.ic_wishlist_inactive);
            } else {
                productCollection.setIsWishlist(true);
                ivWishList.setImageResource(R.drawable.ic_wishlist_active);
            }

            if (mListener != null)
                mListener.onAddWishlistInteraction(true);
            new WishlistdbExecutor(mContext).updateWishlist(String.valueOf(productCollection.getProductId()), productCollection.getIsWishlist());

        } else {
            LogUtils.i(TAG, response.getMessage());
        }

    }

    /**
     * method to perform the function when the back key is pressed
     */
    public void backPressed(boolean wishListChanged) {
//check whether the wish list is changed
        if (wishListChanged) {
            mListener.onAddWishlistInteraction(true);
            setWishlist(new WishlistdbExecutor(mContext).getWishlistbyId(productCollection.getProductId()));
        }
    }

    /**
     * Method that is used to setup Tabstrip Control.
     *
     * @return null;
     */
    private void setWidth() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        PagerSlidingTabStrip.width = width / 2;
    }

    /**
     * method to dismiss the progress bar
     */
    private void dismissProgress() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    /**
     * method to set the configurable options
     *
     * @param position
     * @param code
     * @param label
     */
    public void setConfigurableOptions(int position, String code, String label) {
        customOptions[position] = code;
        if (label.trim().length() > 2)
            resCode[position] = label.substring(0, 2);
        else
            resCode[position] = label;
        adapter.notifyDataSetChanged();
        tabs.notifyDataSetChanged();
        tvDelete.setVisibility(View.VISIBLE);

    }
}