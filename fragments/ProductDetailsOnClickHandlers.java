package com.contus.mcomm.fragments;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;

import com.contus.mcomm.utils.Constant;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.NavigationActivity;
import com.contussupport.ecommerce.R;

import java.util.ArrayList;


/**
 * Created by user on 5/9/2015.
 */
public abstract class ProductDetailsOnClickHandlers extends ProductDetailsFragmentImpl implements View.OnClickListener, AdapterView.OnItemClickListener {


    @Override
    public void onClick(View v) {
        int viewId = v.getId();
        switch (viewId) {
            case R.id.cart_holder:
            case R.id.add_cart:
                isBuyNow = false;
                addCartRequest(productId);
                break;
            case R.id.buynow_holder:
            case R.id.buy_now_button:
                isBuyNow = true;
                addCartRequest(productId);
                break;
            case R.id.view_review:
                if (mListener != null && !productCollection.getReviewsCount().isEmpty()) {
                    mListener.onProductsDetailInteraction(productCollection);
                }
                break;
            case R.id.delete_options:
                customOptions[currentPosition] = "";
                adapter.notifyDataSetChanged();
                tabs.notifyDataSetChanged();
                tvDelete.setVisibility(View.INVISIBLE);
                registeredFragments.get(currentPosition).removeSelection();
                break;
            case R.id.product_wishlist:
                if (McommApplication.isNetandLogin(mContext, Constant.CHECK_USER_LOGIN_TOAST)) {
                    if (productCollection.getIsWishlist())
                        deleteWishlistRequest(String.valueOf(productCollection.getProductId()));
                    else
                        addWishlistRequest(String.valueOf(productCollection.getProductId()));
                }
                break;
            default:
                setDefaultOption(viewId);
                break;
        }
    }

    private void setDefaultOption(int viewId) {
        switch (viewId) {
            case R.id.product_main_image:
                if (!productImages.isEmpty()) {
                    Intent imgActivity = new Intent(getActivity(), NavigationActivity.class);
                    imgActivity.putExtra(Constant.IMG_URLS, (ArrayList<String>) productImages);
                    imgActivity.putExtra(Constant.IMG_POSITION, imgPosition);
                    imgActivity.putExtra(Constant.TITLE, productCollection.getName());
                    imgActivity.putExtra(Constant.NAVIGATION_TYPE, Constant.TYPE_IMAGES);
                    startActivity(imgActivity);
                }
                break;
            case R.id.product_description_layout:
            case R.id.read_more:
                Intent readMore = new Intent(getActivity(), NavigationActivity.class);
                readMore.putExtra(Constant.DESCRIPTION, productCollection.getDescription());
                readMore.putExtra(Constant.NAVIGATION_TYPE, Constant.TYPE_DESCRIPTION);
                readMore.putExtra(Constant.TITLE, productCollection.getName());
                readMore.putExtra(Constant.SINGLE_VIEW, true);
                startActivity(readMore);
                break;
            case R.id.product_share_icon:
                String productUrl = productCollection.getProductUrl().trim();
                if (!TextUtils.isEmpty(productUrl)) {
                    McommApplication.intentShare(mContext, productCollection.getName(), productUrl);
                }
                break;
            default:
                break;
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        int viewId = parent.getId();
        if (viewId == R.id.product_images && !productImages.isEmpty()) {
            imgPosition = position;
            mPicasso.load(productImages.get(position)).placeholder(R.drawable.place_holder).
                    into(ivProductImage);
            imageAdapter.setPosition(imgPosition);
            imageAdapter.notifyDataSetChanged();

        }

    }

}
