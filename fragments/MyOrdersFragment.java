/**
 * MyOrdersFragment.java
 * <p/>
 * This is a view class of my order Page.
 *
 * @category Contus
 * @package contus.com.mcomm
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.adapters.MyAccountAdapter;
import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.model.Order;
import com.contus.mcomm.model.OrderResponse;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.NavigationActivity;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MyOrdersFragment extends Fragment implements AdapterView.OnItemClickListener {
    private static final String TAG = LogUtils.makeLogTag(MyOrdersFragment.class);
    private Context mContext;
    private View rootView;
    private ListView myOrderListView;
    private McommApplication application;
    private List<Order> orderResult;
    private MyAccountAdapter accountAdapter;
    private Order ordr;
    private Dialog pDialog;
    private Progress progress;
    private Intent myOrdrDetails;
    private TextView emptyView;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    public static MyOrdersFragment newInstance() {
        return new MyOrdersFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_order, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = this.getActivity();
        progress = new Progress(mContext);
        application = new McommApplication();
        initViews();
        serverRequest();

    }

    /**
     * method to initialize all the views
     */
    private void initViews() {
        rootView = getView();
        emptyView = (TextView) rootView.findViewById(R.id.emptyView);
        myOrderListView = (ListView) rootView.findViewById(R.id.listview);
        myOrderListView.setDivider(null);
        myOrderListView.setDividerHeight(0);
    }

    /**
     * Request for the order details to list from the server
     */
    private void serverRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            Map<String, String> orderParams = new HashMap<>();
            orderParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            orderParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            orderParams.put(Constant.Request.ACTION, Constant.Request.ORDER_LIST_ACTION);
            orderParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            orderParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            orderParams.put(Constant.Request.PAGE, String.valueOf(Constant.PAGE_VALUE));
            orderParams.put(Constant.Request.LIMIT, String.valueOf(Constant.PROFILE_LIMIT_COUNT));
            new RestClient(mContext).getInstance().get().getOrder(orderParams, new RestCallback<OrderResponse>());
        }
    }

    /**
     * method to get the error response from the server
     *
     * @param errorMessage
     */
    @Subscribe
    public void dataReceived(String errorMessage) {
        LogUtils.i(TAG, errorMessage);
        BusProvider.getInstance().unregister(this);
        if (errorMessage.equalsIgnoreCase(Constant.UNAUTHORIZED_VALUE)) {
            McommApplication.redirectAuthentication(mContext);
        } else {
            Utils.showToast(mContext, errorMessage, Toast.LENGTH_SHORT);
        }
    }

    /**
     * method to get the order response from the server
     *
     * @param result
     */
    @Subscribe
    public void dataReceived(OrderResponse result) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);

        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {
            LogUtils.i(TAG,result.getMessage());
            if (McommApplication.isSuccess(result.getSuccess())) {
                orderResult = result.order;
            }
            setOrderResultAdapter();

        } else {
            LogUtils.i(TAG, result.getMessage());
        }

    }

    /**
     * method to show list view when the list has value
     */
    private void setListView() {
        myOrderListView.setVisibility(View.VISIBLE);
        emptyView.setVisibility(View.GONE);
    }

    /**
     * method to show empty view when the list is empty
     */
    private void setEmptyView() {
        myOrderListView.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        emptyView.setText(getResources().getString(R.string.no_results_orders));
    }

    /**
     * method to set the value to the adapter
     */
    private void setOrderResultAdapter() {

        if (orderResult.isEmpty()) {
            setEmptyView();
        } else {
            setListView();
            accountAdapter = new MyAccountAdapter(mContext, orderResult, MyAccountAdapter.LIST_TYPE.ORDERS);
            myOrderListView.setAdapter(accountAdapter);
            myOrderListView.setOnItemClickListener(this);
        }
    }

    /**
     * method to get the order id from the list
     *
     * @param posOrdr the int
     */
    public void getOrdrId(int posOrdr) {
        ordr = orderResult.get(posOrdr);
        myOrdrDetails = new Intent(mContext, NavigationActivity.class);
        myOrdrDetails.putExtra(Constant.NAVIGATION_TYPE, Constant.TYPE_MY_ORDER_DETAILS);
        myOrdrDetails.putExtra(Constant.Request.ORDER_ID, ordr.getOrderId());
        startActivity(myOrdrDetails);
    }

    /**
     * method to dismiss the progress
     */
    private void dismissProgress() {

        progress.dismissProgress(pDialog);
    }

    /**
     * method to perform on item click event for the list
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getOrdrId(position);
    }
}




