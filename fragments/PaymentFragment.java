package com.contus.mcomm.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.adapters.PaymentAdapter;
import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.model.Checkout;
import com.contus.mcomm.model.Payment;
import com.contus.mcomm.model.PlaceOrderResponse;
import com.contus.mcomm.model.Shipping;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by contus on 4/5/15.
 */
public class PaymentFragment extends Fragment implements View.OnClickListener {

    protected static final String TAG = LogUtils.makeLogTag(PaymentFragment.class);
    ImageView arrow;
    private View rootView;
    private int cashOnDeliveryFlag;
    private Dialog pDialog;
    private Context mContext;
    private McommApplication application;
    private OnPaymentInteractionListener mListener;
    private String currency, totalAmount = "0.0", orderID, currentShippingPrice = "0.0";
    private TextView amountText, codAmount;
    private Checkout paymentCollection;
    private LayoutInflater mInflater;
    private LinearLayout shippingLayout;
    private List<Shipping> shippingItem;
    private String shippingMethod;
    private int selectedPosition;
    private List<Payment> paymentItem;
    private String paymentMethod;
    private ListView paymentListview;
    private Resources res;

    public static PaymentFragment newInstance() {
        return new PaymentFragment();

    }


    public static PaymentFragment newInstance(String item, Checkout collection) {

        PaymentFragment fragment = new PaymentFragment();
        fragment.setInitialCollection(item, collection);
        return fragment;
    }

    private void setInitialCollection(String item, Checkout collection) {
        this.totalAmount = item;
        this.paymentCollection = collection;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_payment, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.application = new McommApplication();
        this.mContext = getActivity();
        mInflater = (LayoutInflater) mContext.getSystemService(mContext.LAYOUT_INFLATER_SERVICE);
        currency = McommApplication.getCurrencySymbol(mContext) + Constant.SINGLE_SPACE;
        res = getResources();
        initViews();

    }

    private void initViews() {
        rootView = getView();
        shippingLayout = (LinearLayout) rootView.findViewById(R.id.shipping_layout);
        amountText = (TextView) rootView.findViewById(R.id.payment_total);
        paymentListview = (ListView) rootView.findViewById(R.id.listview);
        amountText.setText(currency + totalAmount);
        arrow = (ImageView) rootView.findViewById(R.id.deliver_to_arrow);
        arrow.setColorFilter(getResources().getColor(R.color.theme_color));
        setPaymentData();
        shippingItem = paymentCollection.getShipping();
        if (!shippingItem.isEmpty())
            setShippingData();

    }

    private void setShippingData() {
        shippingLayout.removeAllViews();
int count=shippingItem.size();
        int position = 0;
        for (int i = 0; i < count; i++) {
            Shipping items = shippingItem.get(i);
            if (items.getCode().equalsIgnoreCase(Constant.FREE_SHIPPING))
                position = i;
            ViewGroup shippingView = (ViewGroup) mInflater.inflate(R.layout.row_shipping_item,
                    null);
            RelativeLayout rlShippingItem = (RelativeLayout) shippingView.findViewById(R.id.shipping_item);
            TextView txtShippingName = (TextView) shippingView.findViewById(R.id.shipping_name);
            TextView txtShippingPrice = (TextView) shippingView.findViewById(R.id.shipping_price);
            txtShippingName.setText(items.getMethodTitle());
            txtShippingPrice.setText(currency + items.getPrice());
            rlShippingItem.setTag(i);
            rlShippingItem.setOnClickListener(this);
            shippingLayout.addView(shippingView);
        }
        setShippingView(position);

    }

    private void setPaymentData() {

        paymentItem = paymentCollection.getPayment();
        PaymentAdapter adapter = new PaymentAdapter(mContext, paymentItem, PaymentAdapter.LIST_TYPE.PAYMENT_LIST);
        paymentListview.setAdapter(adapter);
        adapter.onClickPaymentListener(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                int firstPosition = paymentListview.getFirstVisiblePosition() - paymentListview.getHeaderViewsCount();
                try {

                    View codView = paymentListview.getChildAt(firstPosition).findViewById(R.id.payment_view);
                    codView.performClick();
                    codView.setVisibility(View.GONE);
                } catch (Exception e) {
                    LogUtils.e(Constant.TAG_EXCEPTION, e.getMessage());
                }
            }
        }, 500);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.shipping_item:
                selectedPosition = Integer.parseInt(view.getTag().toString());
                setShippingView(selectedPosition);
                break;
            case R.id.payment_view:
                selectedPosition = Integer.parseInt(view.getTag().toString());
                View parent = (View) view.getParent().getParent().getParent();
                setPaymentChildData(parent, selectedPosition);
                break;
            case R.id.cash_on_delivery_button:
                placeOrder();
                break;
            case R.id.deliver_to:
                getActivity().getFragmentManager().popBackStack();
                break;
            default:
                break;
        }
    }

    private void setShippingView(int selectedPosition) {
        shippingMethod = shippingItem.get(selectedPosition).getCode();
        int childSize = shippingLayout.getChildCount();
        int padding = shippingLayout.getChildAt(selectedPosition).getPaddingLeft();

        for (int i = 0; i < childSize; i++) {
            shippingLayout.getChildAt(i).setBackgroundColor(Color.WHITE);
        }
        currentShippingPrice = shippingItem.get(selectedPosition).getPrice();
        String amount = currency + McommApplication.round((Double.parseDouble(totalAmount) + Double.parseDouble(currentShippingPrice)), 2);
        amountText.setText(amount);
        if (codAmount != null)
            codAmount.setText(res.getString(R.string.pay) + Constant.SINGLE_SPACE + amount + Constant.SINGLE_SPACE + res.getString(R.string.with_delivery));
        View shippingView = shippingLayout.getChildAt(selectedPosition);

        shippingView.setBackgroundResource(R.drawable.bg_theme);
        shippingView.setPadding(padding, padding, padding, padding);

        shippingView.refreshDrawableState();
    }

    private void setPaymentChildData(View parent, int position) {

        paymentMethod = paymentItem.get(position).getCode();
        if (paymentMethod.equals(Constant.CASHONDELIVERY)) {
            LinearLayout cashLayout = (LinearLayout) parent.findViewById(R.id.child_layout);
            RelativeLayout paymentClick = (RelativeLayout) parent.findViewById(R.id.payment_view);

            if (cashOnDeliveryFlag == 0) {
                paymentClick.setRotation(180);
                cashLayout.setVisibility(View.VISIBLE);
                cashLayout.removeAllViews();
                View view = mInflater.inflate(R.layout.row_payment_cashon, null);
                codAmount = (TextView) view.findViewById(R.id.cod_amount);
                Button paymentButton = (Button) view.findViewById(R.id.cash_on_delivery_button);
                codAmount.setText(res.getString(R.string.pay) + Constant.SINGLE_SPACE + currency + McommApplication.round((Double.parseDouble(totalAmount) + Double.parseDouble(currentShippingPrice)), 2) + Constant.SINGLE_SPACE + res.getString(R.string.with_delivery));
                paymentButton.setOnClickListener(this);
                cashLayout.addView(view);
                cashOnDeliveryFlag = 1;
            } else {
                cashOnDeliveryFlag = 0;
                paymentClick.setRotation(0);
                cashLayout.setVisibility(View.GONE);
            }
        }


    }

    private void placeOrder() {
        Progress progress = new Progress(mContext);
        pDialog = progress.showProgress();
        pDialog.show();
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            BusProvider.getInstance().register(this);
            Map<String, String> placeOrderParams = new HashMap<>();

            placeOrderParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            placeOrderParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            placeOrderParams.put(Constant.Request.ACTION, Constant.Request.PLACE_ORDER_ACTION);
            placeOrderParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            placeOrderParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));

            placeOrderParams.put(Constant.Request.CART_REQ_QUOTE_ID, application.getQuoteId(mContext));
            placeOrderParams.put(Constant.Request.SHIPPING_METHOD, shippingMethod);
            placeOrderParams.put(Constant.Request.PAYMNET_METHOD, paymentMethod);

            new RestClient(mContext).getInstance().get().placeOrder(placeOrderParams, new RestCallback<PlaceOrderResponse>());

        }
    }


    @Subscribe
    public void dataReceived(PlaceOrderResponse response) {
        BusProvider.getInstance().unregister(this);
        pDialog.cancel();
        if (McommApplication.checkResponse(response.getError(), response.getSuccess())) {
            Utils.showToast(mContext, response.getMessage(), Toast.LENGTH_LONG);

            if (McommApplication.isSuccess(response.getSuccess())) {
                orderID = response.getOrderId();
                Utils.savePreferences(getActivity(), Constant.PREF_CART_ITEM_COUNT, "0");
                Utils.savePreferences(getActivity(), Constant.PREF_CART_QUOTE_ID, "");
                onItemSelected();
            }

        } else {
            LogUtils.i(TAG, response.getMessage());
        }
    }

    @Subscribe
    public void dataReceived(String errorMessage) {
        BusProvider.getInstance().unregister(this);
        pDialog.cancel();
        if (errorMessage.equalsIgnoreCase(Constant.UNAUTHORIZED_VALUE)) {
            McommApplication.redirectAuthentication(mContext);
        } else {
            Utils.showToast(mContext, errorMessage, Toast.LENGTH_SHORT);
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnPaymentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void onItemSelected() {
        if (mListener != null) {
            mListener.onPaymentSuccessInteraction(orderID);
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnPaymentInteractionListener {
        public void onPaymentSuccessInteraction(String orderId);
    }
}
