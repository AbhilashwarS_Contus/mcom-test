/**
 * ProductListFragment.java
 * <p/>
 * This is the fragment view for Listing the Products.
 *
 * @category Contus
 * @package com.contus.mcomm.fragments
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.adapters.ProductImageAdapter;
import com.contus.mcomm.adapters.ProductListAdapter;
import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.database.WishlistdbExecutor;
import com.contus.mcomm.model.Product;
import com.contus.mcomm.model.ProductResponse;
import com.contus.mcomm.model.WishlistResponse;
import com.contus.mcomm.model.Wishlistdb;
import com.contus.mcomm.service.DbParser;
import com.contus.mcomm.service.OnParsingCompletionListener;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayout;
import com.orangegangsters.github.swipyrefreshlayout.library.SwipyRefreshLayoutDirection;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnProductListInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProductListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProductListFragment extends Fragment implements SwipyRefreshLayout.OnRefreshListener, View.OnClickListener, OnParsingCompletionListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String TAG = LogUtils.makeLogTag(ProductListFragment.class);
    private static final String ARG_CATEGORYID = "category_ID";
    private static final String ARG_PAGE = "page";
    private ProductListAdapter productListAdapter;
    private TextView emptyView;
    private GridView gridViewProducts;
    private Context mContext;
    private int categoryId;
    private OnProductListInteractionListener mListener;
    private List<Product> productResponse;
    private LinearLayout lineFilterLayout;
    private Dialog pDialog;
    private Progress progress;
    private String sortBy = Constant.SORT_RATING, orderBy = Constant.SORT_ASC;
    private int page = Constant.PAGE_VALUE, sortPosition = 0;
    private SwipyRefreshLayout mSwipyRefreshLayout;
    private int totalCount;
    private McommApplication application;
    private int selectedProduct;
    private List<Product> productList;
    private WishlistdbExecutor wishlistdbExecutor;
    private String selectedPage, searchTerms, filters = "";
    private List<String[]> filterCollections;
    private List<Wishlistdb> wishlistList;
    private Dialog sortDialog;
    private ListView lvSort;

    public ProductListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param categoryId The category id of products.
     * @return A new instance of fragment ProductListFragment.
     */
    public static ProductListFragment newInstance(String categoryId, String page, List<String[]> collection) {
        ProductListFragment fragment = new ProductListFragment();
        fragment.setFilterOptions(collection);
        Bundle args = new Bundle();
        args.putString(ARG_CATEGORYID, categoryId);
        args.putString(ARG_PAGE, page);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            selectedPage = getArguments().getString(ARG_PAGE);
            if (selectedPage.equalsIgnoreCase(Constant.SEARCH_PAGE))
                searchTerms = getArguments().getString(ARG_CATEGORYID);
            else
                categoryId = Integer.parseInt(getArguments().getString(ARG_CATEGORYID));
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.mContext = getActivity();
        this.application = new McommApplication();
        this.wishlistdbExecutor = new WishlistdbExecutor(mContext);
        View rootView = inflater.inflate(R.layout.fragment_product_list, container, false);
        lineFilterLayout = (LinearLayout) rootView.findViewById(R.id.sort_filter);
        lineFilterLayout.setVisibility(View.GONE);
        rootView.findViewById(R.id.filter_holder).setOnClickListener(this);
        rootView.findViewById(R.id.sort_holder).setOnClickListener(this);
        gridViewProducts = (GridView) rootView.findViewById(R.id.gridview);
        emptyView = (TextView) rootView.findViewById(R.id.emptyView);
        mSwipyRefreshLayout = (SwipyRefreshLayout) rootView.findViewById(R.id.swipe_container);
        mSwipyRefreshLayout.setOnRefreshListener(this);
        progress = new Progress(mContext);
        pDialog = progress.showProgress();
        redirectRequest();
        // Inflate the layout for this fragment
        return rootView;
    }


    private void setFilterOptions(List<String[]> filterOptions) {

        if (filterOptions != null) {
            filterCollections = new ArrayList<String[]>();
            this.filterCollections.addAll(filterOptions);
            filters = getAllFilterOptionSelected(filterOptions).toString();
        } else {
            filters = "";
        }
    }

    protected JSONObject getAllFilterOptionSelected(List<String[]> filterOptions) {

        JSONObject customOption = new JSONObject();
        String[] attrCode = filterOptions.remove(0);
        for (int i = 0; i < filterOptions.size(); i++) {
            String[] currentSelection = filterOptions.get(i);
            try {
                JSONArray filter = Utils.toJsonArray(currentSelection);
                if (filter != null && filter.length() > 0)
                    customOption.put(attrCode[i], filter);
            } catch (JSONException e) {
                LogUtils.i(TAG, e.getMessage());
            }
        }
        return customOption;
    }


    private void setEmptyView() {
        mSwipyRefreshLayout.setEnabled(false);
        gridViewProducts.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        emptyView.setText(getResources().getString(R.string.no_results_pdts));

    }

    /**
     * Set the product list to the adapter.
     */
    private void setAdapter() {
        if (page > 1) {
            productListAdapter.notifyDataSetChanged();
        } else {
            if (!selectedPage.equalsIgnoreCase(Constant.SEARCH_PAGE))
                lineFilterLayout.setVisibility(View.VISIBLE);
            gridViewProducts.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            productListAdapter = new ProductListAdapter(getActivity(), productResponse, ProductListAdapter.LIST_TYPE.PRODUCT);
            wishlistList = wishlistdbExecutor.getWishlist();
            productListAdapter.setWishlistdb(wishlistList);
            gridViewProducts.setAdapter(productListAdapter);
            gridViewProducts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
                        onItemSelected("ProductList", null, productResponse.get(position));
                    }

                }
            });
            this.productListAdapter.setOnWishlistClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (McommApplication.isNetandLogin(mContext, Constant.CHECK_USER_LOGIN_TOAST)) {
                        selectedProduct = Integer.parseInt(v.getTag().toString());
                        if (productResponse.get(selectedProduct).getIsWishlist())
                            deleteWishlistRequest(selectedProduct);
                        else
                            addWishlistRequest(selectedProduct);
                    }

                }
            });
        }
    }

    public void onItemSelected(String type, String id, Product productItem) {
        if (mListener != null) {
            mListener.onProductsListInteraction(type, id, productItem);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnProductListInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.filter_holder:
                FilterFragment.setSelections(this.filterCollections);
                onItemSelected("Filter", String.valueOf(categoryId), null);
                break;
            case R.id.sort_holder:
                sortDialog = new Dialog(mContext);
                sortDialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                sortDialog.setContentView(R.layout.listview);
                lvSort = (ListView) sortDialog.findViewById(R.id.listview);
                lvSort.setDividerHeight(1);
                lvSort.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                setSortAdapter();
                break;
            default:
                break;
        }
    }

    private void setSortAdapter() {
        List<String> sortList = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.sort_items)));
        final ProductImageAdapter sortAdapter = new ProductImageAdapter(mContext, sortList, sortPosition, ProductImageAdapter.LIST_TYPE.SORT);
        lvSort.setAdapter(sortAdapter);
        lvSort.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sortPosition = position;
                sortAdapter.setPosition(position);
                sortAdapter.notifyDataSetChanged();
                setSortoptions(position);
                sortDialog.dismiss();
            }
        });

        sortDialog.show();
    }

    @Override
    public void onRefresh(SwipyRefreshLayoutDirection direction) {
        if (totalCount <= page * Constant.LIMIT_COUNT) {
            Utils.showToast(mContext, getString(R.string.no_more_product), Toast.LENGTH_LONG);
            mSwipyRefreshLayout.setRefreshing(false);
            mSwipyRefreshLayout.setEnabled(false);
        } else {
            page = page + 1;
           redirectRequest();
        }
    }

    private void redirectRequest() {
        if (selectedPage.equalsIgnoreCase(Constant.SEARCH_PAGE))
            searchRequest(searchTerms);
        else
            serverRequest();
     }


    private void dismissProgress() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    private void setSortoptions(int position) {
        switch (position) {
            case 0:
                sortBy = Constant.SORT_RATING;
                orderBy = Constant.SORT_DESC;
                break;
            case 1:
                sortBy = Constant.SORT_NAME;
                orderBy = Constant.SORT_ASC;
                break;
            case 2:
                sortBy = Constant.SORT_NAME;
                orderBy = Constant.SORT_DESC;
                break;
            case 3:
                sortBy = Constant.SORT_PRICE;
                orderBy = Constant.SORT_ASC;
                break;
            case 4:
                sortBy = Constant.SORT_PRICE;
                orderBy = Constant.SORT_DESC;
                break;

            default:
                break;
        }
        pDialog = progress.showProgress();
        serverRequest();
    }

    /**
     * Request for product list with specified category id
     */
    private void serverRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            BusProvider.getInstance().register(this);
            Map<String, String> productListParams = new HashMap<>();
            productListParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            productListParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            productListParams.put(Constant.Request.ACTION, Constant.Request.CATEGORY_PRODUCT_ACTION);
            productListParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            productListParams.put(Constant.Request.CATEGORY_ID, String.valueOf(categoryId));
            productListParams.put(Constant.Request.SORTBY, sortBy);
            productListParams.put(Constant.Request.ORDERBY, orderBy);
            productListParams.put(Constant.Request.FILTERS_ACTION, filters);
            productListParams.put(Constant.Request.PAGE, String.valueOf(page));
            productListParams.put(Constant.Request.LIMIT, String.valueOf(Constant.LIMIT_COUNT));
            new RestClient(mContext).getInstance().get().getProductList(productListParams, new RestCallback<ProductResponse>());
        }
    }

    @Subscribe
    public void dataReceived(String errorMessage) {
        BusProvider.getInstance().unregister(this);
        dismissProgress();
        if (errorMessage.equalsIgnoreCase(Constant.UNAUTHORIZED_VALUE)) {
            McommApplication.redirectAuthentication(mContext);
        } else {
            Utils.showToast(mContext, errorMessage, Toast.LENGTH_SHORT);
        }
    }

    public void backPressed(boolean wishListChanged) {
        if (wishListChanged) {
            wishlistList = wishlistdbExecutor.getWishlist();
            productListAdapter.setWishlistdb(wishlistList);
            productListAdapter.notifyDataSetChanged();
        }
    }

    @Subscribe
    public void dataReceived(ProductResponse result) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {
            totalCount = Integer.parseInt(result.totalCount);
            productList = result.products;
            if (totalCount > 0) {
                DbParser parser;
                try {
                    parser = new DbParser(getActivity(), result, DbParser.PARSE_TYPE.WISHLIST_INSERT);
                    parser.setOnParsingCompletionListener(this);
                    parser.start();
                } catch (Exception e) {
                    LogUtils.i(TAG, e.getMessage());
                }
            } else {
                setEmptyView();
            }
            mSwipyRefreshLayout.setRefreshing(false);
        } else {
            LogUtils.i(TAG, result.getMessage());
        }

    }

    @Override
    public void onParsingCompletion() {
        if (page > 1)
            productResponse.addAll(productList);
        else
            productResponse = productList;
        setAdapter();

    }

    @Override
    public void onParsingCompletionError(String error) {
        LogUtils.i(Constant.TAG_EXCEPTION, error);
    }


    @Subscribe
    public void dataReceived(WishlistResponse response) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        if (McommApplication.checkResponse(response.getError(), response.getSuccess())) {
            Utils.showToast(mContext, response.getMessage(), Toast.LENGTH_SHORT);
            Product item = productResponse.get(selectedProduct);
            updateProduct(item);
        } else {
            LogUtils.i(TAG, response.getMessage());
        }

    }

    private void updateProduct(Product item) {

        Wishlistdb wishItem = wishlistList.get(selectedProduct);
        item.setIsWishlist(!wishItem.getIsWishList());
        wishItem.setIsWishList(!wishItem.getIsWishList());
        wishlistdbExecutor.updateWishlist(String.valueOf(item.getProductId()), !item.getIsWishlist());
        productListAdapter.setWishlistdb(wishlistList);
        productListAdapter.notifyDataSetChanged();

    }

    /**
     * Request for product list with specified category id
     *
     * @param position
     */
    private void addWishlistRequest(int position) {
        pDialog = progress.showProgress();
        BusProvider.getInstance().register(this);
        Map<String, String> addParams
                = application.getAddWishlistParams(mContext, productResponse.get(position).getProductId().toString());
        new RestClient(mContext).getInstance().get().addWishlist(addParams, new RestCallback<WishlistResponse>());

    }

    /**
     * Request for product list with specified category id
     *
     * @param position
     */
    private void deleteWishlistRequest(int position) {
        pDialog = progress.showProgress();
        BusProvider.getInstance().register(this);
        new RestClient(mContext).getInstance().get().deleteWishlist(McommApplication.getStoreId(mContext), Constant.WEBSITE_ID_VALUE,
                application.getCustomerId(mContext), application.getToken(mContext),
                productResponse.get(position).getProductId().toString(),
                Constant.Request.DELETE_WISHLIST_ACTION, new RestCallback<WishlistResponse>());

    }


    private void searchRequest(String searchTerm) {

        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            BusProvider.getInstance().register(this);
            new RestClient(mContext)
                    .getInstance()
                    .get()
                    .getSearch(McommApplication.getStoreId(mContext),
                            Constant.WEBSITE_ID_VALUE,
                            Constant.Request.SEARCH_ACTION,
                            searchTerm,
                            McommApplication.getCustomerId(mContext),
                            String.valueOf(Constant.PAGE_VALUE),
                            String.valueOf(Constant.LIMIT_COUNT), new RestCallback<ProductResponse>());
        }

    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnProductListInteractionListener {
        public void onProductsListInteraction(String type, String id, Product productItem);
    }
}
