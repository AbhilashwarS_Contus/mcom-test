/**
 * StaticPageFragment.java
 * <p/>
 * This is the fragment view for static page.
 *
 * @category Contus
 * @package com.contus.mcomm.fragments
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Toast;

import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.model.StaticPageResponse;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;


public class StaticPageFragment extends Fragment {
    private static final String TAG = LogUtils.makeLogTag(StaticPageFragment.class);
    private static final String STATIC_PAGE_ID = "page_id";
    private WebView staticPage;
    private Context mContext;
    private String staticPageId;
    private Progress progress;
    private Dialog pDialog;

    /**
     * Returns a new instance of this fragment.
     */
    public static StaticPageFragment newInstance(String pageId) {
        StaticPageFragment fragment = new StaticPageFragment();
        Bundle args = new Bundle();
        args.putString(STATIC_PAGE_ID, pageId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mContext = getActivity();
        if (getArguments() != null) {
            staticPageId = getArguments().getString(STATIC_PAGE_ID);
        }
        progress = new Progress(mContext);
        pDialog = progress.showProgress();
        staticPageRequest();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_static_page, container, false);
        staticPage = (WebView) view.findViewById(R.id.static_page);
        return view;
    }

    /**
     * Used to request the static page.
     */
    private void staticPageRequest() {
        pDialog.show();
        BusProvider.getInstance().register(this);
        new RestClient(mContext).getInstance().get().getStaticPage(McommApplication.getStoreId(mContext), Constant.WEBSITE_ID_VALUE, Constant.Request.STATIC_PAGE_ACTION, staticPageId, new RestCallback<StaticPageResponse>());

    }

    /**
     * method to get the error response from the server
     *
     * @param errorMessage
     */
    @Subscribe
    public void dataReceived(String errorMessage) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);
        Toast.makeText(mContext, errorMessage, Toast.LENGTH_LONG).show();
        LogUtils.i(TAG, errorMessage.toString());
    }

    /**
     * method to get the static page response from the server
     *
     * @param result
     */
    @Subscribe
    public void dataReceived(StaticPageResponse result) {

        BusProvider.getInstance().unregister(this);
        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {
            staticPage.loadData(
                    Html.fromHtml(
                            "<html><body>" + result.getContent() + "</body></html>")
                            .toString(), "text/html", "utf-8");
        } else {
            LogUtils.i(TAG, result.getMessage());
        }
        dismissProgress();
    }

    /**
     * method to dismiss the progress
     */
    private void dismissProgress() {
        progress.dismissProgress(pDialog);
    }

}