/**
 * CustomFragment.java
 * <p/>
 * This is the fragment view for Custom options.
 *
 * @category Contus
 * @package com.contus.mcomm.fragments
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;

import com.contus.mcomm.adapters.ProductImageAdapter;
import com.contus.mcomm.externallibraries.HorizontalListView;
import com.contus.mcomm.model.CustomAttribute;
import com.contus.mcomm.utils.Utils;
import com.contussupport.ecommerce.R;

import java.util.List;


public class CustomFragment extends Fragment implements AdapterView.OnItemClickListener {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String POSITION = "position";
    List<CustomAttribute> customList;
    ProductImageAdapter customAttributeAdapter;
    private int selectedPosition = -1, position;
    private String selectedId;
    private OnConfigurableListener mListener;

    /**
     * default constructor for custom Fragment
     */
    public CustomFragment() {

    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param position   Parameter 1.
     * @param customList Parameter 2.
     * @return A new instance of fragment CustomFragment.
     */
    public static CustomFragment newInstance(int position, List<CustomAttribute> customList) {
        CustomFragment fragment = new CustomFragment();
        fragment.setCustomList(customList);
        Bundle args = new Bundle();
        args.putInt(POSITION, position);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Used to set the custom list
     *
     * @param customList Parameter1.
     * @return null
     */
    private void setCustomList(List<CustomAttribute> customList) {
        this.customList = customList;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt(POSITION);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.horizontal_listview, container, false);
        HorizontalListView customListView = (HorizontalListView) rootView.findViewById(R.id.custom_list);
        customAttributeAdapter = new ProductImageAdapter(getActivity(), customList, selectedPosition, ProductImageAdapter.LIST_TYPE.CUSTOM_VIEW);
        customListView.setAdapter(customAttributeAdapter);
        customListView.setOnItemClickListener(this);
        return rootView;
    }

    /**
     * Used to perform custom selected option
     *
     * @param position,id,code
     */
    public void onItemSelected(int position, String id, String code) {
        if (mListener != null) {
            mListener.onCustomSelected(position, id, code);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnConfigurableListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Overridden method to perform the on Item click event
     *
     * @param parent,view,position,id.
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (customList.get(position).getIsStock().equals("1")) {
            onItemSelected(this.position, customList.get(position).getConfigId(), customList.get(position).getLabel());
            String configId = customList.get(position).getConfigId();
            selectedId = configId;
            selectedPosition = Integer.parseInt(selectedId);
            customAttributeAdapter.setPosition(selectedPosition);
            customAttributeAdapter.notifyDataSetChanged();
        } else {
            Utils.showToast(getActivity(), getString(R.string.out_of_stock_product_msg), Toast.LENGTH_SHORT);
        }
    }

    /**
     * Method to get the selected id
     */
    protected String getSelectedId() {
        return selectedId;
    }

    /**
     * Method to update the custom view
     *
     * @param updateItems,position.
     */
    protected void updateCustomView(List updateItems, String position) {
        this.customList = updateItems;

        if (!TextUtils.isEmpty(position))
            this.selectedPosition = Integer.parseInt(position);
        customAttributeAdapter.setPosition(selectedPosition);
        customAttributeAdapter.swapList(updateItems);
        customAttributeAdapter.notifyDataSetChanged();
    }

    /**
     * Method to remove the selection
     */
    protected void removeSelection() {
        selectedPosition = -1;
        customAttributeAdapter.setPosition(selectedPosition);
        customAttributeAdapter.notifyDataSetChanged();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnConfigurableListener {
        public void onCustomSelected(int position, String id, String code);
    }
}
