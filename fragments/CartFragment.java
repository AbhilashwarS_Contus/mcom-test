/**
 * CartFragment.java
 * <p/>
 * This is a view class of Cart Page.
 *
 * @category Contus
 * @package contus.com.mcomm
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.adapters.CartAdapter;
import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.model.Cart;
import com.contus.mcomm.model.CartItem;
import com.contus.mcomm.model.CartItemResponse;
import com.contus.mcomm.model.CartList;
import com.contus.mcomm.model.CartUpdateResponse;
import com.contus.mcomm.model.CouponResponse;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.CustomDialog;
import com.contus.mcomm.views.CustomEditTextView;
import com.contus.mcomm.views.NumberPicker;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CartFragment extends Fragment implements View.OnClickListener, CustomDialog.OnDialogSelectedListener {

    protected static final String TAG = LogUtils.makeLogTag(CartFragment.class);
    private ListView viewCartItems;
    private TextView txtSubTotal, txtShipping, txtGrandTotal, txtCartAmount, emptyView;
    private McommApplication application;
    private Context mContext;
    private CartList cartItem;
    private Dialog pDialog;
    private CartAdapter cartListAdapter;
    private boolean isDelete = false;
    private boolean isUpdate = false;
    private int selectedPosition;
    private List<CartItem> cartList;
    private int cartQuatity;
    private Button checkContinue;
    private CustomEditTextView etCouponCode;
    private Button btApply;
    private LinearLayout liViewDiscount;
    private TextView txtDiscountPrice, totalItemCount;
    private LinearLayout cartItemLayout;
    private CartItem seletedItems;
    private boolean isApply = false;
    private String currency;
    private OnCartInteractionListener mListener;
    private CustomDialog mydialog;
    private Bundle args;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_cart, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.application = new McommApplication();
        this.mContext = getActivity();
        initViews();
        Progress progress = new Progress(mContext);
        pDialog = progress.showProgress();
        currency = McommApplication.getCurrencySymbol(mContext) + Constant.SINGLE_SPACE;
        cartInfoRequest();
    }
private LinearLayout cartLayout;
    /**
     * method to initialize all the view objects
     */
    private void initViews() {
        ((Activity) mContext).getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        View rootView = getView();
        if (rootView != null) {
            cartLayout = (LinearLayout) rootView.findViewById(R.id.cart_layout);
            viewCartItems = (ListView) rootView.findViewById(R.id.listview);
            txtCartAmount = (TextView) rootView.findViewById(R.id.cart_amount);
            emptyView = (TextView) rootView.findViewById(R.id.emptyView);
            cartItemLayout = (LinearLayout) rootView.findViewById(R.id.cart_items_layout);
            totalItemCount = (TextView) rootView.findViewById(R.id.total_cart_items);
        }
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View footer = inflater.inflate(R.layout.fragment_cart_footer, null);
        txtSubTotal = (TextView) footer.findViewById(R.id.txt_subtotal);
        txtShipping = (TextView) footer.findViewById(R.id.txt_shipping_charge);
        txtGrandTotal = (TextView) footer.findViewById(R.id.txt_grand_total);
        txtDiscountPrice = (TextView) footer.findViewById(R.id.discount_price);
        liViewDiscount = (LinearLayout) footer.findViewById(R.id.view_discount);
        btApply = (Button) footer.findViewById(R.id.apply_coupon);
        etCouponCode = (CustomEditTextView) footer.findViewById(R.id.coupon_code);
        checkContinue = (Button) rootView.findViewById(R.id.btn_continue);
        checkContinue.setOnClickListener(this);
        btApply.setOnClickListener(this);
        viewCartItems.addFooterView(footer);
    }

    /**
     * method to request the cart Information from server
     */
    private void cartInfoRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog.show();
            BusProvider.getInstance().register(this);
            Map<String, String> addCartParams = new HashMap<>();
            addCartParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            addCartParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            addCartParams.put(Constant.Request.ACTION, Constant.Request.GET_CART_INFO_ACTION);
            addCartParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            addCartParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            new RestClient(mContext).getInstance().get().getCartInfo(addCartParams, new RestCallback<CartItemResponse>());
        }
    }

    /**
     * method to the delete the cart item
     *
     * @param seletedItems
     */
    private void cartDeleteRequest(CartItem seletedItems) {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog.show();
            BusProvider.getInstance().register(this);
            String superAttribute = "", customOption = "";
            if (seletedItems.getTypeId().equalsIgnoreCase(Constant.TYPE_CONFIGURABLE)) {
                superAttribute = seletedItems.getConfig().getCartInfo().getSuperAttribute().toString();
            }
            new RestClient(mContext).getInstance().get().deleteCart(
                    McommApplication.getStoreId(mContext),
                    Constant.WEBSITE_ID_VALUE,
                    application.getCustomerId(mContext),
                    application.getToken(mContext),
                    seletedItems.getEntityId(),
                    application.getQuoteId(mContext),
                    superAttribute,
                    customOption,
                    Constant.Request.DELETE_CART_ACTION, new RestCallback<CartUpdateResponse>());
        }
    }

    /**
     * method to update the cart item
     *
     * @param seletedItems
     */
    private void cartUpdateRequest(CartItem seletedItems) {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog.show();
            BusProvider.getInstance().register(this);
            Map<String, String> updateCartParams = new HashMap<>();

            if (seletedItems.getTypeId().equalsIgnoreCase(Constant.TYPE_CONFIGURABLE)) {
                updateCartParams.put(Constant.Request.SUPER_ATTRIBUTE, seletedItems.getConfig().getCartInfo().getSuperAttribute().toString());
            }

            updateCartParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            updateCartParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            updateCartParams.put(Constant.Request.ACTION, Constant.Request.UPDATE_CART_ACTION);
            updateCartParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            updateCartParams.put(Constant.Request.PRODUCT_ID, seletedItems.getEntityId());
            updateCartParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            updateCartParams.put(Constant.Request.PRODUCT_QUANTITY, String.valueOf(cartQuatity));
            updateCartParams.put(Constant.Request.CURRENCY_CODE, McommApplication.getCurrencyCode(mContext));
            updateCartParams.put(Constant.Request.CART_REQ_QUOTE_ID, application.getQuoteId(mContext));

            new RestClient(mContext).getInstance().get().updateCart(updateCartParams, new RestCallback<CartUpdateResponse>());
        }
    }

    /**
     * method to set the values to the adapter
     */
    private void setAdapter() {

        if (isDelete) {
            cartList.remove(selectedPosition);
            cartListAdapter.setCartError(false);
            cartListAdapter.notifyDataSetChanged();
            isDelete = false;
        } else if (isUpdate) {
            CartItem updatedItems = cartList.remove(selectedPosition);
            updatedItems.setUpdate(false);
            updatedItems.setQty(updatedItems.getUpdateQty());
            updateCart(updatedItems, selectedPosition);
            isUpdate = false;
        } else if (!cartList.isEmpty()) {
            cartItemLayout.setVisibility(View.VISIBLE);
            cartListAdapter = new CartAdapter(getActivity(), cartList, CartAdapter.LIST_TYPE.CART_LIST);
            viewCartItems.setAdapter(cartListAdapter);
            cartListAdapter.setOnCartClickListener(this);
            cartListAdapter.setOnCartChangeListener(new NumberPicker.OnChangedListener() {
                @Override
                public void onChanged(NumberPicker picker, int oldVal, int newVal, int position) {
                    cartQuatity = picker.getCurrent();
                    CartItem updatedItems = cartList.remove(position);
                    if (cartQuatity != Integer.parseInt(updatedItems.getQty()))
                        updatedItems.setUpdate(true);
                    else
                        updatedItems.setUpdate(false);
                    updatedItems.setUpdateQty(String.valueOf(cartQuatity));
                    updateCart(updatedItems, position);
                }
            });
        }

        if (cartList.isEmpty()) {
            setEmptyView();
        }
    }

    /**
     * method to show empty view when the list is empty
     */
    private void setEmptyView() {
        checkContinue.setVisibility(View.GONE);
        viewCartItems.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        cartItemLayout.setVisibility(View.GONE);
        emptyView.setText(getResources().getString(R.string.no_results_cart));
    }

    /**
     * method to update the cart item by getting the position
     *
     * @param updatedItems
     * @param position
     */
    private void updateCart(CartItem updatedItems, int position) {
        updatedItems.setRowTotal(calcRowPrice(updatedItems.getPrice()).toString());
        cartList.add(position, updatedItems);
        cartItem.setItems(cartList);
        cartListAdapter.setCartError(false);
        cartListAdapter.notifyDataSetChanged();
    }

    /**
     * method to get the float value from the string
     *
     * @param price
     */
    private Float calcRowPrice(String price) {

        return cartQuatity * Float.parseFloat(price);
    }

    /**
     * method to set the footer
     *
     * @param footerItem
     */
    private void setFootervalue(Cart footerItem) {

        if (TextUtils.isEmpty(footerItem.getCouponCode())) {
            liViewDiscount.setVisibility(View.GONE);
            btApply.setText(getString(R.string.apply_coupon));
            etCouponCode.setEnabled(true);
            etCouponCode.setHint(getString(R.string.enter_coupon_code));
            isApply = false;
        } else {
            isApply = true;
            txtDiscountPrice.setText("-" + currency + footerItem.getDiscount());
            liViewDiscount.setVisibility(View.VISIBLE);
            etCouponCode.setText(footerItem.getCouponCode());
            btApply.setText(getString(R.string.delete_coupon));
            etCouponCode.setEnabled(false);
        }

        txtSubTotal.setText(currency + footerItem.getSubtotal());
        txtGrandTotal.setText(currency + footerItem.getGrandTotal());

        String shippingAmount = footerItem.getShippingAmount();
        if (Utils.checkFreeShipping(shippingAmount))
            txtShipping.setText(getResources().getString(R.string.free_shipping));
        else
            txtShipping.setText(shippingAmount);

        Resources res = getResources();
        final StyleSpan bold = new StyleSpan(android.graphics.Typeface.BOLD);
        Spannable totalText = new SpannableString(res.getString(R.string.cart_txt_total));
        totalText.setSpan(new ForegroundColorSpan(res.getColor(R.color.text_primary)), 0, totalText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtCartAmount.setText(totalText);
        String grandTotalString = Constant.SINGLE_SPACE + currency + footerItem.getGrandTotal();
        Spannable totalString = new SpannableString(grandTotalString);
        totalString.setSpan(bold, 0, totalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        totalString.setSpan(new ForegroundColorSpan(res.getColor(R.color.text_primary)), 0, totalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        txtCartAmount.append(totalString);


        Spannable itemText = new SpannableString(res.getString(R.string.cart_txt_item));
        itemText.setSpan(new ForegroundColorSpan(res.getColor(R.color.text_primary)), 0, itemText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        totalItemCount.setText(itemText);
        String itemCountString = Constant.SINGLE_SPACE + footerItem.getItemCount();
        Spannable countString = new SpannableString(itemCountString);

        totalString.setSpan(bold, 0, countString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        totalString.setSpan(new ForegroundColorSpan(res.getColor(R.color.text_primary)), 0, countString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        totalItemCount.append(countString);
        setData(footerItem);
    }

    /**
     * method to get the selected item from the view
     *
     * @param v
     */
    private void selectedItem(View v) {
        selectedPosition = Integer.parseInt(v.getTag().toString());
        seletedItems = cartList.get(selectedPosition);
    }

    /**
     * method to set the data to the cart item
     *
     * @param item
     */
    private void setData(Cart item) {
        cartItem.setShippingAmount(item.getShippingAmount());
        cartItem.setItemsQty(item.getItemsQty());
        cartItem.setTax(item.getTax());
        cartItem.setQuoteId(item.getQuoteId());
        cartItem.setGrandTotal(item.getGrandTotal());
        cartItem.setCouponCode(item.getCouponCode());
        cartItem.setSubtotal(item.getSubtotal());
        cartItem.setItemCount(item.getItemCount());
        cartItem.setDiscount(item.getDiscount());
    }

    /**
     * method to perform the click event
     *
     * @param v
     */
    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.cart_delete:
                selectedItem(v);
                args = new Bundle();
                args.putInt(Constant.DIALOG_CART_PARAM, Constant.DIALOG_CART_DELETE);
                showDialog(args);
                break;
            case R.id.cart_update:
                selectedItem(v);
                args = new Bundle();
                args.putInt(Constant.DIALOG_CART_PARAM, Constant.DIALOG_CART_UPDATE);
                showDialog(args);
                break;
            case R.id.apply_coupon:
                String couponCode = etCouponCode.getText().toString();
                if (TextUtils.isEmpty(couponCode)) {
                    etCouponCode.requestFocus();
                    etCouponCode.setFocusableInTouchMode(true);
                    etCouponCode.setError(getString(R.string.please_enter_coupon_code));
                } else {
                    couponRequest(couponCode);
                }
                break;
            case R.id.btn_continue:
                onItemSelected(cartItem);
                break;
            default:
                break;
        }
    }

    /**
     * method to show the dialog
     *
     * @param args
     */
    private void showDialog(Bundle args) {

        mydialog = new CustomDialog();
        mydialog.setArguments(args);
        mydialog.setTargetFragment(this, 0);
        mydialog.show(getFragmentManager(), "dialog");
    }

    /**
     * method to request the coupon
     */
    private void couponRequest(String couponCode) {
        if (isApply) {
            deleteCoupon();
        } else {
            applyCoupon(couponCode);
        }
    }

    /**
     * method to apply the coupon by requesting it from the server
     *
     * @param couponCode
     */
    private void applyCoupon(String couponCode) {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog.show();
            BusProvider.getInstance().register(this);
            Map<String, String> applyParams = new HashMap<>();
            applyParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            applyParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            applyParams.put(Constant.Request.ACTION, Constant.Request.APPLY_COUPON);
            applyParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            applyParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            applyParams.put(Constant.Request.CART_REQ_QUOTE_ID, application.getQuoteId(mContext));
            applyParams.put(Constant.Request.COUPON_CODE, couponCode);
            new RestClient(mContext).getInstance().get().coupon(applyParams, new RestCallback<CouponResponse>());

        }
    }

    /**
     * method to delete the coupon by requesting it from the server
     */
    private void deleteCoupon() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog.show();
            BusProvider.getInstance().register(this);
            Map<String, String> removeParams = new HashMap<>();

            removeParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            removeParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            removeParams.put(Constant.Request.ACTION, Constant.Request.CANCEL_COUPON);
            removeParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            removeParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            removeParams.put(Constant.Request.CART_REQ_QUOTE_ID, application.getQuoteId(mContext));
            new RestClient(mContext).getInstance().get().coupon(removeParams, new RestCallback<CouponResponse>());

        }
    }

    /**
     * method to get the coupon response from the server and set the data to footer
     *
     * @param response
     */
    @Subscribe
    public void dataReceived(CouponResponse response) {
        BusProvider.getInstance().unregister(this);
        pDialog.cancel();
        String message = response.getMessage();
        if (McommApplication.checkResponse(response.getError(), response.getSuccess())) {
            Utils.showToast(mContext, message, Toast.LENGTH_LONG);
            if (McommApplication.isSuccess(response.getSuccess())) {
                setFootervalue(response.getCart());
            }
        } else {
            LogUtils.i(TAG, message);
        }
    }

    /**
     * method to get the Cart item response from the server and set the data
     *
     * @param response
     */
    @Subscribe
    public void dataReceived(CartItemResponse response) {
        BusProvider.getInstance().unregister(this);
        pDialog.cancel();

        if (McommApplication.checkResponse(response.getError(), response.getSuccess())) {
            if (McommApplication.isSuccess(response.getSuccess())) {
                cartItem = response.cartItem;
                Utils.savePreferences(getActivity(), Constant.PREF_CART_ITEM_COUNT, cartItem.getItemCount());
                Utils.savePreferences(getActivity(), Constant.PREF_CART_QUOTE_ID, cartItem.getQuoteId());
                cartList = response.cartItem.getItems();

                setAdapter();
                if (!cartList.isEmpty())
                    setFootervalue(response.cartItem);
            }
        } else {
            setEmptyView();
            LogUtils.i(TAG, response.getMessage());
        }
    }

    /**
     * method to get the Cart update response from the server
     *
     * @param response
     */
    @Subscribe
    public void dataReceived(CartUpdateResponse response) {
        BusProvider.getInstance().unregister(this);
        pDialog.cancel();

        if (McommApplication.checkResponse(response.getError(), response.getSuccess())) {
            Utils.showToast(mContext, response.getMessage(), Toast.LENGTH_LONG);
            if (McommApplication.isSuccess(response.getSuccess())) {
                Cart item = response.cartItem;
                Utils.savePreferences(getActivity(), Constant.PREF_CART_ITEM_COUNT, item.getItemCount());
                Utils.savePreferences(getActivity(), Constant.PREF_CART_QUOTE_ID, item.getQuoteId());
                setAdapter();
                setFootervalue(response.cartItem);
            }
        } else {
            LogUtils.i(TAG, response.getMessage());
        }
    }

    /**
     * method to get the error response from the server
     *
     * @param errorMessage
     */
    @Subscribe
    public void dataReceived(String errorMessage) {
        pDialog.cancel();
        LogUtils.i(TAG, errorMessage);
        BusProvider.getInstance().unregister(this);
        if (errorMessage.equalsIgnoreCase(Constant.UNAUTHORIZED_VALUE)) {
            McommApplication.redirectAuthentication(mContext);
        } else {
            Utils.showToast(mContext, errorMessage, Toast.LENGTH_SHORT);
            mListener.onRemoveCartInteraction();
        }
    }

    /**
     * method to perform click event when yes button is pressed in the dialog
     */
    public void onYesClick(int clickType) {
        mydialog.dismiss();
        switch (clickType) {
            case Constant.DIALOG_CART_DELETE:
                isDelete = true;
                cartDeleteRequest(seletedItems);
                break;
            case Constant.DIALOG_CART_UPDATE:
                isUpdate = true;
                cartUpdateRequest(seletedItems);
                break;
            default:
                break;
        }

    }

    /**
     * method to perform click event when no button is pressed in the dialog
     */
    public void onNoClick(int clickType) {
        mydialog.dismiss();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnCartInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * method to set the listener
     *
     * @param cartItem
     */
    public void onItemSelected(CartList cartItem) {
        if (mListener != null) {
            mListener.onCartInteraction(cartItem);
        }
    }

    /**
     * method to set the visibility of view
     */
    public void setContinueVisibility() {
        if (cartListAdapter.isCartError()) {
            checkContinue.setVisibility(View.INVISIBLE);
        } else {
            checkContinue.setVisibility(View.VISIBLE);
        }
        cartLayout.refreshDrawableState();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

    public interface OnCartInteractionListener {
        public void onCartInteraction(CartList item);

        public void onRemoveCartInteraction();
    }


}
