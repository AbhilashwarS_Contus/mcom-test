/**
 * DiscriptionFragment.java
 * <p/>
 * This is a view class of Discription Page.
 *
 * @category Contus
 * @package contus.com.mcomm
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.views.CustomTextView;
import com.contussupport.ecommerce.R;


public class DescriptionFragment extends Fragment {

    private String strDescription;


    public DescriptionFragment() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param desc Parameter 1.
     * @return A new instance of fragment DescriptionFragment.
     */
    public static DescriptionFragment newInstance(String desc) {
        DescriptionFragment fragment = new DescriptionFragment();
        Bundle args = new Bundle();
        args.putString(Constant.DESCRIPTION, desc);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            strDescription = getArguments().getString(Constant.DESCRIPTION);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_description, container, false);
        CustomTextView tvDescription = (CustomTextView) rootView.findViewById(R.id.content);
        tvDescription.setText(strDescription);
        return rootView;
    }

}
