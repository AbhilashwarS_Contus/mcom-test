/**
 * MyOrderDetail.java
 * <p/>
 * This is a view class of my order detail Page.
 *
 * @category Contus
 * @package contus.com.mcomm
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.adapters.MyAccountAdapter;
import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.model.OrderDetail;
import com.contus.mcomm.model.OrderDetailResponse;
import com.contus.mcomm.model.OrderItem;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.NavigationActivity;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MyOrderDetailFragment extends Fragment {
    private static final String TAG = LogUtils.makeLogTag(MyOrderDetailFragment.class);
    private Context mContext;
    private McommApplication application;
    private TextView ordrId, ordFname, ordLname, ordrDelSt, ordrDelCity,
            ordrDelCountry, ordrDelPost, ordrDelPh, ordrGrandTot, ordrStatus, ordrDate;
    private OrderDetail orderDetailResult;
    private String[] stArrOrdrDelStreet;
    private String orderId;
    private Progress progress;
    private Dialog pDialog;
    private ListView ordrItemList;
    private List<OrderItem> ordrItems;
    private String currency;
    private TextView ordrShipMethod, ordrPayMethod, ordrSubTot, ordrShipChrg;

    public static MyOrderDetailFragment newInstance(String desc) {
        MyOrderDetailFragment fragment = new MyOrderDetailFragment();
        Bundle args = new Bundle();
        args.putString(Constant.Request.ORDER_ID, desc);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!getArguments().isEmpty()) {
            orderId = getArguments().getString(Constant.Request.ORDER_ID);
        } else {
            orderId = null;
        }
        this.mContext = getActivity();
        application = new McommApplication();
        currency = application.getCurrencySymbol(mContext) + Constant.SINGLE_SPACE;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.listview, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = this.getActivity();
        progress = new Progress(mContext);
        application = new McommApplication();
        initViews();
        orderDetailRequest();

    }

    /**
     * method to initialise all view objects
     */
    private void initViews() {
        View rootView = getView();

        ordrItemList = (ListView) rootView.findViewById(R.id.listview);
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View header = inflater.inflate(R.layout.my_order_details, null);
        ordrId = (TextView) header.findViewById(R.id.ordr_id_num);
        ordFname = (TextView) header.findViewById(R.id.ordr_del_fname);
        ordLname = (TextView) header.findViewById(R.id.ord_del_lname);
        ordrDelSt = (TextView) header.findViewById(R.id.ordr_del_st);
        ordrDelCity = (TextView) header.findViewById(R.id.ordr_del_city);
        ordrDelCountry = (TextView) header.findViewById(R.id.ordr_del_ctry);
        ordrDelPost = (TextView) header.findViewById(R.id.ordr_del_post);
        ordrDelPh = (TextView) header.findViewById(R.id.ordr_del_ph);

        ordrShipMethod = (TextView) header.findViewById(R.id.txt_ship_method);
        ordrPayMethod = (TextView) header.findViewById(R.id.txt_payment);
        ordrSubTot = (TextView) header.findViewById(R.id.txt_sub_tot);
        ordrStatus = (TextView) header.findViewById(R.id.txt_status);
        ordrDate = (TextView) header.findViewById(R.id.txt_ordr_date);
        ordrShipChrg = (TextView) header.findViewById(R.id.txt_shipping_charge);
        ordrGrandTot = (TextView) header.findViewById(R.id.txt_grand_total);

        ordrItemList.addHeaderView(header);
    }

    /**
     * method to get the order details from server
     */
    private void orderDetailRequest() {
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            Map<String, String> orderParams = new HashMap<>();
            orderParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            orderParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            orderParams.put(Constant.Request.ACTION, Constant.Request.ORDER_DETAILS_ACTION);
            orderParams.put(Constant.Request.ORDER_ID, orderId);
            orderParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            orderParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            new RestClient(mContext).getInstance().get().getOrderDetail(orderParams, new RestCallback<OrderDetailResponse>());
        }
    }

    /**
     * method to get the order detail response from the server
     *
     * @param result
     */
    @Subscribe
    public void dataReceived(OrderDetailResponse result) {
        dismissProgress();
        BusProvider.getInstance().unregister(this);

        if (McommApplication.checkResponse(result.getError(), result.getSuccess())) {
            if (McommApplication.isSuccess(result.getSuccess())) {
                orderDetailResult = result.orderDetail;
                setOrderDetailVal();
            } else {
                Utils.showToast(mContext, result.getMessage(), Toast.LENGTH_SHORT);
                ((NavigationActivity) getActivity()).moveBack();
            }
        }
        LogUtils.i(TAG, result.getMessage());

    }

    /**
     * method to get the error response from the server
     *
     * @param errorMessage
     */
    @Subscribe
    public void dataReceived(String errorMessage) {
        dismissProgress();
        LogUtils.i(TAG, errorMessage);
        BusProvider.getInstance().unregister(this);
        if (errorMessage.equalsIgnoreCase(Constant.UNAUTHORIZED_VALUE)) {
            McommApplication.redirectAuthentication(mContext);
        } else {
            Utils.showToast(mContext, errorMessage, Toast.LENGTH_SHORT);
        }
    }

    /**
     * method to set the order details values to the view objects
     */
    private void setOrderDetailVal() {
        ordrId.setText(orderDetailResult.getOrderId());
        ordFname.setText(orderDetailResult.getShippingAddress().getFirstName());
        ordLname.setText(orderDetailResult.getShippingAddress().getLastName());
        stArrOrdrDelStreet = orderDetailResult.getShippingAddress().getStreet();

        for (String val : stArrOrdrDelStreet) {
            ordrDelSt.setText(val);
        }
        ordrDelCity.setText(orderDetailResult.getShippingAddress().getCity());
        ordrDelCountry.setText(orderDetailResult.getShippingAddress().getCountryName());
        ordrDelPost.setText(orderDetailResult.getShippingAddress().getPostcode());
        ordrDelPh.setText("T: " + orderDetailResult.getShippingAddress().getTelephone());
        ordrStatus.setText(orderDetailResult.getStatus());
        ordrDate.setText(Utils.changeDateFormat(orderDetailResult.getOrderDate(), true));
        ordrSubTot.setText(currency + orderDetailResult.getSubAmount());
        ordrGrandTot.setText(currency + orderDetailResult.getGrandTotal());
        ordrShipMethod.setText(orderDetailResult.getShippingMethod());
        ordrPayMethod.setText(orderDetailResult.getPaymentMethod());
        String shippingAmount = orderDetailResult.getShippingAmount();
        /**
         * check whether the shipping amount is for free shipping
         */
        if (Utils.checkFreeShipping(shippingAmount))
            ordrShipChrg.setText(getResources().getString(R.string.free_shipping));
        else
            ordrShipChrg.setText(shippingAmount);

        ordrItems = orderDetailResult.getOrderItem();
        ordrItemList.setAdapter(new MyAccountAdapter(mContext, ordrItems, MyAccountAdapter.LIST_TYPE.ORDER_ITEMS));
    }

    /**
     * method to dismiss the progress
     */
    private void dismissProgress() {

        progress.dismissProgress(pDialog);
    }

}
