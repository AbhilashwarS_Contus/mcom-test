package com.contus.mcomm.fragments;
/**
 * FilterFragment.java
 * <p/>
 * This is the fragment view for Filter.
 *
 * @category Contus
 * @package com.contus.mcomm.fragments
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.contus.mcomm.adapters.FilterAdapter;
import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.model.Filter;
import com.contus.mcomm.model.FilterResponse;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link com.contus.mcomm.fragments.FilterFragment.OnFilterFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link com.contus.mcomm.fragments.FilterFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FilterFragment extends Fragment implements View.OnClickListener {
    private static final String TAG = LogUtils.makeLogTag(FilterFragment.class);
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String CATEGORY_ID = "categoryId";
    public static List<String[]> selections = new ArrayList<String[]>();
    private static String prevcategoryId;
    private static FilterResponse response;
    private static String strMinPrice, strMaxPrice;
    int selectedPosition;
    private String categoryId;
    private Context mContext;
    private OnFilterFragmentInteractionListener mListener;
    private LayoutInflater mInflater;
    private LinearLayout linFilterTypeHolder, priceContent;
    private ListView filtertypeList;
    private FilterAdapter filterAdapter;
    private EditText etMinPrice, etMaxPrice;
    private int pricePosition;
    private Dialog pDialog;
    private Progress progress;
    public FilterFragment() {
        // Required empty public constructor
    }

    public static void setPrevcategoryId(String prevcategoryId) {
        FilterFragment.prevcategoryId = prevcategoryId;
    }

    public static void setSelections(List<String[]> selections) {
        if (selections != null) {
            FilterFragment.selections.clear();
            FilterFragment.selections.addAll(selections);
        } else {
            prevcategoryId = "";
        }
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param id Product ID.
     * @return A new instance of fragment FilterFragment.
     */
    public static FilterFragment newInstance(String id) {
        FilterFragment fragment = new FilterFragment();
        Bundle args = new Bundle();
        args.putString(CATEGORY_ID, id);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryId = getArguments().getString(CATEGORY_ID);

        }
        mContext = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.filter, container, false);
        mInflater = LayoutInflater.from(mContext);
        init(rootView);
        return rootView;
    }

    /**
     * Method that is used to generate views.
     *
     * @return null;
     */
    private void init(View rootView) {
        linFilterTypeHolder = (LinearLayout) rootView.findViewById(R.id.filterSelection);
        filtertypeList = (ListView) rootView.findViewById(R.id.list_content);
        priceContent = (LinearLayout) rootView.findViewById(R.id.price_content);
        etMinPrice = (EditText) rootView.findViewById(R.id.minEntry);
        etMaxPrice = (EditText) rootView.findViewById(R.id.maxEntry);
        rootView.findViewById(R.id.clear_all).setOnClickListener(this);
        rootView.findViewById(R.id.apply).setOnClickListener(this);
        if (TextUtils.isEmpty(prevcategoryId) || !categoryId.equalsIgnoreCase(prevcategoryId)) {
            prevcategoryId = categoryId;
            getFilters();
        } else
            addSelectors(response.categories, false);
    }

    /*
     * Method that is used to perform on Filter Frag Interaction
     */
    public void onApply(String id, List<String[]> filterOptions) {
        if (mListener != null) {
            mListener.onFilterFragmentInteraction(id, filterOptions);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFilterFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Method to perform the on click event
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.clear_all:
                Utils.hideSoftKeyboard(mContext);
                Utils.hideSoftKeyboard(mContext);
                etMinPrice.getText().clear();
                etMaxPrice.getText().clear();
                addSelectors(response.categories, true);
                break;
            case R.id.apply:
                String minprice = etMinPrice.getText().toString().trim();
                String maxprice = etMaxPrice.getText().toString().trim();
                if (minprice.length() == 0)
                    minprice = strMinPrice;
                if (maxprice.length() == 0)
                    maxprice = strMaxPrice;
                priceFilter(minprice, maxprice);
                break;
            default:
                break;
        }
    }

    /**
     * Method to perform the price filter
     *
     * @param minPriceValue
     * @param maxPriceValue
     */
    private void priceFilter(String minPriceValue, String maxPriceValue) {
        int minPrice = Math.round(Float.parseFloat(minPriceValue));
        int maxPrice = Math.round(Float.parseFloat(maxPriceValue));
        if (minPrice > maxPrice)
            Utils.showToast(mContext, getString(R.string.min_max), Toast.LENGTH_LONG);
        else {
            selections.get(pricePosition)[0] = minPriceValue;
            selections.get(pricePosition)[1] = maxPriceValue;
            onApply(categoryId, selections);
        }
    }

    /**
     * Method to get the filters
     */
    private void getFilters() {

        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_INTERNET)) {
            progress = new Progress(mContext);
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            new RestClient(mContext)
                    .getInstance()
                    .get()
                    .getFilter(McommApplication.getStoreId(mContext), Constant.WEBSITE_ID_VALUE, Constant.Request.FILTERS_ACTION, categoryId, new RestCallback<FilterResponse>());
        }

    }

    /**
     * method to get the filter response from the server
     *
     * @param response
     */
    @Subscribe
    public void dataReceived(FilterResponse response) {
        dismissProgress();
        if (McommApplication.checkResponse(response.getError(), response.getSuccess()) && McommApplication.isSuccess(response.getSuccess())) {
            this.response = response;
            addSelectors(response.categories, true);
        } else {
            LogUtils.i(TAG, response.getMessage());
        }

    }

    /**
     * method to add the selectors
     */
    private void addSelectors(final List<Filter> categories, final boolean recreate) {
        if (recreate)
            selections.clear();
        linFilterTypeHolder.removeAllViews();
        String[] filterCategories = new String[categories.size()];
        for (int i = 0; i < categories.size(); i++) {

            View root = mInflater.inflate(R.layout.row_filtertypes,
                    null);
            Button tvType = (Button) root.findViewById(R.id.filter_type);
            String code = categories.get(i).getFilterCode();
            if ("price".equals(code)) {
                pricePosition = i + 1;
                if (recreate) {
                    strMinPrice = categories.get(i).getValues().get(0).getValue();
                    strMaxPrice = categories.get(i).getValues().get(1).getValue();
                } else {
                    etMinPrice.setText(selections.get(pricePosition)[0]);
                    etMaxPrice.setText(selections.get(pricePosition)[1]);
                }
            }
            filterCategories[i] = code;
            tvType.setText(categories.get(i).getFilterLabel());
            if (recreate) {
                selections.add(new String[categories.get(i).getValues().size()]);
                root.setTag(i);
            } else
                root.setTag(i);
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedPosition = Integer.parseInt(v.getTag().toString());
                    filterClick(selectedPosition, categories, recreate);
                }
            });
            if (categories.get(i).getValues().isEmpty()) {
                root.setVisibility(View.GONE);
            }
            linFilterTypeHolder.addView(root);
        }
        if (recreate)
            selections.add(0, filterCategories);
        selections.get(pricePosition)[0] = strMinPrice;
        selections.get(pricePosition)[1] = strMaxPrice;

        for (int i = 0; i < categories.size(); i++) {
            if (!categories.get(i).getValues().isEmpty()) {
                linFilterTypeHolder.getChildAt(i).performClick();
                break;
            }
        }

    }

    private void filterClick(int selectedPosition, List<Filter> filterCategories, boolean recreate) {
        setSelectedBG(selectedPosition);
        if ("price".equals(filterCategories.get(selectedPosition).getFilterCode())) {
            priceContent.setVisibility(View.VISIBLE);
            filtertypeList.setVisibility(View.GONE);
            etMinPrice.setHint(filterCategories.get(selectedPosition).getValues().get(0).getValue());
            etMaxPrice.setHint(filterCategories.get(selectedPosition).getValues().get(1).getValue());
        } else {
            priceContent.setVisibility(View.GONE);
            filtertypeList.setVisibility(View.VISIBLE);
            filterAdapter = new FilterAdapter(mContext, filterCategories.get(selectedPosition).getValues(), selections.get(selectedPosition + 1));
            filtertypeList.setAdapter(filterAdapter);
        }
    }

    /**
     * method to get the error response from the server
     *
     * @param errorMessage
     */
    @Subscribe
    public void dataReceived(String errorMessage) {
        dismissProgress();
        LogUtils.i(TAG, errorMessage);
        BusProvider.getInstance().unregister(this);
    }

    /**
     * Used to set the selected type.
     *
     * @param type ; Selected type
     * @return null;
     */
    private void setSelectedBG(int type) {

        for (int i = 0; i < linFilterTypeHolder.getChildCount(); i++) {
            RelativeLayout layout = (RelativeLayout) linFilterTypeHolder.getChildAt(i);
            Button tvType = (Button) layout.findViewById(R.id.filter_type);

            if (i == type) {
                tvType.setBackgroundColor(Color.WHITE);
                layout.setBackgroundColor(Color.WHITE);
                tvType.setTextColor(Color.BLACK);
            } else {
                tvType.setBackgroundColor(Color.BLACK);
                layout.setBackgroundColor(Color.BLACK);
                tvType.setTextColor(Color.WHITE);
            }
        }
    }

    /**
     * method to dismiss the progress
     */
    private void dismissProgress() {
        if (pDialog != null && pDialog.isShowing()) {
            pDialog.dismiss();
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFilterFragmentInteractionListener {
        public void onFilterFragmentInteraction(String id, List<String[]> filterOptions);
    }
}
