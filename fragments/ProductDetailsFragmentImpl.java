package com.contus.mcomm.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.contus.mcomm.adapters.ProductImageAdapter;
import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.database.AttributeExecutor;
import com.contus.mcomm.externallibraries.HorizontalListView;
import com.contus.mcomm.externallibraries.PagerSlidingTabStrip;
import com.contus.mcomm.model.CartResponse;
import com.contus.mcomm.model.Config;
import com.contus.mcomm.model.CustomAttribute;
import com.contus.mcomm.model.Options;
import com.contus.mcomm.model.Product;
import com.contus.mcomm.model.ProductDetailResponse;
import com.contus.mcomm.model.ProductDetails;
import com.contus.mcomm.model.WishlistResponse;
import com.contus.mcomm.service.RestCallback;
import com.contus.mcomm.service.RestClient;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contus.mcomm.views.Progress;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by user on 5/9/2015.
 */
public class ProductDetailsFragmentImpl extends Fragment /*implements CustomFragment.OnCustomListInteractionListeners*/ {

    protected static final String TAG = LogUtils.makeLogTag(ProductDetailsFragment.class);
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    protected Product productItem;
    protected Context mContext;
    protected View rootView;
    protected TextView tvCartCount, tvProductName, tvOfferPrice, tvActualPrice, tvProductDesc, tvRatings, tvReview,
            tvOffer, tvReadMore, tvStock, tvDelete;
    protected ImageView ivProductImage, ivWishList, ivShare;
    protected RelativeLayout relDescription;
    protected HorizontalListView imagesList;
    protected Button addToCart, btnBuyNow;
    protected OnProductsDetailListener mListener;
    protected ProductDetails productCollection;
    protected RatingBar ratProductReview;
    protected int imgPosition = 0, currentPosition = 0;
    protected List<String> productImages;
    protected Picasso mPicasso;
    protected AttributeExecutor arrtibuteExecutor;
    protected int productId;
    protected McommApplication application;
    protected String currency;
    protected boolean isBuyNow = false;
    protected SparseArray<CustomFragment> registeredFragments;
    protected String[] customOptions, resCode;
    protected Dialog pDialog;
    protected Progress progress;
    protected ViewPagerAdapter adapter;
    protected PagerSlidingTabStrip tabs;
    protected ViewPager mPager;
    protected int interation = 0;
    protected LinearLayout cartHolder, buyNowHolder;
    ProductImageAdapter imageAdapter;
    List<Config> configList;

    /**
     * Request for product list with specified category id
     */
    protected void serverRequest() {
        BusProvider.getInstance().register(this);
        progress = new Progress(mContext);
        pDialog = progress.showProgress();
        new RestClient(mContext).getInstance().get().getProductDetails(productItem.getProductId(), application.getCustomerId(mContext),
                McommApplication.getStoreId(mContext), Constant.WEBSITE_ID_VALUE, Constant.Request.PRODUCT_DETAIL_ACTION,
                new RestCallback<ProductDetailResponse>());
    }

    /**
     * Request for product list with specified category id
     */
    protected void addCartRequest(int productId) {

        progress = new Progress(mContext);
        Map<String, String> addCartParams = new HashMap<>();
        JSONObject customOptionsObj;
        if (productCollection.getProductType().equalsIgnoreCase(Constant.TYPE_CONFIGURABLE)) {
            customOptionsObj = checkAllCustomOptionSelected();
            if (customOptionsObj == null) {
                return;
            } else {
                addCartParams.put(Constant.Request.SUPER_ATTRIBUTE, customOptionsObj.toString());
            }
        }
        if (McommApplication.isNetandLogin(mContext, Constant.CHECK_USER_LOGIN_TOAST)) {
            pDialog = progress.showProgress();
            BusProvider.getInstance().register(this);
            addCartParams.put(Constant.Request.STORE_ID, McommApplication.getStoreId(mContext));
            addCartParams.put(Constant.Request.WEBSITE_ID, Constant.WEBSITE_ID_VALUE);
            addCartParams.put(Constant.Request.ACTION, Constant.Request.ADD_CART_ACTION);
            addCartParams.put(Constant.Request.TOKEN, application.getToken(mContext));
            addCartParams.put(Constant.Request.PRODUCT_ID, String.valueOf(productId));
            addCartParams.put(Constant.Request.CUSTOMER_ID, application.getCustomerId(mContext));
            addCartParams.put(Constant.Request.PRODUCT_QUANTITY, "1");
            addCartParams.put(Constant.Request.CURRENCY_CODE, McommApplication.getCurrencyCode(mContext));
            new RestClient(mContext).getInstance().get().addToCart(addCartParams, new RestCallback<CartResponse>());
        }
    }

    protected JSONObject checkAllCustomOptionSelected() {

        JSONObject customOption = new JSONObject();
        for (int i = 0; i < customOptions.length; i++) {
            if (TextUtils.isEmpty(customOptions[i])) {
                customOption = null;
                Utils.showToast(mContext, getString(R.string.choose) + Constant.SINGLE_SPACE + configList.get(i).getTitle(), Toast.LENGTH_LONG);
                break;
            } else {
                try {
                    customOption.put(configList.get(i).getAttributeId(), customOptions[i]);
                } catch (JSONException e) {
                    LogUtils.i(TAG, e.getMessage());
                }
            }
        }

        return customOption;
    }

    /**
     * Set the product discount price.
     *
     * @param actualPrice
     * @param @discountedPrice return null;
     */
    protected void setDiscountPrice(String actualPrice, String discountedPrice) {
        try {
            int percentage = Utils.calculatePercentage(Float.parseFloat(actualPrice), Float.parseFloat(discountedPrice));
            if (percentage > 0) {
                tvOffer.setVisibility(View.VISIBLE);
                tvOffer.setText(String.valueOf(percentage) + getString(R.string.percentage_off));
                tvActualPrice.setText(currency+productItem.getPrice());
                tvActualPrice.setVisibility(View.VISIBLE);
                tvActualPrice.setPaintFlags(tvActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            } else {
                tvOffer.setVisibility(View.GONE);
                tvActualPrice.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            LogUtils.i(TAG, e.getMessage().toString());
        }
    }

    /**
     * Set product main image to the view
     *
     * @param imgUrl return null;
     */
    protected void setMainImage(String imgUrl) {
        if (imgUrl != null && !imgUrl.isEmpty()) {
            mPicasso.load(imgUrl).placeholder(R.drawable.place_holder).
                    into(ivProductImage);
        }
    }


    /**
     * Method that is used to setup Custom Options Control.
     * <p/>
     * param option, Custom option records;
     * <p/>
     * return null;
     */
    protected void setCustomOption(int productId, Options option) {

        List<CustomAttribute> attributeItem = option.getAttribute();
        arrtibuteExecutor.insertData(productId, attributeItem);
        configList = option.getConfig();
        customOptions = new String[configList.size()];
        resCode = new String[configList.size()];
        setCustomAdapter();

    }

    /**
     * Method that is used to set Description of Product.
     * <p/>
     * param description, Long description data;
     * param shortDescription, Short description data;
     * <p/>
     * return null;
     */
    protected void setDescription(String description, String shortDescription) {

        if (description.isEmpty() && shortDescription.isEmpty())
            relDescription.setVisibility(View.GONE);
        else if (description.isEmpty()) {
            relDescription.setVisibility(View.VISIBLE);
            tvProductDesc.setText(shortDescription);
        } else {
            tvProductDesc.setText(description);
            relDescription.setVisibility(View.VISIBLE);
        }

    }

    /**
     * Method that is used to set Stock of a product.
     * <p/>
     * param stock, availability of product;
     * <p/>
     * return null;
     */
    protected void setStock(boolean stock) {
        if (stock) {
            tvStock.setTextColor(getResources().getColor(R.color.in_stock));
            tvStock.setText(mContext.getResources().getString(R.string.in_stock));
        } else {
            rootView.findViewById(R.id.bottom_layout).setVisibility(View.GONE);
            tvStock.setTextColor(Color.RED);
            tvStock.setText(mContext.getResources().getString(R.string.out_of_stock));
        }
    }


    /**
     * Method that is used to generate view of a Configurable product.
     * <p/>
     * param selectedListView, Listview that has to be generate a view;
     * param selectedId, Id of the selected configurable product;
     * <p/>
     * return null;
     */
    private void setCustomAdapter() {

        tabs = (PagerSlidingTabStrip) rootView.findViewById(R.id.custom_strips);
        tabs.setIndicatorColor(getResources().getColor(R.color.theme_color));
        tabs.setIndicatorHeight(5);
        tabs.setTextColor(getResources().getColor(R.color.theme_color));
        FragmentManager fm = getActivity().getSupportFragmentManager();
        adapter = new ViewPagerAdapter(fm);
        mPager = (ViewPager) rootView.findViewById(R.id.pager);
        mPager.setAdapter(adapter);
        tabs.setViewPager(mPager);

        tabs.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                // onPageScrolled
            }

            @Override
            public void onPageSelected(int position) {
                configTabSelected(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                // onPageScrollStateChanged
            }
        });


    }

    private void configTabSelected(int position) {
        currentPosition = position;
        String value = "";
        CustomFragment frag;

        String[] removedList = Arrays.copyOf(customOptions, customOptions.length);

        if (removedList.length > position) {

            removedList[position] = "";
        }

        value = Utils.join(",", removedList);

        if (TextUtils.isEmpty(customOptions[position])) {
            tvDelete.setVisibility(View.INVISIBLE);
        } else {
            tvDelete.setVisibility(View.VISIBLE);
        }
        if (registeredFragments.get(position) != null) {

            frag = registeredFragments.get(position);
            List selectedList = getListForTag(configList.get(position).getCode(), value);
            frag.updateCustomView(selectedList, customOptions[position]);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnProductsDetailListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private List<CustomAttribute> getListForTag(String selectedTag, String selectedId) {
        int selectedProductId = productCollection.getProductId();
        List<CustomAttribute> selectedCollection;
        if (TextUtils.isEmpty(selectedId))
            selectedCollection = arrtibuteExecutor.getAttributeByCode(selectedTag, selectedProductId);
        else {
            String selectedValue = arrtibuteExecutor.getValueByCode(selectedId);
            selectedCollection = arrtibuteExecutor.getAttributeById(selectedTag, selectedValue, selectedProductId);
        }

        return selectedCollection;
    }

    /**
     * Request for product add wishlist with specified product id
     *
     * @param productId
     */
    protected void addWishlistRequest(String productId) {
        pDialog = progress.showProgress();
        BusProvider.getInstance().register(this);
        Map<String, String> addParams
                = application.getAddWishlistParams(mContext, productId);
        new RestClient(mContext).getInstance().get().addWishlist(addParams, new RestCallback<WishlistResponse>());

    }

    /**
     * Request for product remove wishlist with specified product id
     *
     * @param productId
     */
    protected void deleteWishlistRequest(String productId) {
        pDialog = progress.showProgress();
        BusProvider.getInstance().register(this);
        new RestClient(mContext).getInstance().get().deleteWishlist(McommApplication.getStoreId(mContext), Constant.WEBSITE_ID_VALUE,
                application.getCustomerId(mContext), application.getToken(mContext),
                productId,
                Constant.Request.DELETE_WISHLIST_ACTION, new RestCallback<WishlistResponse>());

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnProductsDetailListener {
        void onProductsDetailInteraction(ProductDetails productItem);

        void onCartUpdateInteraction();

        void onAddWishlistInteraction(boolean flag);

    }

    /**
     * The Class ViewPagerAdapter.
     */
    public class ViewPagerAdapter extends FragmentStatePagerAdapter {


        /**
         * Instantiates a new view pager adapter.
         *
         * @param fm the fm
         */
        public ViewPagerAdapter(FragmentManager fm) {
            super(fm);
            registeredFragments = new SparseArray<CustomFragment>();
        }

        /*
         * (non-Javadoc)
         *
         * @see android.support.v4.view.PagerAdapter#getPageTitle(int)
         */
        @Override
        public CharSequence getPageTitle(int position) {

            String strTitle = configList.get(position).getTitle();
            if (!TextUtils.isEmpty(customOptions[position])) {
                strTitle = "[" + resCode[position] + "] " + strTitle;
            }
            return strTitle;
        }

        /*
         * (non-Javadoc)
         *
         * @see android.support.v4.app.FragmentPagerAdapter#getItem(int)
         */
        @Override
        public Fragment getItem(int position) {

            CustomFragment customFragment;
            customFragment = CustomFragment.newInstance(position, getListForTag(configList.get(position).getCode(), ""));
            return customFragment;

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);
            registeredFragments.put(position, (CustomFragment) fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            registeredFragments.remove(position);
            super.destroyItem(container, position, object);
        }

        public Fragment getRegisteredFragment(int position) {
            return registeredFragments.get(position);
        }

        /*
         * (non-Javadoc)
         *
         * @see android.support.v4.view.PagerAdapter#getCount()
         */
        @Override
        public int getCount() {
            return configList.size();
        }
    }
}
