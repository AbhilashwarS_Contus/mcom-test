package com.contus.mcomm.externallibraries;
/**
 * WrapContentHeightViewPager.java
 * <p/>
 * This Class is the custom Viewpager class for Offers that wrap contents according to height.
 *
 * @category Contus
 * @package com.contus.mcomm.views
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.View;

public class WrapContentHeightViewPager extends ViewPager {
    public WrapContentHeightViewPager(Context context) {
        super(context);
    }

    /**
     * Instantiates a new wrap content height view pager.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    public WrapContentHeightViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int height = 0;
        for (int i = 0; i < getChildCount(); i++) {
            View child = getChildAt(i);
            child.measure(widthMeasureSpec,
                    MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));

            int measuredHeight = child.getMeasuredHeight();
            if (measuredHeight > height)
                height = measuredHeight;
        }

        int heightMeasure = MeasureSpec.makeMeasureSpec(height,
                MeasureSpec.EXACTLY);

        super.onMeasure(widthMeasureSpec, heightMeasure);
    }
}