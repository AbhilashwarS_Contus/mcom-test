/**
 * BusProvider.java
 * <p/>
 * This is the Helper class that is used to communicate between activity and fragments.
 *
 * @category Contus
 * @package com.contus.mcomm.bus
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.bus;

import com.squareup.otto.Bus;

public final class BusProvider {

    private static final Bus BUS = new Bus();

    /**
     * Create constructor
     */
    private BusProvider() {
    }

    /**
     * Returns the instance of the bus.
     *
     * @return new instance
     */
    public static Bus getInstance() {
        return BUS;
    }
}
