/**
 * CartAdapter.java
 * <p/>
 * This is the helper class to display categories in home page.
 *
 * @category Contus
 * @package com.contus.mcomm.adapters
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.contus.mcomm.model.CartAttribute;
import com.contus.mcomm.model.CartItem;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.views.NumberPicker;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class CartAdapter extends BaseAdapter {


    private static LayoutInflater inflater = null;
    private final Context mcontext;
    private final LIST_TYPE type;
    private final List mList;
    private final Picasso mPicasso;
    private View convertView;
    private View.OnClickListener onClickCartListener;
    private NumberPicker.OnChangedListener onChangeCartListener;
    private String currency;
    private Resources resources;
    private boolean cartError = false;
    private AdapterCallback mAdapterCallback;

    public CartAdapter(Context context, List collectionList, LIST_TYPE type) {
        this.mcontext = context;
        this.mList = collectionList;
        this.type = type;
        resources = mcontext.getResources();
        inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mPicasso = Picasso.with(mcontext);
        currency = McommApplication.getCurrencySymbol(mcontext) + Constant.SINGLE_SPACE;
        try {
            this.mAdapterCallback = ((AdapterCallback) mcontext);
        } catch (ClassCastException e) {
            throw new ClassCastException("Fragment must implement AdapterCallback.");
        }

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getView(int, android.view.View,
     * android.view.ViewGroup)
     */

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        return mList.size();
    }

    public boolean isCartError() {
        return cartError;
    }

    public void setCartError(boolean cartError) {
        this.cartError = cartError;
    }

    /*
         * (non-Javadoc)
         *
         * @see android.widget.Adapter#getItemId(int)
         */
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if (type == LIST_TYPE.CART_LIST) {
            convertView = setCartItem(position, view);
        }
        return convertView;
    }


    /**
     * Load the cart data and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setCartItem(int position, View view) {
        final CartItem cartItem = (CartItem) mList.get(position);
        convertView = view;
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_cart_item, null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.txt_item_name);
            holder.tvPrice = (TextView) convertView.findViewById(R.id.txt_price);
            holder.tvRowPrice = (TextView) convertView.findViewById(R.id.row_price);
            holder.tvOffer = (TextView) convertView.findViewById(R.id.txt_offer);
            holder.tvQuantityCount = (TextView) convertView.findViewById(R.id.quantity_count);
            holder.liConfigLayout = (LinearLayout) convertView.findViewById(R.id.config_option);
            holder.ivDelete = (ImageView) convertView.findViewById(R.id.cart_delete);
            holder.ivCart = (ImageView) convertView.findViewById(R.id.img_cart_item);
            holder.ivUpdate = (ImageView) convertView.findViewById(R.id.cart_update);
            holder.quantity = (NumberPicker) convertView.findViewById(R.id.cart_quantity);
            holder.liConfigLayout = (LinearLayout) convertView.findViewById(R.id.config_option);
            holder.liQuantityLayout = (LinearLayout) convertView.findViewById(R.id.quantity_layout);
            holder.liErrorLayout = (LinearLayout) convertView.findViewById(R.id.error_layout);
            holder.tvError = (TextView) convertView.findViewById(R.id.error_message);
            holder.ivDelete.setOnClickListener(onClickCartListener);
            holder.ivUpdate.setOnClickListener(onClickCartListener);
            holder.quantity.setOnChangeListener(onChangeCartListener);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.ivDelete.setTag(position);
        holder.ivUpdate.setTag(position);
        holder.quantity.setTag(position);

        if (cartItem.getIsStock()) {
            holder.liQuantityLayout.setVisibility(View.VISIBLE);
            holder.liErrorLayout.setVisibility(View.GONE);
            holder.quantity.setPosition(position,cartItem);
            if (Integer.parseInt(cartItem.getQty()) > (int) Float.parseFloat(cartItem.getStockQty())) {
                cartError = true;
                holder.tvError.setText(resources.getString(R.string.quantity_product_msg));
                holder.liErrorLayout.setVisibility(View.VISIBLE);
                holder.quantity.setQuantity(Integer.parseInt(cartItem.getQty()));
            } else {
                holder.liErrorLayout.setVisibility(View.GONE);
                holder.quantity.setQuantity((int) Float.parseFloat(cartItem.getStockQty()));
            }
        } else {
            cartError = true;
            holder.liQuantityLayout.setVisibility(View.GONE);
            holder.liErrorLayout.setVisibility(View.VISIBLE);
            Spannable totalText = new SpannableString(resources.getString(R.string.order_deatil_qty));
            totalText.setSpan(new ForegroundColorSpan(resources.getColor(R.color.text_primary)), 0, totalText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.tvQuantityCount.setText(totalText);
            Spannable totalString = new SpannableString(cartItem.getQty());
            totalString.setSpan(new ForegroundColorSpan(resources.getColor(R.color.text_primary)), 0, totalString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.tvQuantityCount.append(totalString);
            holder.tvError.setText(resources.getString(R.string.out_of_stock_product_msg));
        }

        holder.quantity.setIncrementValue(1);
        holder.quantity.setPosition(position, cartItem);
        holder.tvName.setText(cartItem.getName());
        holder.tvPrice.setText(currency + cartItem.getPrice());
        holder.tvRowPrice.setText(currency + cartItem.getRowTotal());
        String imgUrl = cartItem.getImageUrl();
        if (imgUrl != null && !imgUrl.isEmpty()) {
            mPicasso.load(imgUrl).placeholder(R.drawable.place_holder).
                    into(holder.ivCart);
        }

        if (cartItem.isUpdate()) {
            holder.ivUpdate.setVisibility(View.VISIBLE);
            holder.quantity.setCurrentQty(Integer.parseInt(cartItem.getUpdateQty()));
        } else {
            holder.ivUpdate.setVisibility(View.GONE);
            holder.quantity.setCurrentQty(Integer.parseInt(cartItem.getQty()));
        }

        if (cartItem.getTypeId().equalsIgnoreCase(Constant.TYPE_CONFIGURABLE)) {
            holder.liConfigLayout.removeAllViews();
            List<CartAttribute> attibute = cartItem.getConfig().getAttribute();
            for (int i = 0; i < attibute.size(); i++) {
                View cartView = inflater.inflate(R.layout.row_cart_config, null);
                TextView label = (TextView) cartView.findViewById(R.id.config_label);
                TextView value = (TextView) cartView.findViewById(R.id.config_value);
                label.setText(attibute.get(i).getLabel() + ":");
                value.setText(attibute.get(i).getValue());
                holder.liConfigLayout.addView(cartView);
            }
            holder.liConfigLayout.setVisibility(View.VISIBLE);
        } else {
            holder.liConfigLayout.setVisibility(View.GONE);
        }
        if (position + 1 == getCount())
            mAdapterCallback.onCallback();

        return convertView;
    }


    public void setOnCartClickListener(final View.OnClickListener onClickListener) {
        this.onClickCartListener = onClickListener;
    }

    public void setOnCartChangeListener(final NumberPicker.OnChangedListener onChangeListener) {
        this.onChangeCartListener = onChangeListener;

    }

    public enum LIST_TYPE {
        CART_LIST
    }

    public static interface AdapterCallback {
        void onCallback();
    }

    /**
     * listview holder class to hold the list item views
     */
    public class ViewHolder {

        TextView tvName, tvPrice, tvOffer, tvRowPrice, tvError, tvQuantityCount;
        ImageView ivCart, ivDelete, ivUpdate;
        NumberPicker quantity;
        LinearLayout liConfigLayout, liQuantityLayout, liErrorLayout;
    }
}