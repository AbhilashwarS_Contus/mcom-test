/**
 * PaymentAdapter.java
 * <p/>
 * This is the helper class to display payment method.
 *
 * @category Contus
 * @package com.contus.mcomm.adapters
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.contus.mcomm.model.Payment;
import com.contussupport.ecommerce.R;

import java.util.List;


public class PaymentAdapter extends BaseAdapter {


    private static LayoutInflater inflater = null;
    private final Context mcontext;
    private final LIST_TYPE type;
    private final List mList;
    private View convertView;
    private View.OnClickListener onClickPaymentListener;

    public PaymentAdapter(Context context, List collectionList, LIST_TYPE type) {
        this.mcontext = context;
        this.mList = collectionList;
        this.type = type;
        inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getView(int, android.view.View,
     * android.view.ViewGroup)
     */

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        return mList.size();
    }


    /*
         * (non-Javadoc)
         *
         * @see android.widget.Adapter#getItemId(int)
         */
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        if (type == LIST_TYPE.PAYMENT_LIST) {
            convertView = setPaymentItem(position, view);
        }
        return convertView;
    }


    /**
     * Load the cart data and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setPaymentItem(int position, View view) {
        final Payment item = (Payment) mList.get(position);
        convertView = view;
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_payment_item, null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.payment_name);
            holder.moreView = (RelativeLayout) convertView.findViewById(R.id.payment_view);
            convertView.setTag(holder);
            holder.moreView.setOnClickListener(onClickPaymentListener);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.moreView.setTag(position);
        holder.tvName.setText(item.getTitle());

        return convertView;
    }

    public void onClickPaymentListener(final View.OnClickListener onClickListener) {
        this.onClickPaymentListener = onClickListener;
    }


    public enum LIST_TYPE {
        PAYMENT_LIST
    }


    /**
     * listview holder class to hold the list item views
     */
    public class ViewHolder {
        TextView tvName;
        RelativeLayout moreView;
    }
}