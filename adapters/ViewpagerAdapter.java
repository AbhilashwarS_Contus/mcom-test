/**
 * ViewpagerAdapter.java
 * <p/>
 * This class is used to load the images for Gallery view in ViewPager.
 *
 * @category Contus
 * @package com.contus.mcomm.adapters
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.contus.mcomm.fragments.HomeFragment.OnHomeInteractionListener;
import com.contus.mcomm.model.Product;
import com.contussupport.ecommerce.R;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * The type Viewpager adapter.
 */

public class ViewpagerAdapter extends PagerAdapter {

    private static LayoutInflater inflater = null;
    private final List mList;
    private final Picasso mPicasso;
    private final OnHomeInteractionListener mListener;
    private int mLayout;

    public ViewpagerAdapter(Context context, List collectionList, OnHomeInteractionListener listener, int layout) {
        this.mList = collectionList;
        this.mListener = listener;
        this.mLayout = layout;
        inflater = (LayoutInflater) context
                .getSystemService(context.LAYOUT_INFLATER_SERVICE);
        mPicasso = Picasso.with(context);

    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        final Product productItem = (Product) mList.get(position);
        View vi = inflater.inflate(R.layout.offers_item, null);
        ImageView image = (ImageView) vi.findViewById(R.id.slider_image);

        String imgUrl = productItem.getImageName();
        if (imgUrl != null && !imgUrl.isEmpty()) {
            mPicasso.load(imgUrl).placeholder(R.drawable.place_holder)
                    .error(android.R.drawable.stat_notify_error)
                    .into(image);
        }

        vi.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mListener.onHomeInteraction(mLayout, productItem);
            }
        });

        // load the images using the Image Loader library.
        //Commented for future use.
        ((ViewPager) container).addView(vi, 0);

        return vi;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((RelativeLayout) object);
    }
}