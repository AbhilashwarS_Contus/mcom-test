/**
 * MyAccountAdapter.java
 * <p/>
 * This is the helper class for My Account Listviews.
 *
 * @category Contus
 * @package com.contus.mcomm.adapters
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.contus.mcomm.model.Address;
import com.contus.mcomm.model.CartAttribute;
import com.contus.mcomm.model.Country;
import com.contus.mcomm.model.Order;
import com.contus.mcomm.model.OrderItem;
import com.contus.mcomm.model.Regions;
import com.contus.mcomm.model.Wishlist;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by user on 19-May-15.
 */
public class MyAccountAdapter extends BaseAdapter {
    private static final String TAG = LogUtils.makeLogTag(MyAccountAdapter.class);
    private static LayoutInflater inflater = null;
    private final BitmapDrawable defaultBitmap;
    private List mList;
    private Context context;
    private View convertView;
    private ViewHolder holder;
    private LIST_TYPE type;
    private int height, width;
    private String currency;
    private View.OnClickListener onClickAccountListener;
    private Picasso mPicasso;
    private String orderDate;


    private boolean selection = false;

    public MyAccountAdapter(Context context, List collectionList, LIST_TYPE type) {
        this.mList = collectionList;
        this.context = context;
        this.type = type;
        mPicasso = Picasso.with(context);
        defaultBitmap = (BitmapDrawable) context.getResources().getDrawable(R.drawable.place_holder);
        height = defaultBitmap.getBitmap().getHeight();
        width = defaultBitmap.getBitmap().getWidth();
        currency = McommApplication.getCurrencySymbol(context) + Constant.SINGLE_SPACE;

    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        try {
            holder = null;
            inflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            setAdapterView(position, view);
        } catch (Exception e) {
            LogUtils.e(TAG, e.getMessage());
        }
        return convertView;
    }

    private void setAdapterView(int position, View view) {

        switch (type) {
            case WISHLIST:
                convertView = setWishListView(position, view);
                break;
            case ADDRESS:
                convertView = setAddressListView(position, view);
                break;
            case ORDERS:
                convertView = setOrdersListView(position, view);
                break;
            case ORDER_ITEMS:
                convertView = setOrderItemListView(position, view);
                break;
            case COUNTRY:
                convertView = setCountryListView(position, view);
                break;
            case REGION:
                convertView = setRegionListView(position, view);
                break;
            default:
                break;
        }
    }

    /**
     * Load the wishlist product  data and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setWishListView(int position, View view) {
        Wishlist wishObj = (Wishlist) mList.get(position);
        convertView = view;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_mywish_item, null);
            holder = new ViewHolder();

            holder.tvTitle = (TextView) convertView.findViewById(R.id.txt_product_title);
            holder.tvDiscount = (TextView) convertView.findViewById(R.id.txt_discount);
            holder.tvPrice = (TextView) convertView.findViewById(R.id.txt_price);
            holder.tvOffPrice = (TextView) convertView.findViewById(R.id.txt_off_price);
            holder.imWishDelete = (ImageView) convertView.findViewById(R.id.img_wish_delete);
            holder.imPdtWish = (ImageView) convertView.findViewById(R.id.im_pdt_wish);
            holder.tvOutofStock = (TextView) convertView.findViewById(R.id.outofstock);
            holder.imWishDelete.setOnClickListener(onClickAccountListener);
            convertView.setTag(holder);
        }
        holder = (ViewHolder) convertView.getTag();
        holder.tvTitle.setText(wishObj.getName());
        holder.tvPrice.setText(currency + wishObj.getFinalPrice());
        holder.imWishDelete.setTag(position);
        String imgUrl = wishObj.getImageUrl();
        int percentage = Utils.calculatePercentage(Float.parseFloat(wishObj.getRegularPric()), Float.parseFloat(wishObj.getFinalPrice()));
        if (percentage > 0) {
            holder.tvDiscount.setVisibility(View.VISIBLE);
            holder.tvDiscount.setText(String.valueOf(percentage) + context.getString(R.string.percentage_off));
            holder.tvOffPrice.setVisibility(View.VISIBLE);
            holder.tvOffPrice.setText(currency + wishObj.getRegularPric());
            holder.tvOffPrice.setPaintFlags(holder.tvOffPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.tvOffPrice.setVisibility(View.INVISIBLE);
            holder.tvOffPrice.setVisibility(View.GONE);
        }
        if (wishObj.getIsSaleable())
            holder.tvOutofStock.setVisibility(View.GONE);
        else
            holder.tvOutofStock.setVisibility(View.VISIBLE);

        if (imgUrl != null && !imgUrl.isEmpty()) {

            mPicasso.load(imgUrl).placeholder(R.drawable.place_holder).
                    into(holder.imPdtWish);
        }

        holder.tvOffPrice.setPaintFlags(holder.tvOffPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        return convertView;
    }

    /**
     * Load the Address list data and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setAddressListView(int position, View view) {

        convertView = view;
        Address addrObj = (Address) mList.get(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_myaddr_item, null);
            holder = new ViewHolder();
            holder.tvDelFname = (TextView) convertView.findViewById(R.id.del_fname);
            holder.tvDelLname = (TextView) convertView.findViewById(R.id.del_lname);
            holder.tvDelStreet = (TextView) convertView.findViewById(R.id.del_street);
            holder.tvDelCity = (TextView) convertView.findViewById(R.id.del_city);
            holder.tvDelCountry = (TextView) convertView.findViewById(R.id.del_ctry);
            holder.tvDelRegion = (TextView) convertView.findViewById(R.id.del_region);
            holder.tvDelPostcode = (TextView) convertView.findViewById(R.id.del_postcode);
            holder.tvDelPhone = (TextView) convertView.findViewById(R.id.del_phone);
            holder.imDelAddr = (ImageView) convertView.findViewById(R.id.ic_delete);
            holder.imEtAddr = (ImageView) convertView.findViewById(R.id.ic_edit);
            holder.imgSelection = (ImageView) convertView.findViewById(R.id.select);
            holder.imEtAddr.setOnClickListener(onClickAccountListener);
            holder.imDelAddr.setOnClickListener(onClickAccountListener);

            convertView.setTag(holder);

        }

        holder = (ViewHolder) convertView.getTag();
        holder.tvDelFname.setText(addrObj.getFirstName());
        holder.tvDelLname.setText(addrObj.getLastName());
        holder.tvDelCity.setText(addrObj.getCity());
        holder.tvDelCountry.setText(addrObj.getCountryName());
        holder.tvDelPostcode.setText(addrObj.getPostcode());
        holder.tvDelPhone.setText(addrObj.getTelephone());
        holder.tvDelRegion.setText(addrObj.getRegion());
        holder.imEtAddr.setTag(position);
        holder.imDelAddr.setTag(position);
        for (String st : addrObj.getStreet()) {
            holder.tvDelStreet.setText(st);
        }


        if (selection) {
            holder.imDelAddr.setVisibility(View.GONE);
            holder.imEtAddr.setVisibility(View.GONE);
            holder.imgSelection.setVisibility(View.VISIBLE);
        } else {
            holder.imDelAddr.setVisibility(View.VISIBLE);
            holder.imEtAddr.setVisibility(View.VISIBLE);
            holder.imgSelection.setVisibility(View.GONE);
            if (addrObj.getIsDefaultBilling() || addrObj.getIsDefaultShipping()) {
                holder.imDelAddr.setVisibility(View.GONE);
            } else {
                holder.imDelAddr.setVisibility(View.VISIBLE);
            }
        }
        return convertView;
    }

    /**
     * Load the My Orders list data and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setOrdersListView(int position, View view) {

        convertView = view;
        Order ordrObj = (Order) mList.get(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_myord_items, null);
            holder = new ViewHolder();
            holder.linear = (LinearLayout) convertView.findViewById(R.id.order_information_layout);
            holder.tvOrdIdNum = (TextView) convertView.findViewById(R.id.order_id_num);
            holder.tvOrdDate = (TextView) convertView.findViewById(R.id.order_date);
            holder.tvNumOfItems = (TextView) convertView.findViewById(R.id.no_of_items_result);
            holder.tvStatus = (TextView) convertView.findViewById(R.id.status_result);
            holder.tvTotal = (TextView) convertView.findViewById(R.id.total_result);
            holder.imArrow = (ImageView) convertView.findViewById(R.id.ic_arrow);
            convertView.setTag(holder);

        }
        holder = (ViewHolder) convertView.getTag();

        holder.tvOrdIdNum.setText(ordrObj.getOrderId());
        orderDate = Utils.changeDateFormat(ordrObj.getOrderDate(), true);
        holder.tvOrdDate.setText(orderDate);
        holder.tvNumOfItems.setText(ordrObj.getItemCount());
        holder.tvStatus.setText(ordrObj.getStatus());
        holder.tvTotal.setText(currency + ordrObj.getGrandTotal());
        return convertView;
    }

    /**
     * Load the My Orders Items list data and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setOrderItemListView(int position, View view) {

        convertView = view;
        OrderItem ordrItemObj = (OrderItem) mList.get(position);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row_myord_items_list, null);
            holder = new ViewHolder();
            holder.tvOrdItemName = (TextView) convertView.findViewById(R.id.txt_order_item_title);
            holder.tvOrdItemPrice = (TextView) convertView.findViewById(R.id.txt_ordr_price);
            holder.tvOrdItemQty = (TextView) convertView.findViewById(R.id.txt_quantity);
            holder.liConfigLayout = (LinearLayout) convertView.findViewById(R.id.config_option);
            holder.imOrdrItem = (ImageView) convertView.findViewById(R.id.im_ordr_item);
            convertView.setTag(holder);

        }
        holder = (ViewHolder) convertView.getTag();

        holder.tvOrdItemName.setText(ordrItemObj.getName());
        holder.tvOrdItemPrice.setText(currency + ordrItemObj.getPrice());
        holder.tvOrdItemQty.setText(context.getString(R.string.order_deatil_qty) + Constant.SINGLE_SPACE + ordrItemObj.getQty());
        String imgUrl = ordrItemObj.getImage();
        if (imgUrl != null && !imgUrl.isEmpty()) {

            mPicasso.load(imgUrl).placeholder(R.drawable.place_holder).
                    resize(width, height).into(holder.imOrdrItem);
        }
        List<CartAttribute> attibute = ordrItemObj.getAttribute();

        if (!attibute.isEmpty()) {
            holder.liConfigLayout.removeAllViews();
            for (int i = 0; i < attibute.size(); i++) {
                View cartView = inflater.inflate(R.layout.row_cart_config, null);
                TextView attrLabel = (TextView) cartView.findViewById(R.id.config_label);
                TextView attrValue = (TextView) cartView.findViewById(R.id.config_value);
                attrLabel.setText(attibute.get(i).getLabel() + ":");
                attrValue.setText(attibute.get(i).getValue());
                holder.liConfigLayout.addView(cartView);
            }
            holder.liConfigLayout.setVisibility(View.VISIBLE);
        } else {
            holder.liConfigLayout.setVisibility(View.GONE);
        }

        return convertView;
    }

    /**
     * Load the category product  data and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setCountryListView(int position, View view) {

        convertView = view;
        Country countryObj = (Country) mList.get(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.popup, null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.value);
            holder.ivIndicator = (ImageView) convertView.findViewById(R.id.selection_indiacator);
            convertView.setTag(holder);

        }
        holder = (ViewHolder) convertView.getTag();

        holder.tvName.setText(countryObj.getName());

        holder.ivIndicator.setVisibility(View.GONE);
        return convertView;
    }

    /**
     * Load the category product  data and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setRegionListView(int position, View view) {

        convertView = view;
        Regions regionObj = (Regions) mList.get(position);

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.popup, null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.value);
            holder.ivIndicator = (ImageView) convertView.findViewById(R.id.selection_indiacator);
            convertView.setTag(holder);

        }
        holder = (ViewHolder) convertView.getTag();

        holder.tvName.setText(regionObj.getName());
        holder.ivIndicator.setVisibility(View.GONE);
        return convertView;
    }

    public void setOnClickAccountListener(final View.OnClickListener onClickListener) {
        this.onClickAccountListener = onClickListener;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    public void setSelection(boolean selection) {
        this.selection = selection;
    }

    public enum LIST_TYPE {
        WISHLIST,
        ADDRESS,
        ORDERS, ORDER_ITEMS, COUNTRY, REGION
    }

    /**
     * listview holder class to hold the list item views
     */
    public class ViewHolder {

        TextView tvTitle, tvPrice, tvDiscount, tvOffPrice, tvOrdItemName, tvOrdItemQty, tvOrdItemPrice, tvOutofStock;
        ImageView imPdtWish, imWishDelete, imOrdrItem, imgSelection;

        TextView tvDelFname, tvDelLname, tvDelStreet, tvDelCity, tvDelCountry, tvDelRegion, tvDelPostcode, tvDelPhone;

        TextView tvOrdIdNum, tvOrdDate, tvNumOfItems, tvStatus, tvTotal, tvName;

        ImageView imDelAddr, imEtAddr, imArrow, ivIndicator;
        LinearLayout linear, liConfigLayout;
    }
}
