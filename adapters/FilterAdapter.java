/**
 * FilterAdapter.java
 * <p/>
 * Comments.
 *
 * @category Contus
 * @package com.contus.mcomm.adapters
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.contus.mcomm.model.FilterValues;
import com.contussupport.ecommerce.R;

import java.util.List;


/**
 * Created by user on 28-May-15.
 */
public class FilterAdapter extends BaseAdapter {
    private Context mContext;
    private List<FilterValues> mList;
    private LayoutInflater inflater = null;
    private String[] selectors;

    public FilterAdapter(Context context, List<FilterValues> collectionList, String[] selections) {

        mContext = context;
        this.mList = collectionList;
        this.selectors = selections;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View viewItem = convertView;
        ListViewHolder holder;
        if (convertView == null) {
            viewItem = inflater.inflate(R.layout.filter_item, null);
            holder = new ListViewHolder();
            holder.tvTitle = (TextView) viewItem
                    .findViewById(R.id.filter_title);
            holder.cboxSelector = (CheckBox) viewItem
                    .findViewById(R.id.checkBoxSelector);
            viewItem.setTag(holder);

        }
        holder = (ListViewHolder) viewItem.getTag();

        final FilterValues values = mList.get(position);
        holder.tvTitle.setText(values.getLabel());
        if (TextUtils.isEmpty(selectors[position]))
            holder.cboxSelector.setChecked(false);
        else
            holder.cboxSelector.setChecked(true);

        holder.cboxSelector.setTag(position);

        holder.cboxSelector.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                CheckBox selector = (CheckBox) v;

                setChecked(position, selector.isChecked());

            }
        });
        return viewItem;
    }

    private void setChecked(int position, boolean selected) {
        if (selected)
            selectors[position] = mList.get(position).getValue();
        else
            selectors[position] = "";
    }

    /**
     * listview holder class to hold the list item views
     */
    public class ListViewHolder {

        TextView tvTitle;
        CheckBox cboxSelector;

    }
}
