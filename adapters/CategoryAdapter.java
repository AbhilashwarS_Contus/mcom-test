/**
 * CategoryAdapter.java
 * <p/>
 * This is the helper class to display categories in home page.
 *
 * @category Contus
 * @package com.contus.mcomm.adapters
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.contus.mcomm.model.Category;
import com.contus.mcomm.utils.LogUtils;
import com.contussupport.ecommerce.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class CategoryAdapter extends BaseAdapter {

    private static LayoutInflater inflater = null;
    private final Context mcontext;
    private final LIST_TYPE type;
    private final List<Category> mList;
    private final Picasso mPicasso;
    private View convertView;


    public CategoryAdapter(Context context, List collectionList, LIST_TYPE type) {
        this.mcontext = context;
        this.mList = collectionList;
        this.type = type;
        inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mPicasso = Picasso.with(mcontext);
    }

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {
        return mList.size();
    }

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getView(int, android.view.View,
     * android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        switch (type) {
            case MAIN_CATEGORY:
                convertView = setCategory(position, view);
                break;
            case SUB_CATEGORY:
                convertView = setChildCategory(position, view);
                break;
            case NAVIGATION:
                convertView = setNavigation(position, view);
                break;
            default:
                break;
        }

        return convertView;
    }

    /**
     * Load the category data and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setCategory(int position, View view) {
        Category catItem = mList.get(position);

        convertView = view;
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.category_item, null);
            holder = new ViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.category_name);
            holder.ivCategory = (ImageView) convertView.findViewById(R.id.category_image);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        String catImgUrl = catItem.getImageName();
        if (catImgUrl != null && !catImgUrl.isEmpty()) {
            mPicasso.load(catImgUrl).placeholder(R.drawable.place_holder)
                    .into(holder.ivCategory);
        }

        holder.tvName.setText(catItem.getName());
        return convertView;
    }

    /**
     * Load the sub category data and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setChildCategory(int position, View view) {
        Category catItem =  mList.get(position);
        convertView = view;
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_fragment_category_row, null);
            holder = new ViewHolder();
            holder.tvSubCatName = (TextView) convertView.findViewById(R.id.category_row_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvSubCatName.setText(catItem.getName());
        return convertView;
    }

    /**
     * Load the Navigation Listview and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setNavigation(int position, View view) {
        Category catItem =  mList.get(position);
        convertView = view;
        final ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.navigation_item, null);
            holder = new ViewHolder();
            holder.txtItem = (TextView) convertView.findViewById(R.id.txt_item);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txtItem.setText(catItem.getName());
        return convertView;
    }

    public enum LIST_TYPE {
        MAIN_CATEGORY,
        SUB_CATEGORY,
        NAVIGATION
    }

    /**
     * listview holder class to hold the list item views
     */
    public class ViewHolder {

        TextView tvName, tvSubCatName, txtItem;
        ImageView ivCategory;
    }
}