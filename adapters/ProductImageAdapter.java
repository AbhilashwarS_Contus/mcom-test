package com.contus.mcomm.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.contus.mcomm.model.CustomAttribute;
import com.contus.mcomm.utils.LogUtils;
import com.contussupport.ecommerce.R;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by contus on 4/21/2015.
 */
public class ProductImageAdapter extends BaseAdapter {
    private static final String TAG = LogUtils.makeLogTag(ProductImageAdapter.class);
    //sample images for viewpager
    private static LayoutInflater inflater = null;
    private List mList;
    private Context context;
    private Picasso mPicasso;
    private int imgPosition;
    private LIST_TYPE type;
    private View convertView;
    private ViewHolder holder;

    public ProductImageAdapter(Context context, List collectionList, int position, LIST_TYPE type) {
        this.context = context;
        mPicasso = Picasso.with(context);
        this.mList = collectionList;
        imgPosition = position;
        this.type = type;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mList.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {

        try {
            holder = null;
            inflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            setAdapterView(position, view);
        } catch (Exception e) {
            LogUtils.e(TAG, e.getMessage());
        }
        return convertView;

    }

    private void setAdapterView(int position, View view) {

        switch (type) {
            case IMAGES_LIST:
                convertView = setImageListView(position, view);
                break;
            case CUSTOM_VIEW:
                convertView = setCustomListView(position, view);
                break;
            case SORT:
                convertView = setSortListView(position, view);
                break;
            default:
                break;
        }
    }

    /**
     * Load the category product  data and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setImageListView(int position, View view) {

        convertView = view;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.product_image_item, null);
            holder = new ViewHolder();
            holder.images = (ImageView) convertView.findViewById(R.id.product_gallery_image);

            convertView.setTag(holder);

        }
        holder = (ViewHolder) convertView.getTag();

        String imgUrl = (String) mList.get(position);
        if (imgUrl != null && !imgUrl.isEmpty()) {

            mPicasso.load(imgUrl).placeholder(R.drawable.place_holder)
                    .into(holder.images);
        }
        if (imgPosition == position) {
            holder.images.setBackgroundResource(R.drawable.bg_outofstock);
            convertView.setBackgroundColor(context.getResources().getColor(android.R.color.transparent));
        }
        return convertView;
    }

    
    private View setCustomListView(int position, View view) {
        CustomAttribute customItem = (CustomAttribute) mList.get(position);

        convertView = view;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.custom_tab_item, null);
            holder.tvAttribute = (TextView) convertView.findViewById(R.id.attribute_item);
            holder.selectionIndicator = (View) convertView.findViewById(R.id.selection_indiacator);
            convertView.setTag(holder);
        }

        holder = (ViewHolder) convertView.getTag();

        holder.tvAttribute.setVisibility(View.VISIBLE);
        holder.tvAttribute.setText(customItem.getLabel());

        if (imgPosition == Integer.parseInt(customItem.getConfigId())) {
            holder.selectionIndicator.setVisibility(View.VISIBLE);
        } else {
            holder.selectionIndicator.setVisibility(View.GONE);
        }

        if (customItem.getIsStock().equals("0"))
            holder.tvAttribute.setAlpha(0.4f);
        else
            holder.tvAttribute.setAlpha(1.0f);
        return convertView;
    }

    
    private View setSortListView(int position, View view) {

        convertView = view;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.sort_item, null);
            holder = new ViewHolder();
            holder.tvSort = (TextView) convertView.findViewById(R.id.sort_value);
            holder.ivIndicator = (ImageView) convertView.findViewById(R.id.selection_indiacator);
            convertView.setTag(holder);

        }
        holder = (ViewHolder) convertView.getTag();

        String sortType = (String) mList.get(position);

        holder.tvSort.setText(sortType);
        if (imgPosition == position) {
            holder.ivIndicator.setVisibility(View.VISIBLE);
        } else {
            holder.ivIndicator.setVisibility(View.GONE);
        }
        return convertView;
    }

    public void setPosition(int position) {
        imgPosition = position;
    }

    public void swapList(List swapList) {
        this.mList = swapList;
    }

    public enum LIST_TYPE {
        IMAGES_LIST,
        CUSTOM_VIEW,
        SORT
    }

    /**
     * listview holder class to hold the list item views
     */
    public class ViewHolder {
        ImageView images, ivIndicator;
        TextView tvAttribute, tvSort;
        View selectionIndicator;
    }

}
