/**
 * ProductListAdapter.java
 * <p/>
 * This is the Helper class that is used to display New Arrivals Product in Horizontal Listview.
 *
 * @category Contus
 * @package com.contus.mcomm.adapters
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

package com.contus.mcomm.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.contus.mcomm.model.Product;
import com.contus.mcomm.model.Wishlistdb;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contus.mcomm.utils.Utils;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class ProductListAdapter extends BaseAdapter {

    private static final String TAG = LogUtils.makeLogTag(ProductListAdapter.class);
    private static LayoutInflater inflater = null;
    private LIST_TYPE type;
    private Context mContext;
    private List mList;
    /**
     * The holder.
     */
    private ListViewHolder holder;
    /**
     * The convert view.
     */
    private Picasso mPicasso;
    private View convertView;
    private int height, width;
    private BitmapDrawable defaultBitmap;
    private View.OnClickListener onClickWislistListener;
    private List<Wishlistdb> mWishlist;
    private String currency;

    public ProductListAdapter(Context context, List collectionList, LIST_TYPE type) {
        this.mContext = context;
        this.mList = collectionList;
        this.type = type;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mPicasso = Picasso.with(mContext);
        defaultBitmap = (BitmapDrawable) mContext.getResources().getDrawable(R.drawable.place_holder);
        height = defaultBitmap.getBitmap().getHeight();
        width = defaultBitmap.getBitmap().getWidth();
        currency = McommApplication.getCurrencySymbol(mContext)+ Constant.SINGLE_SPACE;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getCount()
     */
    @Override
    public int getCount() {

        return mList.size();
    }

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getItem(int)
     */
    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getItemId(int)
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /*
     * (non-Javadoc)
     *
     * @see android.widget.Adapter#getView(int, android.view.View,
     * android.view.ViewGroup)
     */
    @Override
    public View getView(int position, View view, ViewGroup parent) {

        try {
            holder = null;
            inflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            setAdapterView(position, view);
        } catch (Exception e) {
            LogUtils.i(TAG, e.getMessage());
        }
        return convertView;


    }

    private void setAdapterView(int position, View view) {

        switch (type) {
            case NEW_PRODUCT:
                convertView = setNewArrivalView(position, view);
                break;
            case PRODUCT:
                convertView = setProductListView(position, view);
                break;
            case BESTSELLER:
                break;

            default:
                break;
        }
    }


    /**
     * Load the New Arrival data and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setNewArrivalView(int position, View view) {
        Product productItem = (Product) mList.get(position);
        convertView = view;
        if (convertView == null) {
            holder = new ListViewHolder();
            convertView = inflater.inflate(R.layout.row_newarivals_item, null);
            holder.tvName = (TextView) convertView.findViewById(R.id.new_arrivals_name);
            holder.ivCategory = (ImageView) convertView.findViewById(R.id.new_arrivals_image);
            holder.tvPrice = (TextView) convertView.findViewById(R.id.originalprice);
            holder.tvfinalPrice = (TextView) convertView.findViewById(R.id.finalprice);
            convertView.setTag(holder);
        }

        holder = (ListViewHolder) convertView.getTag();

        String imgUrl = productItem.getImageName();
        if (imgUrl != null && !imgUrl.isEmpty()) {

            mPicasso.load(imgUrl).placeholder(R.drawable.place_holder).into(holder.ivCategory);
        }
        holder.ivCategory.getLayoutParams().height = height;
        holder.ivCategory.getLayoutParams().width = width;
        holder.ivCategory.requestLayout();
        holder.tvName.setText(productItem.getName());
        holder.tvfinalPrice.setText(currency + productItem.getSpecialPrice());
        int percentage = Utils.calculatePercentage(Float.parseFloat(productItem.getPrice()), Float.parseFloat(productItem.getSpecialPrice()));
        if (percentage > 0) {
            holder.tvPrice.setVisibility(View.VISIBLE);
            holder.tvPrice.setText(currency + productItem.getPrice());
            holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.tvPrice.setVisibility(View.GONE);
        }

        return convertView;
    }

    /**
     * Load the product list data and display the specified position in the view.
     *
     * @param position the position
     * @param view     the view
     * @return view
     */
    private View setProductListView(int position, View view) {
        Product productItem = (Product) mList.get(position);
        convertView = view;
        holder = null;
        if (convertView == null) {
            holder = new ListViewHolder();
            convertView = inflater.inflate(R.layout.row_productlist_item, null);
            holder = new ListViewHolder();
            holder.tvName = (TextView) convertView.findViewById(R.id.product_title);
            holder.tvPrice = (TextView) convertView.findViewById(R.id.originalprice);
            holder.tvfinalPrice = (TextView) convertView.findViewById(R.id.finalprice);
            holder.ivProductImage = (ImageView) convertView.findViewById(R.id.product_image);
            holder.ivWishList = (ImageView) convertView.findViewById(R.id.wishlist);
            holder.tvOfferPercentage = (TextView) convertView.findViewById(R.id.offer_percentage);
            holder.tvOutofStock = (TextView) convertView.findViewById(R.id.outofstock);
            holder.ivWishList.setOnClickListener(this.onClickWislistListener);
            convertView.setTag(holder);
        }

        holder = (ListViewHolder) convertView.getTag();
        holder.ivWishList.setTag(position);
        holder.tvfinalPrice.setText(currency + productItem.getSpecialPrice());
        holder.tvName.setText(productItem.getName());
        String imgUrl = productItem.getImageName();
        int percentage = Utils.calculatePercentage(Float.parseFloat(productItem.getPrice()), Float.parseFloat(productItem.getSpecialPrice()));
        if (percentage > 0) {
            holder.tvOfferPercentage.setVisibility(View.VISIBLE);
            holder.tvOfferPercentage.setText(String.valueOf(percentage) + mContext.getString(R.string.percentage_off));
            holder.tvPrice.setVisibility(View.VISIBLE);
            holder.tvPrice.setText(currency + productItem.getPrice());
            holder.tvPrice.setPaintFlags(holder.tvPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        } else {
            holder.tvOfferPercentage.setVisibility(View.INVISIBLE);
            holder.tvPrice.setVisibility(View.GONE);
        }
        if (imgUrl != null && !imgUrl.isEmpty()) {

            mPicasso.load(imgUrl).placeholder(R.drawable.place_holder)
                    .into(holder.ivProductImage);
        }

        if (productItem.getIsSaleable())
            holder.tvOutofStock.setVisibility(View.GONE);
        else
            holder.tvOutofStock.setVisibility(View.VISIBLE);


        if (mWishlist.get(position).getIsWishList())
            holder.ivWishList.setImageResource(R.drawable.ic_wishlist_active);
        else
            holder.ivWishList.setImageResource(R.drawable.ic_wishlist_inactive);
        return convertView;
    }

    public void setOnWishlistClickListener(final View.OnClickListener onClickListener) {
        this.onClickWislistListener = onClickListener;
    }

    public void swapProductList(List productList) {
        this.mList = productList;
    }

    public void setWishlistdb(List wishlistdb) {
        this.mWishlist = wishlistdb;
    }

    public enum LIST_TYPE {
        NEW_PRODUCT,
        PRODUCT,
        BESTSELLER

    }

    /**
     * listview holder class to hold the list item views
     */
    static class ListViewHolder {

        TextView tvName, tvPrice, tvfinalPrice, tvOfferPercentage, tvOutofStock, tvCurrency;
        ImageView ivCategory, ivProductImage, ivWishList;

    }
}


