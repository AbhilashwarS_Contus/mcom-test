/**
 * @category Glimmy_M_New
 * @package com.magna.glimym.server
 * @version 1.0
 * @author ContusTeam <developers@contus.in>
 * @copyright Copyright (C) 2015 <Contus>. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.service;

/**
 * The listener interface for receiving onParsingCompletion events. The class
 * that is interested in processing a onParsingCompletion event implements this
 * interface, and the object created with that class is registered with a
 * component using the component's
 * <code>addOnParsingCompletionListener<code> method. When
 * the onParsingCompletion event occurs, that object's appropriate
 * method is invoked.
 *
 * @see OnParsingCompletionListener
 */
public interface OnParsingCompletionListener {

    /**
     * On parsing completion.
     */
    public void onParsingCompletion();

    public void onParsingCompletionError(String error);

}

