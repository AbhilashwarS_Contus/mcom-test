package com.contus.mcomm.service;

public interface OnTaskCompletionListener {

    /**
     * On parsing completion.
     */
    public void onTaskCompletion();

    public void onTaskCompletionError(String error);

}
