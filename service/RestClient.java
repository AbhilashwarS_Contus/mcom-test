/**
 * RestClient.java
 * The Class is the adapter class for server request and response . used okhttp client for request and gson for parsing the data .
 *
 * @category Contus
 * @package contus.com.mcomm.service
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.service;


import android.content.Context;

import com.contus.mcomm.model.CartConfigInfo;
import com.contus.mcomm.utils.Constant;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

public class RestClient {

    private static API REST_CLIENT;

    static {
        setupRestClient();
    }

    private static Context mContext;
    private static final RestClient restClient = new RestClient(mContext);

    /**
     * Rest client constructor which is called when the object created.
     *
     * @param context
     */
    public RestClient(Context context) {
        mContext = context;
    }

    /**
     * To build the client and converter to rest adapter.
     */
    private static void setupRestClient() {
        Gson gson = new GsonBuilder()
                .registerTypeAdapter(CartConfigInfo.class, new Deserializer())
                .setFieldNamingPolicy(
                        FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constant.BASEURL) //Api url
                .setClient(new OkClient(new OkHttpClient())) // Ok http client used for requests.
                .setConverter(new GsonConverter(gson)) // Serialize and deserialize object by using gson
                .setLogLevel(RestAdapter.LogLevel.FULL).build();

        REST_CLIENT = restAdapter.create(API.class);
    }

    /**
     * Returns a new instance of this rest client.
     *
     * @return rest client
     */
    public RestClient getInstance() {
        return restClient;
    }

    /**
     * Returns the api interface.
     *
     * @return
     */
    public API get() {
        return REST_CLIENT;
    }

}