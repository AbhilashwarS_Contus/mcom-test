/**
 * BackgroundTaskCallback.java
 * This is the interface class which used to save category and product and return to the invoked class.
 *
 * @package contus.com.mcomm.service
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.service;


import android.content.Context;
import android.os.AsyncTask;

import com.contus.mcomm.database.CountryExecutor;
import com.contus.mcomm.model.Country;
import com.contus.mcomm.model.CountryResponse;
import com.contus.mcomm.utils.LogUtils;

import java.util.List;

public class BackgroundTaskCallback extends AsyncTask<Void, Void, Void> {
    private static final String TAG = LogUtils.makeLogTag(BackgroundTaskCallback.class);
    /**
     * The article tags.
     */
    private final Object collection;
    private final CountryExecutor countryExe;
    /**
     * The listener.
     */
    private OnTaskCompletionListener listener;

    private TASK_TYPE type;


    public BackgroundTaskCallback(Context ctx, Object result, TASK_TYPE type) {
        this.collection = result;
        this.type = type;
        countryExe = new CountryExecutor(ctx);
    }

    /**
     * Gets the listener.
     *
     * @return the listener
     */
    public OnTaskCompletionListener getListener() {
        return listener;
    }

    /**
     * Sets the listener.
     *
     * @param listener the new listener
     */
    public void setListener(OnTaskCompletionListener listener) {
        this.listener = listener;
    }

    /**
     * Start.
     */
    public void start() {
        this.execute();
    }

    /**
     * Stop.
     */
    public void stop() {
        this.cancel(true);
    }

    @Override
    protected Void doInBackground(Void... params) {

        try {
            if (this.type == TASK_TYPE.COUNTRY) {
                CountryResponse country = (CountryResponse) collection;
                saveCountry(country.getCountries());
            }
        } catch (Exception e) {
            this.listener.onTaskCompletionError(e.getMessage());
            LogUtils.i(TAG, e.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        this.listener.onTaskCompletion();
    }

    /**
     * Save country.
     *
     * @param country the Country list
     */
    private void saveCountry(List<Country> country) {

        try {
            countryExe.insertData(country);
        } catch (NullPointerException e) {
            LogUtils.i(TAG, e.getMessage());
        }

    }

    /**
     * Sets the on parsing completion listener.
     *
     * @param listener the new on parsing completion listener
     */
    public void setOnTaskCompletionListener(
            OnTaskCompletionListener listener) {
        this.listener = listener;
    }

    public enum TASK_TYPE {

        COUNTRY
    }

}
