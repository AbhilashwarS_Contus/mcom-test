/**
 * RestCallback.java
 * The Class is the callback class for maintain the cilent response due to the failure and success.
 *
 * @category Contus
 * @package contus.com.mcomm.service
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.service;


import android.content.Context;

import com.contus.mcomm.model.ErrorResponse;
import com.contussupport.ecommerce.R;

import org.xml.sax.ErrorHandler;

import retrofit.RetrofitError;

public abstract class CustomErrorHandler implements ErrorHandler {
    private final Context ctx;

    public CustomErrorHandler(Context ctx) {
        this.ctx = ctx;
    }


    // @Override
    public Throwable handleError(RetrofitError cause) {

        String errorDescription;
        try {
            ErrorResponse errorResponse = (ErrorResponse) cause.getBodyAs(ErrorResponse.class);
            errorDescription = errorResponse.message;
        } catch (Exception ex) {
            errorDescription = cause.getLocalizedMessage();
        }


        return new Exception(errorDescription);
    }

    private String errorHandling(RetrofitError cause) {
        String errorDescription = "";
        switch (cause.getKind()) {
            case HTTP:
                errorDescription = ctx.getString(R.string.error_network_http_error, cause.getResponse().getStatus());
                break;

            case NETWORK:
                errorDescription = ctx.getString(R.string.error_network);
                break;

            case CONVERSION:
                break;
            case UNEXPECTED:
                throw cause;

            default:
                errorDescription = ctx.getString(R.string.error_unknown);
        }
        return errorDescription;
    }
}