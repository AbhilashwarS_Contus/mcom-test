/**
 * DbParser.java
 * This is the interface class which used to save category and product and return to the invoked class.
 *
 * @category Contus
 * @package contus.com.mcomm.service
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.service;


import android.content.Context;
import android.os.AsyncTask;

import com.contus.mcomm.database.CategoryExecutor;
import com.contus.mcomm.database.ProductExecutor;
import com.contus.mcomm.database.StoreExecutor;
import com.contus.mcomm.database.WishlistdbExecutor;
import com.contus.mcomm.model.Category;
import com.contus.mcomm.model.Product;
import com.contus.mcomm.model.ProductResponse;
import com.contus.mcomm.model.Response;
import com.contus.mcomm.model.Store;
import com.contus.mcomm.utils.Constant;
import com.contus.mcomm.utils.LogUtils;
import com.contussupport.ecommerce.McommApplication;

import java.util.List;

public class DbParser extends AsyncTask<Void, Void, String> {
    private static final String TAG = LogUtils.makeLogTag(DbParser.class);
    /**
     * The ctx.
     */
    private final Context ctx;


    /**
     * The article tags.
     */
    private final Object collection;
    private final ProductExecutor productExe;
    private String wishListId;
    /**
     * The listener.
     */
    private OnParsingCompletionListener listener;
    /**
     * The Enum PARSE_TYPE.
     */
    /**
     * The parse type.
     */
    private PARSE_TYPE parseType;


    public DbParser(Context ctx, Object result, PARSE_TYPE type) {
        this.ctx = ctx;
        this.collection = result;
        this.parseType = type;
        productExe = new ProductExecutor(ctx);
    }

    public DbParser(Context ctx, String id, PARSE_TYPE type) {
        this.ctx = ctx;
        this.parseType = type;
        this.collection = null;
        this.wishListId = id;
        productExe = new ProductExecutor(ctx);
    }

    /**
     * Gets the listener.
     *
     * @return the listener
     */
    public OnParsingCompletionListener getListener() {
        return listener;
    }

    /**
     * Sets the listener.
     *
     * @param listener the new listener
     */
    public void setListener(OnParsingCompletionListener listener) {
        this.listener = listener;
    }

    /**
     * Start.
     */
    public void start() {
        this.execute();
    }

    /**
     * Stop.
     */
    public void stop() {
        this.cancel(true);
    }

    @Override
    protected String doInBackground(Void... params) {
        String response = "";
        try {
            switch (this.parseType) {
                case HOME:
                    response = parseGetall();
                    break;
                case PRODUCT:
                    response = parseProduct();
                    break;
                case PRODUCT_DELETE:
                    productExe.deleteProductByType(Constant.CATEGORY_PRODUCT);
                    break;
                case WISHLIST_DELETE:
                    new WishlistdbExecutor(ctx).deleteAll();
                    break;
                case WISHLIST_INSERT:
                    response = saveWishlistda();
                    break;
                case WISHLIST_UPDATE:
                    new WishlistdbExecutor(ctx).updateWishlist(wishListId, false);
                    break;
                default:
                    break;
            }

        } catch (Exception e) {
            this.listener.onParsingCompletionError(e.getMessage());
            LogUtils.i(TAG, e.getMessage());
        }
        return response;
    }

    private String saveWishlistda() {
        ProductResponse productCollection = (ProductResponse) collection;
        new WishlistdbExecutor(ctx).insertData(productCollection.products);
        return null;
    }

    private String parseProduct() {
        ProductResponse productCollection = (ProductResponse) collection;
        saveProduct(productCollection.products, Constant.CATEGORY_PRODUCT);
        return null;
    }

    private String parseGetall() {

        Response homeResponse = (Response) collection;
        if (!homeResponse.categories.isEmpty())
            saveCategory(homeResponse.categories);
        productExe.deleteAll(); // to delete all productdao
        if (!homeResponse.product.isEmpty())
            saveProduct(homeResponse.product, Constant.NEW_PRODUCT);
        if (!homeResponse.offer.isEmpty())
            saveProduct(homeResponse.offer, Constant.OFFER_PRODUCT);
        if (!homeResponse.stores.isEmpty())
            saveStore(homeResponse.stores);
        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        this.listener.onParsingCompletion();
    }

    /**
     * Save store.
     *
     * @param store the store array
     */
    private void saveStore(List<Store> store) {

        try {
            new StoreExecutor(ctx).insertData(store);

        } catch (NullPointerException e) {
            LogUtils.i(TAG, e.getMessage().toString());
        }

    }

    /**
     * Save product.
     *
     * @param product the product array
     */
    private void saveProduct(List<Product> product, int typeId) {

        try {
            productExe.insertData(product, typeId);
        } catch (NullPointerException e) {
            LogUtils.i(TAG, e.getMessage().toString());
        }

    }

    /**
     * Save category.
     *
     * @param categories the category array
     */
    private void saveCategory(List<Category> categories) {

        try {
            new CategoryExecutor(ctx).insertData(categories);

        } catch (NullPointerException e) {
            LogUtils.i(TAG, e.getMessage().toString());
        }

    }

    /**
     * Sets the on parsing completion listener.
     *
     * @param listener the new on parsing completion listener
     */
    public void setOnParsingCompletionListener(
            OnParsingCompletionListener listener) {
        this.listener = listener;
    }

    public enum PARSE_TYPE {

        /**
         * The parse get all.
         */
        HOME,
        /**
         * The parse live updates.
         */
        PRODUCT,
        PRODUCT_DELETE,
        WISHLIST_UPDATE,
        WISHLIST_DELETE,
        WISHLIST_INSERT
    }

}
