/**
 * API.java
 * The Class used to maintain the request method such as GET,PUT and POST , and also define the endpoint.
 *
 * @category Contus
 * @package contus.com.mcomm.service
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.service;

import com.contus.mcomm.model.CartConfigInfo;
import com.contus.mcomm.utils.Constant;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class Deserializer implements JsonDeserializer {

    @Override
    public CartConfigInfo deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
            throws JsonParseException {
        JsonObject content = je.getAsJsonObject();
        CartConfigInfo config = new Gson().fromJson(je, type);
        JsonElement option = content.get(Constant.Cart.CART_ATTR);
        config.setSuperAttribute(new Gson().toJson(option));

        JsonElement customOption = content.get(Constant.Cart.OPTIONS);
        config.setOptions(new Gson().toJson(customOption));
        return config;

    }
}