/**
 * API.java
 * The Class used to maintain the request method such as GET,PUT and POST , and also define the endpoint.
 *
 * @category Contus
 * @package contus.com.mcomm.service
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.service;

import com.contus.mcomm.model.AddressResponse;
import com.contus.mcomm.model.CartItemResponse;
import com.contus.mcomm.model.CartResponse;
import com.contus.mcomm.model.CartUpdateResponse;
import com.contus.mcomm.model.ChangeResponse;
import com.contus.mcomm.model.CheckoutResponse;
import com.contus.mcomm.model.CountryResponse;
import com.contus.mcomm.model.CouponResponse;
import com.contus.mcomm.model.DeleteAddress;
import com.contus.mcomm.model.FilterResponse;
import com.contus.mcomm.model.ForgotResponse;
import com.contus.mcomm.model.LoginResponse;
import com.contus.mcomm.model.OrderDetailResponse;
import com.contus.mcomm.model.OrderResponse;
import com.contus.mcomm.model.PlaceOrderResponse;
import com.contus.mcomm.model.ProductDetailResponse;
import com.contus.mcomm.model.ProductResponse;
import com.contus.mcomm.model.RegionResponse;
import com.contus.mcomm.model.Response;
import com.contus.mcomm.model.ReviewListResponse;
import com.contus.mcomm.model.ReviewResponse;
import com.contus.mcomm.model.StaticPageResponse;
import com.contus.mcomm.model.WishlistResponse;
import com.contus.mcomm.utils.Constant;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.DELETE;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Handle the request api with annotations , methods and parameter.
 */
public interface API {

    @GET("/api")
    void getHomePage(@Query(Constant.Request.STORE_ID) String storeId, @Query(Constant.Request.WEBSITE_ID) String websiteId, @Query(Constant.Request.ACTION) String action,
                     Callback<Response> callback);

    @FormUrlEncoded
    @POST("/api")
    void getProductList(@FieldMap Map<String, String> parameters, Callback<ProductResponse> callback);

    @GET("/api")
    void getProductDetails(@Query(Constant.Request.PRODUCT_ID) int productID, @Query(Constant.Request.CUSTOMER_ID) String cusId, @Query(Constant.Request.STORE_ID) String storeId, @Query(Constant.Request.WEBSITE_ID) String websiteId, @Query(Constant.Request.ACTION) String action,
                           Callback<ProductDetailResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void getProfile(@FieldMap Map<String, String> parameters, Callback<LoginResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void forgotPassword(@FieldMap Map<String, String> parameters, Callback<ForgotResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void changePassword(@FieldMap Map<String, String> parameters, Callback<ChangeResponse> callback);


    @GET("/api")
    void getStaticPage(@Query(Constant.Request.STORE_ID) String storeId, @Query(Constant.Request.WEBSITE_ID) String websiteId, @Query(Constant.Request.ACTION) String action,
                       @Query(Constant.Request.PAGE_KEY) String pageKey, Callback<StaticPageResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void addToCart(@FieldMap Map<String, String> parameters, Callback<CartResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void getCartInfo(@FieldMap Map<String, String> parameters, Callback<CartItemResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void updateCart(@FieldMap Map<String, String> parameters, Callback<CartUpdateResponse> callback);


    @DELETE("/api")
    void deleteCart(@Query(Constant.Request.STORE_ID) String storeId,
                    @Query(Constant.Request.WEBSITE_ID) String websiteId,
                    @Query(Constant.Request.CUSTOMER_ID) String cusId,
                    @Query(Constant.Request.TOKEN) String token,
                    @Query(Constant.Request.PRODUCT_ID) String productId,
                    @Query(Constant.Request.CART_REQ_QUOTE_ID) String quoteId,
                    @Query(Constant.Request.SUPER_ATTRIBUTE) String attibute,
                    @Query(Constant.Request.CUSTOM_OPTION) String option,
                    @Query(Constant.Request.ACTION) String action,
                    Callback<CartUpdateResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void getAddress(@FieldMap Map<String, String> parameters, Callback<AddressResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void manageAddress(@FieldMap Map<String, String> parameters, Callback<AddressResponse> callback);

    @DELETE("/api")
    void deleteAddress(@Query(Constant.Request.STORE_ID) String storeId,
                       @Query(Constant.Request.WEBSITE_ID) String websiteId,
                       @Query(Constant.Request.CUSTOMER_ID) String cusId,
                       @Query(Constant.Request.TOKEN) String token,
                       @Query(Constant.Request.ADDRESS_ID) String addressId,
                       @Query(Constant.Request.ACTION) String action,
                       Callback<DeleteAddress> callback);

    @FormUrlEncoded
    @POST("/api")
    void getOrder(@FieldMap Map<String, String> parameters, Callback<OrderResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void getOrderDetail(@FieldMap Map<String, String> parameters, Callback<OrderDetailResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void getWishlist(@FieldMap Map<String, String> parameters, Callback<WishlistResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void addWishlist(@FieldMap Map<String, String> parameters, Callback<WishlistResponse> callback);

    @DELETE("/api")
    void deleteWishlist(@Query(Constant.Request.STORE_ID) String storeId,
                        @Query(Constant.Request.WEBSITE_ID) String websiteId,
                        @Query(Constant.Request.CUSTOMER_ID) String cusId,
                        @Query(Constant.Request.TOKEN) String token,
                        @Query(Constant.Request.PRODUCT_ID) String productId,
                        @Query(Constant.Request.ACTION) String action,
                        Callback<WishlistResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void addReview(@FieldMap Map<String, String> parameters, Callback<ReviewResponse> callback);

    @GET("/api")
    void getFilter(@Query(Constant.Request.STORE_ID) String storeId, @Query(Constant.Request.WEBSITE_ID) String websiteId,
                   @Query(Constant.Request.ACTION) String action, @Query(Constant.Request.CATEGORY_ID) String catID,
                   Callback<FilterResponse> callback);

    @GET("/api")
    void getSearch(@Query(Constant.Request.STORE_ID) String storeId,
                   @Query(Constant.Request.WEBSITE_ID) String websiteId,
                   @Query(Constant.Request.ACTION) String action,
                   @Query(Constant.Request.SEARCH_TERM) String terms,
                   @Query(Constant.Request.CUSTOMER_ID) String customerID,
                   @Query(Constant.Request.PAGE) String page,
                   @Query(Constant.Request.LIMIT) String limit,
                   Callback<ProductResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void addAddressCart(@FieldMap Map<String, String> parameters, Callback<CheckoutResponse> callback);

    @FormUrlEncoded
    @POST("/api")
    void placeOrder(@FieldMap Map<String, String> parameters, Callback<PlaceOrderResponse> callback);


    @FormUrlEncoded
    @POST("/api")
    void coupon(@FieldMap Map<String, String> parameters, Callback<CouponResponse> callback);


    @GET("/api")
    void getCountryList(@Query(Constant.Request.ACTION) String action, Callback<CountryResponse> callback);


    @GET("/api")
    void getStateList(@Query(Constant.Request.ACTION) String action, @Query(Constant.Request.COUNTRY_CODE) String countryCode, Callback<RegionResponse> callback);


    @GET("/api")
    void getReview(@Query(Constant.Request.STORE_ID) String storeId,
                   @Query(Constant.Request.WEBSITE_ID) String websiteId,
                   @Query(Constant.Request.ACTION) String action,
                   @Query(Constant.Request.PRODUCT_ID) String customerID,
                   @Query(Constant.Request.PAGE) int page,
                   @Query(Constant.Request.LIMIT) int limit,
                   Callback<ReviewListResponse> callback);

}