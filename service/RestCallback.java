/**
 * RestCallback.java
 * The Class is the callback class for maintain the cilent response due to the failure and success.
 *
 * @category Contus
 * @package contus.com.mcomm.service
 * @version 1.0
 * @author Contus Team <developers@contus.in>
 * @copyright Copyright (C) 2015 Contus. All rights reserved.
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */
package com.contus.mcomm.service;


import android.content.Context;
import android.content.res.Resources;

import com.contus.mcomm.bus.BusProvider;
import com.contus.mcomm.model.ErrorResponse;
import com.contus.mcomm.utils.LogUtils;
import com.contussupport.ecommerce.McommApplication;
import com.contussupport.ecommerce.R;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class RestCallback<T> implements Callback<T> {
    private static final String TAG = LogUtils.makeLogTag(RestCallback.class);
    private Context mContext;
    private Resources res;

    /**
     * Returns the unsuccessful httpresponse due to network error and exception.
     *
     * @param error
     */
    @Override
    public void failure(RetrofitError error) {
        mContext = McommApplication.getContext();
        res = mContext.getResources();
        String errorDescription;
        try {
            ErrorResponse errorResponse = (ErrorResponse) error.getBodyAs(ErrorResponse.class);
            errorDescription = errorResponse.message;
        } catch (Exception ex) {
            errorDescription = error.getLocalizedMessage();
        }

        switch (error.getKind()) {
            case HTTP:
                break;
            case NETWORK:
                errorDescription = res.getString(R.string.error_connecting_error);
                break;
            case CONVERSION:
                errorDescription = res.getString(R.string.error_passing_data);
                break;
            case UNEXPECTED:
                errorDescription = res.getString(R.string.error_unexpected);
                break;
            default:
                break;
        }
        BusProvider.getInstance().post(errorDescription);

    }

    /**
     * Returns the successful http response
     *
     * @param arg0
     * @param arg1 body
     */
    @Override
    public void success(Object arg0, Response arg1) {
        try {
            BusProvider.getInstance().post(arg0);
        } catch (Exception e) {
            LogUtils.i(TAG, e.getMessage());
            BusProvider.getInstance().post(e.getMessage());
        }
    }
}
